using DG.Tweening;
using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateObject : MonoBehaviour
{
    [Separator("Particle System")]
    [SerializeField] List<GameObject> objectToInst;
    [SerializeField] Transform positionToInst;
    [SerializeField] float particleDuration;

    [Separator("Lily Pad Feedback")]
    [SerializeField] private float yOffset = 1f;
    [SerializeField] private float playerParentDuration = .5f;
    public void OnLilyPadStepped(Transform _player)
    {
        foreach (GameObject gm in objectToInst)
        {
            GameObject tgm=Instantiate(gm, positionToInst.position, Quaternion.identity);
            Destroy(tgm, particleDuration);
        }

        StartCoroutine(SetPlayerParent(_player));
        StartCoroutine(MoveLilyPad());
    }

    private IEnumerator SetPlayerParent(Transform _player)
    {
        _player.SetParent(this.transform);
        yield return new WaitForSeconds(playerParentDuration);
        _player.SetParent(null);
    }

    public IEnumerator MoveLilyPad()
    {
        this.transform.DOMoveY(this.transform.position.y - yOffset, playerParentDuration / 2);
        yield return new WaitForSeconds(playerParentDuration / 2);
        this.transform.DOMoveY(this.transform.position.y + yOffset, playerParentDuration / 2);
    }
}
