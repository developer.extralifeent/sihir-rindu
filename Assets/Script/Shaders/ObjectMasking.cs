using MyBox;
using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Shader
{
    public class ObjectMasking : MonoBehaviour, ISerializationCallbackReceiver
    {
        [Separator("Class References")]
        [SerializeField] private PlayerController player;
        [SerializeField] private Transform camTargetTransform;
        private bool isSerialized;

        [Separator("Objects To Mask")]
        [SerializeField] private GameObject[] maskObject;

        // Start is called before the first frame update
        void Start()
        {
            for (int i = 0; i < maskObject.Length; i++)
            {
                maskObject[i].GetComponent<Renderer>().material.renderQueue = 3002;
            }
        }

        // Update is called once per frame
        void Update()
        {
            RaycastHit hit;
            if(Physics.Raycast(camTargetTransform.position, (player.transform.position - camTargetTransform.position), out hit, 1000f))
            {
                this.transform.position = hit.point;
            }
        }
        public void OnAfterDeserialize()
        {
            isSerialized = true;
        }

        public void OnBeforeSerialize()
        {
            if(!isSerialized)
            {
                player = FindObjectOfType<PlayerController>();
                camTargetTransform = player.transform.GetChild(0);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(camTargetTransform.position, (player.transform.position - camTargetTransform.position) * 1000f);
        }
    }
}
