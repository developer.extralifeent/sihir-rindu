using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.Events;
using OLJ.Character;
using MyBox;
using System;

namespace OLJ.Manager {
    public class ControlManager : Singleton<ControlManager> {
        private CharacterInput charInput;
        [SerializeField] PlayerInput playerInput;
        [SerializeField] CharacterBody previousBody;
        [SerializeField] CharacterBody currentBody;

        public class OnControlSchemeChanged : UnityEvent<string> { }
        public OnControlSchemeChanged onControlSchemeChanged = new OnControlSchemeChanged();
        [SerializeField] List<InputSO> inputIcon;
        string currentControlScheme;
        
        List<InputAction> qteInput=new List<InputAction>();
        public string CurrentControlScheme => currentControlScheme;
        public Action<string> onControlScemeChanged;

        public List<InputAction> QTEInput => qteInput;
        public CharacterInput CharacterInputAction => charInput;
        public CharacterBody PreviousBody => previousBody;                       
        
        public CharacterBody CurrentBody {
            get {
                return currentBody;
            }
            set {
                if (currentBody) {
                    previousBody = currentBody;
                    previousBody.DisableControl();
                }               
                currentBody = value;
                if (currentBody != null) {
                    currentBody.EnableControl();
                }
            }
        }
        protected override void Awake() {
            base.Awake();           
            charInput = new CharacterInput();
            currentControlScheme = "Keyboard";
            foreach(InputAction act in charInput.asset.FindActionMap("QTE")) {
                qteInput.Add(act);
            }
            InputUser.onChange += (user, change, device) => {
                onControlScemeChanged?.Invoke(playerInput.currentControlScheme);

                if (change == InputUserChange.ControlSchemeChanged || change == InputUserChange.ControlsChanged) {
                    Debug.Log("Current Scheme: " + playerInput.currentControlScheme);
                    currentControlScheme = playerInput.currentControlScheme;
                    onControlSchemeChanged?.Invoke(currentControlScheme);
                } else {
                    currentControlScheme = "Keyboard";
                }
            };
        }
       
        public Sprite GetIcon(string path) {
            Sprite iconSelected = null;
            InputSO iconSet = inputIcon.Find(x => x.group.Equals(currentControlScheme,System.StringComparison.OrdinalIgnoreCase));
            Debug.Log("Icon Path: " + path);
            if (iconSet && iconSet.controlIcons != null) {
                string newPath = path.Replace("<", "").Replace(">", "");
                Debug.Log("New Path: " + newPath);
                ControlIcon ci = iconSet.controlIcons.Find(x => x.path.Equals(newPath, System.StringComparison.InvariantCultureIgnoreCase));
                Debug.Log("Control Icon: " + ci);
                if (ci != null) {

                    iconSelected = ci.icon;
                } else {
                    Debug.Log("Control Icon Not Found");
                }
            }
            return iconSelected;
        }

        #region About to changed soon
        public void EnableControl() {
            charInput.Player.Enable();
        }
        public void DisableControl() {
            charInput.Player.Disable();
        }
        public void EnabledQTE() {
            charInput.QTE.Enable();
        }
        public void DisableQTE() {
            charInput.QTE.Disable();
        }
        #endregion

        public void SwitchInputAction(InputActionMap inputActionMap)
        {
            if (inputActionMap.enabled)
            {
                Debug.Log($"{inputActionMap.name} action map is already active");
                return;
            }
            charInput.Disable();
            inputActionMap.Enable();

            Debug.Log(inputActionMap.name + " Enabled");
        }
     
    }
}

