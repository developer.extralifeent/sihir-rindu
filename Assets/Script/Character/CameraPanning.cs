using OLJ.Manager;
using OLJ.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace OLJ.Gameplay
{
    public class CameraPanning : MonoBehaviour
    {
        private ControlManager controlManager;
        private CharacterInput playerInput;

        [Header("Panning")]
        [SerializeField] private float panSpeed;

        [Header("Resetting Cam Pos")]
        [SerializeField] private float camResetDelay;
        [SerializeField] private float resetLerpSpeed;
        private Vector3 initPos;

        private Vector3 inputVector;
        private bool isPanActive;

        public Action onPanningActive;
        public Action onPanningDeactive;

        // Start is called before the first frame update
        void Start()
        {
            controlManager = ControlManager.Instance;
            playerInput = controlManager.CharacterInputAction;

            UIManager.Instance.CamPan = this;

            playerInput.CameraControl.Enable();
            playerInput.CameraControl.CamPan.performed += (InputAction.CallbackContext _context) =>
            {
                isPanActive = true;
                StopAllCoroutines();
                Cursor.lockState = CursorLockMode.Locked;
                //onPanningActive?.Invoke();

                UIManager.Instance.ToggleCamPanIcon(true);
            };
            playerInput.CameraControl.CamPan.canceled += (InputAction.CallbackContext _context) =>
            {
                isPanActive = false;
                StartCoroutine(ResetCamPos());
                Cursor.lockState = CursorLockMode.None;
                //onPanningDeactive?.Invoke();

                UIManager.Instance.ToggleCamPanIcon(false);
            };

            initPos = this.transform.localPosition;
        }

        private void OnDestroy()
        {
            playerInput.CameraControl.CamPan.performed -= (InputAction.CallbackContext _context) =>
            {
                isPanActive = true;
                StopAllCoroutines();
                Cursor.lockState = CursorLockMode.Locked;
                //onPanningActive?.Invoke();

                UIManager.Instance.ToggleCamPanIcon(true);
            };
            playerInput.CameraControl.CamPan.canceled -= (InputAction.CallbackContext _context) =>
            {
                isPanActive = false;
                StartCoroutine(ResetCamPos());
                Cursor.lockState = CursorLockMode.None;
                //onPanningDeactive?.Invoke();

                UIManager.Instance.ToggleCamPanIcon(false);
            };
        }

        private void Update()
        {
            CameraPanHandler();
        }

        private void CameraPanHandler()
        {
            if (!isPanActive) return;

            inputVector = playerInput.CameraControl.CamPos.ReadValue<Vector2>();
            Vector3 moveDir = (transform.forward * inputVector.y + transform.right * inputVector.x).normalized;

            this.transform.localPosition = new Vector3(
                Mathf.Clamp(this.transform.localPosition.x, -25f, 25f),
                Mathf.Clamp(this.transform.localPosition.y, 40f, 40),
                Mathf.Clamp(this.transform.localPosition.z, -55f, -20f));

            this.transform.localPosition += moveDir * panSpeed * Time.deltaTime;

        }

        private IEnumerator ResetCamPos()
        {
            yield return new WaitForSeconds(camResetDelay);

            while (this.transform.localPosition != initPos)
            {
                this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, initPos, resetLerpSpeed * Time.deltaTime);
                yield return null;
            }
        }
    }
}
