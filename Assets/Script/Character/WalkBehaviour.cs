using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.XR;

namespace OLJ.Character
{
    public class WalkBehaviour : MonoBehaviour
    {
        private int slowWalkAnimHash = Animator.StringToHash("Slow Run");
        private int normalWalkAnimHash = Animator.StringToHash("run");
        private int idleAnimHash = Animator.StringToHash("idle");

        [Header("Class References")]
        [SerializeField] private PlayerController controller;

        [Header("Movement value")]
        [SerializeField] private float slowedMovementSpeed;

        public void SlowAreaHandler(bool _isSlowed)
        {
            if (controller.MoveValue != Vector3.zero && _isSlowed)
            {
                controller.PlayAnimation(slowWalkAnimHash);
                controller.CharSpeed = slowedMovementSpeed;
            }
            else if (controller.MoveValue != Vector3.zero && !_isSlowed)
            {
                controller.PlayAnimation(normalWalkAnimHash);
                controller.CharSpeed = controller.BaseSpeed;
            }
            else
                controller.PlayAnimation(idleAnimHash);
        }


    }

}
