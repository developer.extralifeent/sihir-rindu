using Cinemachine;
using OLJ;
using System.Collections;
using UnityEngine;

namespace OLJ.Gameplay
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera virtualCam;
        private CinemachineBasicMultiChannelPerlin cinemachinePerlin;

        [SerializeField] private float shakeDuration = .5f;
        private float currentTime = 0f;

        [SerializeField] private float magnitude = 5f;

        private void Start()
        {
            cinemachinePerlin = virtualCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        private void Update()
        {
            ShakeCamCountDown();
        }

        public void ShakeCam()
        {
            cinemachinePerlin = virtualCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            
            cinemachinePerlin.m_AmplitudeGain = magnitude;
            currentTime = shakeDuration;
        }

        public void ShakeCam(float _duration, float _magnitude)
        {
            cinemachinePerlin = virtualCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

            cinemachinePerlin.m_AmplitudeGain = _magnitude;
            currentTime = _duration;
        }

        private void ShakeCamCountDown()
        {
            if (currentTime <= 0f) return;

            currentTime -= Time.deltaTime;
            if (currentTime <= 0f)
            {
                cinemachinePerlin.m_AmplitudeGain = Mathf.Lerp(magnitude, 0f, 1 - (currentTime / shakeDuration));
            }
        }
    }
}
