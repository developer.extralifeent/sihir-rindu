using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;
using OLJ.Manager;

namespace OLJ.Character {
    public class AIController : CharacterAnimation {
       
        public enum Behaviour {
            Patrol,
            //Attack,
            Controlled,
            Chasing,
            None
        }
        [Header("AI Controller Parameters")]
        public Behaviour MonsterState;
        public NavMeshAgent Nav;
        private bool isAgentActive = true;
        public bool IsAgentActive
        {
            get { return isAgentActive; }
            set { isAgentActive = value; }
        }

        [Header("Patrol Behaviour")]
        [SerializeField] float idlingDuration;
        [SerializeField] List<Transform> patrolPos;
        int patrolIndex;
        float tempIdleDuration;
        Vector3 lastDirection;

        [Header("Chasing Behaviour")]
        [SerializeField] private PlayerWaypointAI playerWaypoint;
        [SerializeField] private CharacterBody playerTarget;
        [SerializeField] private float minDistanceToTarget = 1.5f;
        [SerializeField] private float playerDistanceTarget = 15f;
        [SerializeField] private float slowedDuration = 1.5f;
        [SerializeField] private float slowedSpeed = 3f;
        [SerializeField] private ParticleSystem obstacleDestroyParticle;
        private bool isSlowedDown = false;
        private PlayerHp playerHp;

        //[Header("Attacking Behaviour")]
        //[SerializeField] float playerDetectRange;
        //[SerializeField] float eatingRange;
        //[SerializeField] float letGoRange;
        ////[SerializeField] float speedMultiplier = 3;
        //[SerializeField] Collider playerDetection;
        //Transform target; 

        private float initialSpeed = 10;

        //Events
        public event Action OnPatrol;
        //public event Action OnAttack;
        private void Start() {
            StartCoroutine(ChangeStateBehaviour());
            tempIdleDuration = idlingDuration;
            Nav = GetComponent<NavMeshAgent>();
            initialSpeed = Nav.speed;
            Nav.stoppingDistance = minDistanceToTarget;

            if(playerTarget == null)
            {
                playerTarget = ControlManager.Instance.CurrentBody;
            }

            playerHp = playerTarget.GetComponent<PlayerHp>();
            HPManager.Instance.onPlayerDeadInstantly += () =>
            {
                Invoke(nameof(ResetPosition), playerHp.RespawnDuration);
                Debug.Log("Monster Position Has Been Reset");
            };
        }

        private void OnDestroy()
        {
            HPManager.Instance.onPlayerDeadInstantly -= () =>
            {
                Invoke(nameof(ResetPosition), playerHp.RespawnDuration);
                Debug.Log("Monster Position Has Been Reset");
            };
        }

        protected virtual void TakeControlled() {
            Nav.enabled = false;
            rb.isKinematic = false;
            CameraV.m_Priority = 15;
        }
        //protected virtual void Attacking() {
        //    OnAttack?.Invoke();
        //    Nav.enabled = true;
        //    Nav.isStopped = false;
        //    Nav.destination = target.position;
        //    Nav.speed = initialSpeed * speedMultiplier;
        //    Vector3 currentTransform = transform.position;
        //    lastDirection = target.position - currentTransform;
        //    SetAnimDirection((lastDirection).normalized, 1);
        //    playerDetection.transform.localPosition = AnimeDirection * playerDetectRange;
        //    EatingPlayer();
        //}
        protected virtual void Patrol() {
            //Nav.enabled = true;
            if (!Nav.enabled) return;
            
            Nav.speed = initialSpeed;
            rb.isKinematic = true;
            CameraV.m_Priority = 10;
            if (patrolPos.Count > 0 && patrolPos != null) {
                if (Nav.remainingDistance <= 0) {
                    Nav.isStopped = true;
                    if (tempIdleDuration <= 0) {
                        OnPatrol?.Invoke();
                        Vector3 currentTransform = transform.position;
                        Transform selectedTrans = SelectPatrolPos();
                        Nav.destination = selectedTrans.position;
                        lastDirection = selectedTrans.position - currentTransform;
                        SetAnimDirection((lastDirection).normalized, 1);
                        //playerDetection.transform.localPosition = AnimeDirection * playerDetectRange;
                        Nav.isStopped = false;
                        tempIdleDuration = idlingDuration;
                    } else {
                        tempIdleDuration -= Time.deltaTime;
                        SetAnimDirection(lastDirection, 0f);
                    }

                }
            }
           

        }
        //void EatingPlayer() {
        //    if (Vector3.Distance(transform.position, target.position) < eatingRange) {
        //        target.GetComponent<PlayerController>().Dead();
        //        PlayAnimation("Eat");
        //        MonsterState = Behaviour.Patrol;
        //    }
        //}
        Transform SelectPatrolPos() {
            patrolIndex++;
            if (patrolIndex > patrolPos.Count - 1) {
                patrolIndex = 0;
            }else if (patrolIndex < 0) {
                patrolIndex = patrolPos.Count - 1;
            }
            Transform selectedTrans = patrolPos[patrolIndex];
            return selectedTrans;
        }

        private void ChasePlayer()
        {
            #region SlowDownParams
            if (!isSlowedDown)
                Nav.speed = initialSpeed;
            else
                Nav.speed = slowedSpeed;
            #endregion

            #region Setting AI Animation
            Vector3 animDirection = (playerTarget.transform.position - this.transform.position).normalized;
            SetAnimDirection(animDirection, 1);
            #endregion

            float distance = Vector3.Distance(this.transform.position, playerTarget.transform.position);
            Debug.Log("Monster distance to player: " + distance);

            //If player is too far = Set state to chase with waypoint. Or else, set state to chase with navmesh
            if(distance >= playerDistanceTarget)
            {
                Nav.SetDestination(playerWaypoint.ClosestWaypoint.position);
                Debug.Log("Chasing Waypoint");
            }
            else
            {
                Nav.SetDestination(playerTarget.transform.position);
                Debug.Log("Chasing Player");
            }

            //If player is too close = Player get caught
            if (distance <= minDistanceToTarget)
            {
                HPManager.Instance.onPlayerDeadInstantly?.Invoke();
            }
        }

        private void SlowDown()
        {
            StartCoroutine(SlowDownCoroutine());

            IEnumerator SlowDownCoroutine()
            {
                Nav.speed = slowedSpeed;
                isSlowedDown = true;
                yield return new WaitForSeconds(slowedDuration);
                isSlowedDown = false;
            }
        }

       public IEnumerator ChangeStateBehaviour() {
            while (true) {
                switch (MonsterState) {
                    case Behaviour.Patrol:
                        if (patrolPos.Count > 0) {
                            //Debug.Log("Patrol");
                            Patrol();
                        }
                        break;
                    //case Behaviour.Attack:
                    //    Attacking();
                    //    break;
                    case Behaviour.Controlled:
                        TakeControlled();
                        break;
                    case Behaviour.Chasing:
                        ChasePlayer();
                        break;
                    case Behaviour.None:
                        break;
                }
                yield return null;
            }
       }

        private void OnCollisionEnter(Collision other)
        {
            if (other.transform.CompareTag("DestructibleObstacle"))
            {
                SlowDown();
                other.gameObject.SetActive(false);

                if (obstacleDestroyParticle)
                {
                    Instantiate(obstacleDestroyParticle, other.transform.position, Quaternion.identity);
                    obstacleDestroyParticle.Play();
                }
            }
        }

        //IEnumerator DetectingTarget() {
        //    while (true) {
        //        if (Nav.remainingDistance > letGoRange) {
        //            Debug.Log("AI: Patrol");
        //            MonsterState = Behaviour.Patrol;
        //            StopCoroutine(DetectingTarget());
        //        }
        //        yield return null;
        //    }
        //}

    }
}

