using OLJ.Gameplay;
using OLJ.Manager;
using OLJ.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Character
{
    public class PlayerHp : MonoBehaviour
    {
        private HPManager hpManager;

        [Header("Class References")]
        [SerializeField] private CameraShake camShake;

        [Header("Hp Setter")]
        [SerializeField] private float respawnDuration = .5f;
        public float RespawnDuration => respawnDuration;

        [Header("Broken HP")]
        [SerializeField] private bool isBroken;
        public bool IsBroken
        {
            get
            {
                return isBroken;
            }
            set
            {
                isBroken = value;
            }
        }

        private float currentHp;

        private void Awake()
        {
            hpManager = HPManager.Instance;
        }

        // Start is called before the first frame update
        void Start()
        {
            currentHp = hpManager.CurrentHp;

            hpManager.onPlayerDeadInstantly += RespawnHp;
        }

        private void OnDisable()
        {
            hpManager.onPlayerDeadInstantly -= RespawnHp;
        }

        public void TakeDamage(float _hpToSubtract)
        {
            currentHp -= _hpToSubtract;
            hpManager.CurrentHp = currentHp;

            camShake.ShakeCam();

            if(currentHp <= 0f)
            {
                currentHp = 0f;
                hpManager.CurrentHp = 0f;
                StopCoroutine(RegenHpCoroutine());
                //Respawn
            }
            else
                Invoke(nameof(RegenHp), .5f);

            if(HpRootUIManager.Instance)
                HpRootUIManager.Instance.PlaySpineAnim();
        }

        //This function is used for testing purpose only.
        //DO NOT CALL IT ANYWHERE ELSE!
        public void SetHp(string _hpToSet)
        {
            float hp = float.Parse(_hpToSet);
            currentHp = hp;
            hpManager.CurrentHp = currentHp;

            if(hp < hpManager.MaxHp)
            {
                StopAllCoroutines();
                RegenHp();
            }
        }

        private IEnumerator RegenHpCoroutine()
        {
            if (!isBroken || (isBroken && currentHp > hpManager.BrokenHpThreshold))
            {
                while (currentHp < hpManager.MaxHp)
                {
                    currentHp += hpManager.HpToRegen;
                    hpManager.CurrentHp = currentHp;
                    yield return new WaitForSeconds(hpManager.HpRegenTime);
                }
            }
            else if(isBroken && currentHp < hpManager.BrokenHpThreshold)
            {
                while (currentHp < hpManager.BrokenHpThreshold)
                {
                    currentHp += hpManager.HpToRegen;
                    hpManager.CurrentHp = currentHp;
                    yield return new WaitForSeconds(hpManager.HpRegenTime);
                }
            }
        }

        private void RegenHp()
        {
            StartCoroutine(RegenHpCoroutine());
        }

        private IEnumerator RegenHpInstantlyCoroutine()
        {
            float regenMultiplier = 150f;

            while (currentHp < hpManager.MaxHp)
            {
                currentHp += Time.deltaTime * regenMultiplier;
                hpManager.CurrentHp = currentHp;
                yield return null;
            }
        }

        public void RegenHpInstantly()
        {
            StartCoroutine(RegenHpInstantlyCoroutine());
        }

        private IEnumerator DrainHpCoroutine()
        {
            float drainMultiplier = 150f;

            while(currentHp > 0)
            {
                currentHp -= Time.deltaTime * drainMultiplier;
                hpManager.CurrentHp = currentHp;
                yield return null;
            }
        }

        public void DrainHp()
        {
            StartCoroutine(DrainHpCoroutine());
        }

        private IEnumerator RespawnHpCoroutine()
        {
            DrainHp();
            yield return new WaitForSeconds(respawnDuration / 2f);
            RegenHpInstantly();
        }

        private void RespawnHp()
        {
            StopAllCoroutines();
            StartCoroutine(RespawnHpCoroutine());
        }
    }
}
