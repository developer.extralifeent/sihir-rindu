using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Manager;
using UnityEngine.InputSystem;

namespace OLJ.Character {
    public class CharacterAnimation : CharacterBody {
        [Header("Character Animation Parameter")]
        [SerializeField] Animator anime;
        [SerializeField] Animator maskingAnime;
        private Vector3 animeDirection;

        public Animator Anime => anime;
        public Animator MaskingAnim => maskingAnime;

        public Vector3 AnimeDirection => animeDirection;

        protected override void CharacterMove(InputAction.CallbackContext context) {
            base.CharacterMove(context);
            if (ControlManager.Instance.CurrentBody == this) {

                PlayMoveAnim();
            }
        }
        protected override void CharacterStop(InputAction.CallbackContext context) {
            base.CharacterStop(context);
            if (ControlManager.Instance.CurrentBody == this) {
                PlayMoveAnim();
            }
        }
        public void MoveCharacter(Transform target) {

            Vector3 direction = (target.position - transform.position).normalized;
            
            //transform.position = Vector3.MoveTowards(transform.position, target.position, charSpeed * Time.deltaTime);
            transform.position = Vector3.MoveTowards(transform.position, target.position, charSpeed * Time.deltaTime);
            SetAnimDirection(direction, 1);
        }
        public void PlayMoveAnim() {

            anime.SetFloat("Horizontal", DirCalculation(LastInput.x));
            anime.SetFloat("Vertical", DirCalculation(LastInput.z));
            animeDirection = new Vector3(DirCalculation(LastInput.x), 0, DirCalculation(LastInput.z));
            anime.SetFloat("Speed", MoveValue.magnitude);

            if (maskingAnime)
            {
                maskingAnime.SetFloat("Horizontal", DirCalculation(LastInput.x));
                maskingAnime.SetFloat("Vertical", DirCalculation(LastInput.z));
                animeDirection = new Vector3(DirCalculation(LastInput.x), 0, DirCalculation(LastInput.z));
                maskingAnime.SetFloat("Speed", MoveValue.magnitude);
            }
        }

        private float DirCalculation(float value) {
            if (value > 0.2f) {
                return 1;
            } else if (value < -0.2f) {
                return -1;
            } else {
                return 0;
            }
        }
       
        public void SetAnimDirection(Vector3 direction,float speed) {
            anime.SetFloat("Horizontal", DirCalculation(direction.x));
            anime.SetFloat("Vertical", DirCalculation(direction.z));
            anime.SetFloat("Speed", speed);
            animeDirection = new Vector3(DirCalculation(direction.x), 0, DirCalculation(direction.z));

            if (maskingAnime != null)
            {
                Debug.Log("Moving masking character");
                maskingAnime.SetFloat("Horizontal", DirCalculation(direction.x));
                maskingAnime.SetFloat("Vertical", DirCalculation(direction.z));
                animeDirection = new Vector3(DirCalculation(direction.x), 0, DirCalculation(direction.z));
                maskingAnime.SetFloat("Speed", speed);
            }
        }
        public void PlayAnimation(string animName) {
            anime.Play(animName);
        }
        public void PlayAnimation(int animHash)
        {
            if(maskingAnime)
                maskingAnime.Play(animHash);

            anime.Play(animHash);
        }
    }
}

