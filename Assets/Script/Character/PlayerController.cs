using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using OLJ.Gameplay.Interactable;
using OLJ.Manager;

namespace OLJ.Character {
    public class PlayerController : CharacterAnimation {
        [Header("Player Controller Parameter")]
        [SerializeField] int speedMultiplier = 3;
        [SerializeField] float jumpPower = 2;
        float baseSpeed;

        [Header("Jumping")]
        [SerializeField] private float jumpDuration = .5f;

        public float BaseSpeed
        {
            get { return baseSpeed; }
            private set { baseSpeed = value; }
        }

        private HPManager hpManager;
        private PlayerHp playerHp;

        protected override void Awake()
        {
            base.Awake();
            ControlManager.Instance.CurrentBody = this;
        }

        private void Start() {
            hpManager = HPManager.Instance;

            ControlManager.Instance.SwitchInputAction(ControlManager.Instance.CharacterInputAction.Player);
            baseSpeed = charSpeed;
            charInput.Player.Additional.performed += StartRunning;
            charInput.Player.Additional.canceled += StopRunning;
            charInput.Player.Jump.performed += Jumping;

            EnableControl();
            playerHp = this.GetComponent<PlayerHp>();

            hpManager.onPlayerDeadInstantly += RespawnPlayer;

            //Testing material
            Renderer renderer = this.GetComponentInChildren<Renderer>();
            renderer.material.renderQueue = 4000;

            
        }
        private void OnDisable() {
            charInput.Player.Additional.performed -= StartRunning;
            charInput.Player.Additional.canceled -= StopRunning;
            charInput.Player.Jump.performed -= Jumping;
            hpManager.onPlayerDeadInstantly -= RespawnPlayer;
        }

        private void Update()
        {
            Debug.DrawRay(wallCheckPos.position, MoveValue * jumpPower);
        }

        void Jumping(InputAction.CallbackContext context) {
            JumpDetection();
        }

        void JumpDetection() {
            float sphereCenterMultiplier = 3f;
            sphereCenterOffset = LastInput.normalized * sphereCenterMultiplier;
            Vector3 currentPos = itemCheckPos.position + sphereCenterOffset;
            if (Physics.CheckSphere(currentPos, objectDetectionRadius, interactType)) {
                Collider[] colliders = Physics.OverlapSphere(currentPos, objectDetectionRadius);
                foreach (Collider collider in colliders) {
                    if (collider.TryGetComponent(out JumpInteract _jumpInteract)) {
                        _jumpInteract.InteractStart(this);
                        return;
                    }
                }
                return;
            }

            #region Free Jumping (Temporarily disabled)
            //if (!IsGrounded || !IsCanJump) return;

            //Debug.Log("IsWallDetected : " + IsWallDetected(jumpPower));
            //Vector3 endPos;
            //if (!IsWallDetected(jumpPower))
            //    endPos = this.transform.position + MoveValue * jumpPower;
            //else
            //    endPos = this.transform.localPosition;
            //GetComponent<JumpBehaviour>().Jump(endPos, jumpDuration, MoveValue);
            //SetAnimDirection(LastInput, 0);
            #endregion
        }
        void StartRunning(InputAction.CallbackContext context) {
            charSpeed = charSpeed * speedMultiplier;
        }
        void StopRunning(InputAction.CallbackContext context) {
            charSpeed = baseSpeed;
        }
        public override void Dead() {
            base.Dead();
            OLJ.Manager.GameManager.Instance.onGameOver?.Invoke();
        }
      
        private void RespawnPlayer()
        {
            DisableControl();
            Invoke(nameof(ResetPosition), playerHp.RespawnDuration);
            Invoke(nameof(EnableControl), playerHp.RespawnDuration);
        }
    }
}
