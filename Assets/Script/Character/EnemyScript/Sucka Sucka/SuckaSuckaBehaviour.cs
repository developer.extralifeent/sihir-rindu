using DG.Tweening;
using OLJ.Manager;
using OLJ.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace OLJ.Character
{
    public class SuckaSuckaBehaviour : MonoBehaviour
    {
        [Header("Sucka States")]
        [SerializeField] private SUCKA_SUCKA_STATE suckaState = SUCKA_SUCKA_STATE.spawning;

        [Header("Cain")]
        [SerializeField] private CharacterBody cain;
        [SerializeField] private Animator cainAnim;

        [Header("Spawning State")]
        [SerializeField] private SpriteRenderer suckaRenderer;
        [SerializeField] private float spawnDuration = 2f;

        [Header("Chasing State")]
        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private float attackDistance = 1f;

        [Header("Attack State")]
        [SerializeField] private float backingOffset = 5f;
        [SerializeField] private float holdingDuration = .5f;
        [SerializeField] private float attackDelay = 3f;
        [SerializeField] private float suckaDamage = 30f;
        private const string playerAttackedAnimName = "Sucka Sucka";
        private int playerAttackAnimHash;
        private bool isChasing = true;

        [Header("Escape State")]
        [SerializeField] private float escapeSpeed;
        [SerializeField] private float fadeOutDuration = 2.5f;
        private const string cainNormalAnimName = "idle";
        private int cainNormalAnimHash;
        private Vector3 escapeDir;
        private bool isEscaping;
        //private bool isEscapeing;

        private Vector3 lastPos;

        private void Start()
        {
            SetUpVariables();

            StartCoroutine(SpawnSucka());
        }

        //private void OnDestroy()
        //{
        //    QTEManager.Instance.onQTEfinished.RemoveListener((int _index) =>
        //    {
        //        suckaState = SUCKA_SUCKA_STATE.escaping;
        //        StartCoroutine(DeactiveSucka());
        //    });
        //}

        // Update is called once per frame
        void Update()
        {
            CheckSuckaState();
        }

        private void CheckSuckaState()
        {
            switch (suckaState)
            {
                case SUCKA_SUCKA_STATE.chasing:
                    ChasePlayer();
                    break;
                case SUCKA_SUCKA_STATE.sucking:
                    //AttackPlayer();
                    break;
                case SUCKA_SUCKA_STATE.escaping:
                    //Escape();
                    break;
            }
        }

        private IEnumerator SpawnSucka()
        {
            agent.enabled = true;
            this.transform.position = RandomPosition();

            suckaRenderer.DOFade(1f, spawnDuration);

            yield return new WaitForSeconds(spawnDuration + 1f);

            suckaState = SUCKA_SUCKA_STATE.chasing;
        }

        private void ChasePlayer()
        {
            if (!isChasing) return;

            agent.SetDestination(cain.transform.position);

            float distance = Vector3.Distance(this.transform.position, cain.transform.position);
            Debug.Log("Sucka Distance To Player : " + distance);

            //if (distance <= attackDistance)
            //{
            //    agent.enabled = false;
            //    lastPos = this.transform.position;
            //    AttackPlayer();
            //    suckaState = SUCKA_SUCKA_STATE.sucking;
            //}
            
        }

        #region OG Sucka (Temporarily Disabled)
        private void AttackPlayer()
        {
            if (isChasing) return;

            StartCoroutine(AttackCoroutine());

            //QTEManager.Instance.onQTEfinished.AddListener((int _index) =>
            //{
            //    suckaState = SUCKA_SUCKA_STATE.escaping;
            //    StartCoroutine(DeactiveSucka());
            //});
        }

        private IEnumerator AttackCoroutine()
        {
            this.transform.position = lastPos;

            Vector3 backPos = new Vector3(this.transform.position.x - backingOffset, this.transform.position.y, this.transform.position.z - backingOffset);
            this.transform.DOMove(backPos, .8f);

            yield return new WaitForSeconds(holdingDuration);
            this.transform.DOMove(cain.transform.position, .5f);

            //yield return new WaitForSeconds(.5f);
            //if(isAttacking)
            //{
            //    //this.transform.position = cain.transform.GetChild(8).position;

            //    //ControlManager.Instance.DisableControl();
            //    //cainAnim.Play(playerAttackAnimHash);

            //    //int randomQte = Random.Range(0, 3);
            //    //QTEManager.Instance.StartQTE(randomQte);

            //    isEscaping = false;
            //}
            //else
            //{
            //    suckaState = SUCKA_SUCKA_STATE.chasing;
            //}
        }

        private void Escape()
        {
            agent.enabled = false;  
            this.transform.position += escapeDir.normalized * escapeSpeed * Time.deltaTime;
            suckaRenderer.DOFade(0f, fadeOutDuration);

            if (isEscaping) return;
            cainAnim.Play(cainNormalAnimHash);
            isEscaping = true;

        }

        private IEnumerator DeactiveSucka()
        {
            QTEUIManager.Instance.HideQTEAnimation();
            ControlManager.Instance.EnableControl();

            yield return new WaitForSeconds(fadeOutDuration);
            Destroy(this.gameObject);
        }
        #endregion

        private IEnumerator StopSucka()
        {
            agent.SetDestination(lastPos);
            yield return new WaitForSeconds(attackDelay);

            isChasing = true;
            suckaState = SUCKA_SUCKA_STATE.chasing;
        }
        
        private void SetUpVariables()
        {
            if (cain == null)
                cain = ControlManager.Instance.CurrentBody;

            if (cainAnim == null)
                cainAnim = cain.GetComponentInChildren<Animator>();

            suckaRenderer.DOFade(0f, .001f);

            playerAttackAnimHash = Animator.StringToHash(playerAttackedAnimName);
            cainNormalAnimHash = Animator.StringToHash(cainNormalAnimName);

            escapeDir = RandomPosition();
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.TryGetComponent(out CharacterBody _body))
            {
                isChasing = false;
                lastPos = other.transform.position;
                other.GetComponent<PlayerHp>().TakeDamage(suckaDamage);

                StartCoroutine(StopSucka());
            }
        }

        private Vector3 RandomPosition()
        {
            float randomXPos = cain.transform.position.x + Random.Range(-50f, 50f);
            float randomZPos = cain.transform.position.z + Random.Range(-12f, 60f);
            Vector3 randomPosition = new Vector3(randomXPos, this.transform.position.y, randomZPos);

            return randomPosition;
        }
    }

    public enum SUCKA_SUCKA_STATE
    {
        none,
        spawning,
        chasing,
        sucking,
        escaping
    }
}
