using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

namespace OLJ.Character {
	public class FollowPointBehaviour : MonoBehaviour {
        [Header("Drone Component")]
        [SerializeField] List<Transform> waypoints;
        [SerializeField] float speed = 3f;
        [SerializeField] float waitTime = 2f;
        [SerializeField] float damageStopDuration = 3f;
        [SerializeField] float damageAmount = 10f;
        [SerializeField] float damageInterval = 1f; // Fire rate for damage

        [Space]
        [SerializeField, MyBox.ReadOnly] PlayerHp playerHP;

        [Header("Spine Animation")]
        [SerializeField] SkeletonAnimation attackSkeleton;
        [SerializeField] SkeletonDataAsset attackDataAsset;
        [SerializeField, SpineAnimation(nameof(attackDataAsset))] string idleAnimation;
        [SerializeField, SpineAnimation(nameof(attackDataAsset))] string attackAnimation;

        int currentIndex = 0;
        bool movingForward = true;
        bool isStopped = false;
        bool isDamaging = false; // Prevents multiple coroutines from stacking

        void Start() {
            PlayIdleAnimation();
            StartCoroutine(MoveRoutine());
            StartCoroutine(RefreshListRoutine());
        }

        IEnumerator RefreshListRoutine() {
            while (true) {
                if (playerHP == null) {
                    RefreshList();
                }
                else {
                    RefreshList();
                }
                yield return new WaitForSeconds(3f);
            }
        }

        void RefreshList() {
            playerHP = FindObjectOfType<PlayerHp>();
        }

        IEnumerator MoveRoutine() {
            while (true) {
                if (waypoints.Count == 0 || isStopped)
                    yield return null;

                Transform target = waypoints[currentIndex];

                while (!isStopped && Vector3.Distance(transform.position, target.position) > 0.1f) {
                    transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
                    yield return null;
                }

                if (!isStopped) {
                    yield return new WaitForSeconds(waitTime);
                    UpdateNextWaypoint();
                }
            }
        }

        public void DamageZone() {
            if (!isDamaging) {
                StartCoroutine(DamageOverTime());
            }
        }

        IEnumerator DamageOverTime() {
            isDamaging = true;
            PlayAttackAnimation();

            while (isDamaging) {
                if (playerHP != null) {
                    playerHP.TakeDamage(damageAmount);
                }

                StopMovement();

                yield return new WaitForSeconds(damageInterval);
            }
        }

        public void StopDamage() {
            isDamaging = false; // Stop applying damage
            PlayIdleAnimation();
        }

        void StopMovement() {
            isStopped = true;
            StartCoroutine(ResumeAfterDamage());
        }

        IEnumerator ResumeAfterDamage() {
            yield return new WaitForSeconds(damageStopDuration);
            isStopped = false;
        }

        void UpdateNextWaypoint() {
            if (movingForward) {
                currentIndex++;
                if (currentIndex >= waypoints.Count) {
                    currentIndex = waypoints.Count - 2;
                    movingForward = false;
                }
            }
            else {
                currentIndex--;
                if (currentIndex < 0) {
                    currentIndex = 1;
                    movingForward = true;
                }
            }
        }

        void PlayIdleAnimation() {
            if (attackSkeleton != null && !string.IsNullOrEmpty(idleAnimation)) {
                attackSkeleton.AnimationState.SetAnimation(0, idleAnimation, true);
            }
        }

        void PlayAttackAnimation() {
            if (attackSkeleton != null && !string.IsNullOrEmpty(attackAnimation)) {
                attackSkeleton.AnimationState.SetAnimation(0, attackAnimation, true);
            }
        }

        //Experimental, delete if you dont like
        void OnDrawGizmos() {
            if (waypoints == null || waypoints.Count < 2)
                return;

            Gizmos.color = Color.green;
            for (int i = 0;i < waypoints.Count - 1;i++) {
                Gizmos.DrawLine(waypoints[i].position, waypoints[i + 1].position);
            }
        }
    }
}

