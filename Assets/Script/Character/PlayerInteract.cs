using DG.Tweening;
using OLJ.Gameplay.Interactable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    [Header("Interactable Detection")]
    [SerializeField] private float detectionRange = 25f;
    [SerializeField] private float fadingDuration = .5f;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.grey;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
}
