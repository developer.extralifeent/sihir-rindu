using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWaypointAI : MonoBehaviour
{
    [Separator("Waypoints in scene")]
    [SerializeField] private Transform[] waypoints;

    public Transform ClosestWaypoint { get; private set; }

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(SetClosestWaypoint());
    }

    private IEnumerator SetClosestWaypoint()
    {
        while(true)
        {
            ClosestWaypoint = FindClosestWaypoint();
            Debug.Log("Closest Waypoint : " + ClosestWaypoint.name);
            yield return null;
        }
    }

    private Transform FindClosestWaypoint()
    {
        Transform closestWaypoint = this.transform;
        float waypointNearby = Mathf.Infinity;

        foreach (Transform waypoint in waypoints)
        {
            float distance = Vector3.Distance(waypoint.transform.position, this.transform.position);

            if (distance < waypointNearby)
            {
                waypointNearby = distance;
                closestWaypoint = waypoint.transform;
            }
        }

        return closestWaypoint;
    } 
}
