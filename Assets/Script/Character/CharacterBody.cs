using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;
using OLJ.Manager;
using OLJ.Gameplay.Interactable;
using OLJ.UI;
using UnityEngine.InputSystem;
using Cinemachine;
using DG.Tweening;
using System.Collections.Generic;

namespace OLJ.Character {
    public class CharacterBody : MonoBehaviour
    {
        [Header("----CharacterBody Parameters----")]
        [SerializeField] protected float charSpeed = 5f;
        [SerializeField] protected float velocityTolerance = 3f;
        [SerializeField] protected Rigidbody rb;
        [SerializeField] CinemachineVirtualCamera cameraV;
        private bool isControlled;

        [Header("----Groundcheck----")]
        [SerializeField] protected bool useGroundCheck = true;
        [SerializeField] protected Transform CheckPos;
        [SerializeField] protected float onGroundRange = 2.1f;
        [SerializeField] protected float groundCheckRadius = 10f;
        [SerializeField] protected LayerMask GroundType;
        bool isGrounded;
        private bool isCanJump = true;

        [Header("----Wallcheck----")]
        [SerializeField] protected Transform wallCheckPos;
        [SerializeField] protected LayerMask wallLayer;

        [Header("----Detection----")]
        [SerializeField] protected Transform itemCheckPos;
        [SerializeField] protected float objectDetectionRadius = 10f;
        [SerializeField] protected LayerMask interactType;
        [SerializeField] protected float raycastOriginOffset;
        InteractableBehaviour currentInteract;
        InteractableBehaviour currentDetectedInteract;
        protected Vector3 sphereCenterOffset;
        bool isInteracting;
        private bool isItemDetected;
        Vector3 moveValue;
        Vector3 lastInput;

        [Header("----Indicator UI----")]
        [SerializeField] protected float farDetectionRadius = 15f;
        [SerializeField] protected float indicatorFadeDuration = .5f;
        
        [Header("----Death----")]
        [HideInInspector] public bool Death;
        [SerializeField] GameObject[] ParticleDead;
        protected CharacterInput charInput;
        private Vector3 startPos;

        public bool IsGrounded => isGrounded;

        public InteractableBehaviour CurrentInteract => currentInteract;
        public float CharSpeed
        {
            get { return charSpeed; }
            set { charSpeed = value; }
        }
        public Vector3 MoveValue => moveValue;
        public Vector3 LastInput => lastInput;
        public bool IsInteracting
        {
            get { return isInteracting; }
            set { isInteracting = value; }
        }
        public CinemachineVirtualCamera CameraV
        {
            get { return cameraV; }
            set { cameraV = value; }
        }

        public bool IsCanJump
        {
            get { return isCanJump; }
            set { isCanJump = value; }
        }

        public bool IsItemDetected
        {
            get { return isItemDetected; }
        }

        public event Action OnDead;


        protected virtual void Awake()
        {
            SetupVariable();
        }

        protected virtual void FixedUpdate()
        {
            //rb.velocity = moveValue * charSpeed;
            MovementAndGroundDetection();
            CheckCloseInteractable();
            CheckFarInteractable();
        }

        void SetupVariable()
        {
            charInput = ControlManager.Instance.CharacterInputAction;
            charInput.Player.Enable();
            charInput.Player.Movement.performed += CharacterMove;
            charInput.Player.Movement.canceled += CharacterStop;
            charInput.Player.Interact.performed += InteractWithObject;
            if (rb == null)
            {
                rb = GetComponent<Rigidbody>();
            }

            startPos = this.transform.position;
        }

        protected virtual void InteractWithObject(InputAction.CallbackContext context)
        {
            sphereCenterOffset = lastInput.normalized * 5;
            Vector3 currentPos = itemCheckPos.position + sphereCenterOffset;
            if (Physics.CheckSphere(currentPos, objectDetectionRadius, interactType) && ControlManager.Instance.CurrentBody == this)
            {
                Collider[] colliders = Physics.OverlapSphere(currentPos, objectDetectionRadius);
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject.GetComponent<Iinteractable>() != null && !collider.gameObject.GetComponent<JumpInteract>())
                    {
                        currentInteract = collider.gameObject.GetComponent<InteractableBehaviour>();
                        currentInteract.InteractStart(this);
                    }
                }
            }
        }
        protected virtual void CharacterMove(InputAction.CallbackContext context)
        {
            if (ControlManager.Instance.CurrentBody == this)
            {
                moveValue = new Vector3(context.ReadValue<Vector2>().x, 0, context.ReadValue<Vector2>().y);
                lastInput = new Vector3(moveValue.x, 0, moveValue.z);
            }

        }
        protected virtual void CharacterStop(InputAction.CallbackContext context)
        {
            if(rb!=null)
                rb.velocity = Vector3.zero;
            moveValue = Vector3.zero;
        }
        public virtual void Dead()
        {
            OnDead?.Invoke();
            gameObject.SetActive(false);
            for (int i = 0; i < ParticleDead.Length; i++)
            {
                Instantiate(ParticleDead[i], transform.position, Quaternion.identity);
            }
        }

        protected virtual void ResetPosition()
        {
            this.transform.position = startPos;
        }

        public void EnableControl()
        {
            charInput.Player.Enable();
            Debug.Log("Control Enabled: " + this.gameObject.name);
        }
        public void DisableControl()
        {
            rb.velocity = Vector3.zero;
            moveValue = Vector3.zero;
            charInput.Player.Disable();
            Debug.Log("Control Disabled: " + this.gameObject.name);

        }
        public void Knockback(Vector3 forceDirection, float knockBackForce)
        {
            DisableControl();
            rb.AddForce(forceDirection * knockBackForce, ForceMode.Impulse);
            Invoke("StopKnockback", 1f);
        }
        private void StopKnockback()
        {
            EnableControl();
            rb.velocity = Vector3.zero;
        }
        private void MovementAndGroundDetection()
        {
            if (!useGroundCheck) return;

            RaycastHit hit;
            isGrounded = Physics.Raycast(CheckPos.position, Vector3.down, out hit, groundCheckRadius, GroundType);
            if (isGrounded)
            {
                SnapPlayerToGround(hit);
                rb.velocity = Vector3.ProjectOnPlane(moveValue, hit.normal).normalized * charSpeed;
            }
            Debug.DrawRay(CheckPos.position, Vector3.down * groundCheckRadius, Color.blue);
        }
        private void SnapPlayerToGround(RaycastHit groundHit)
        {
            if (Mathf.Abs(transform.position.y - groundHit.point.y) > onGroundRange)
            {
                Debug.Log("Character Body: Snapping To " + groundHit.point.y);
                transform.position = new Vector3(transform.position.x, groundHit.point.y, transform.position.z);
            }
        }

        public bool IsWallDetected(float wallCheckLength)
        {
            return Physics.Raycast(wallCheckPos.position, moveValue, wallCheckLength, wallLayer) || Physics.Raycast(wallCheckPos.position, -moveValue, wallCheckLength, wallLayer);
        }

        private void CheckCloseInteractable()
        {
            //sphereCenterOffset = lastInput.normalized * 5;
            //Vector3 currentPos = itemCheckPos.position + sphereCenterOffset;

            if (!IsGrounded) return;

            SetRayOrigin();

            isItemDetected = Physics.Raycast(itemCheckPos.position, lastInput.normalized, out RaycastHit hit, lastInput.magnitude * objectDetectionRadius, interactType);
            if (isItemDetected && currentDetectedInteract == null && hit.transform.GetComponent<InteractableBehaviour>())
            {
                Debug.Log($"Check Close Interactable: {isItemDetected}");
                currentDetectedInteract = hit.transform.GetComponent<InteractableBehaviour>();
                currentDetectedInteract.ShowIcon(this);
            }
            else if (!isItemDetected && currentDetectedInteract != null)
            {
                currentDetectedInteract.HideIcon(this);

                if (currentDetectedInteract == null) return;
                currentDetectedInteract = null;
            }

            Debug.DrawRay(itemCheckPos.position, lastInput.normalized * objectDetectionRadius, Color.magenta);
        }

        private void SetRayOrigin()
        {
            //Check last input x
            if (lastInput.x > 0) itemCheckPos.localPosition = new Vector3(-raycastOriginOffset, -1.2f, 0f);
            else if (lastInput.x < 0) itemCheckPos.localPosition = new Vector3(raycastOriginOffset, -1.2f, 0f);

            //Check last input y
            if (lastInput.z > 0) itemCheckPos.localPosition = new Vector3(0f, -1.2f, -raycastOriginOffset);
            else if (lastInput.z < 0) itemCheckPos.localPosition = new Vector3(0f, -1.2f, raycastOriginOffset);
        }

        private void CheckFarInteractable()
        {
            if (!isItemDetected)
            {
                FadeIndicator(1f);
            }
            else
            {
                FadeIndicator(0f);
            }
        }

        private void FadeIndicator(float _indicatorFadeValue)
        {
            Collider[] interactables = Physics.OverlapSphere(this.transform.position, farDetectionRadius, interactType);

            foreach (Collider col in interactables)
            {
                if (!col.GetComponent<InteractableBehaviour>()) return;

                InteractableBehaviour interactable = col.GetComponent<InteractableBehaviour>();
                for (int i = 0; i < interactable.EyesIndicator.Length; i++)
                {
                    interactable.CheckPlayerDistance(this, farDetectionRadius, _indicatorFadeValue);
                }
            }
        }

        private void OnDestroy()
        {
            charInput.Player.Movement.performed -= CharacterMove;
            charInput.Player.Movement.canceled -= CharacterStop;
            charInput.Player.Interact.performed -= InteractWithObject;
        }

        private void OnDrawGizmos()
        {
            //Far interact detection
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(this.transform.position, farDetectionRadius);
        }
    }
}
