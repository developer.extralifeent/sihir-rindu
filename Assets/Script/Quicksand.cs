using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using OLJ.Utility;
using Spine.Unity;
using OLJ.UI;
using MyBox;
using OLJ;

public class Quicksand : MonoBehaviour
{
    bool isPulled, isStuck, isCooldown;
    CharacterBody cb;

    [SerializeField] SpineAnimationData spa;
    [SerializeField] float force = 30f;
    [SerializeField] float distanceToCenter = 18f;
    [SerializeField] float cooldownTime = 3f;

    [System.Serializable] public class OnQuicksandEvent : UnityEvent { }
    public OnQuicksandEvent onStuckOnCenter;
    public OnQuicksandEvent onExitFromStuck;

    private const string id = "QTE";
    private const string qteAnimation = "cain_quicksand";

    public void Start()
    {
        OLJ.Manager.QTEManager.Instance.onQTEfinished.AddListener(ExitFromStuck);
       
    }
    void ExitFromStuck(int idx) {
        onExitFromStuck?.Invoke();
        isStuck = false;
        isCooldown = true;
        if (QTEUIManager.Instance) {
            QTEUIManager.Instance.HideQTEAnimation();
        }
        OLJ.Manager.ControlManager.Instance.EnableControl();
        StartCoroutine(StuckCooldown());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharacterBody>() && other.CompareTag("Player"))
        {
            cb = other.GetComponent<CharacterBody>();
            isPulled = true;
        }
    }
    IEnumerator StuckCooldown() {
        yield return new WaitForSeconds(cooldownTime);
        isCooldown = false;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CharacterBody>() && other.CompareTag("Player"))
        {
            cb = null;
            isPulled = false;
        }
    }

    public void FixedUpdate()
    {
        if (isPulled)
        {
            Vector3 direction = transform.position - cb.transform.position;
            direction = new Vector3(direction.x, 0f, direction.z);
            cb.transform.position += direction.normalized * force;

            /*Vector3 position = Vector3.MoveTowards(cb.transform.position, transform.position, force * Time.fixedDeltaTime);
            cb.transform.position = new Vector3(position.x, cb.transform.position.y, position.z);*/

            //ini buat cek proximity sama tengah2 buat QTE


            float distanceX = Mathf.Abs(cb.transform.position.x - transform.position.x);
            float distanceZ = Mathf.Abs(cb.transform.position.z - transform.position.z);
            if (distanceX < distanceToCenter && distanceZ < distanceToCenter && !isStuck && !isCooldown)
            {
                isStuck = true;
                Debug.Log("Quicksand: stuck");
                OLJ.Manager.ControlManager.Instance.DisableControl();
                OLJ.Manager.QTEManager.Instance.StartQTE(0);
                QTEUIManager.Instance.SetQTEAnimation(spa.sk, qteAnimation);
                if (QTEUIManager.Instance && spa.animName.Length > 0 && spa.sk) {
                    QTEUIManager.Instance.SetQTEAnimation(spa.sk, spa.animName);
                }
                onStuckOnCenter?.Invoke();
            }
        }
    }
}
