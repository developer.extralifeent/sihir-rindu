using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using OLJ.Utility;
using MyBox;

namespace OLJ {
    [System.Serializable]
    public class SpineAnimationData : ISerializationCallbackReceiver {
        private const string id = "QTE";
        public SkeletonDataAsset sk;
        [SpineAnimation(dataField: "sk")] public string animName;
        bool isSerialized = false;

        public void OnAfterDeserialize() {
            isSerialized = true;           
           
        }

        public void OnBeforeSerialize() {
            if (!isSerialized) {
                SpineVariableSO[] allSpineVariable = Resources.LoadAll<SpineVariableSO>("");
                // Iterate through each asset path
                foreach (SpineVariableSO svso in allSpineVariable) {
                    if (svso.id == id) {
                        sk = svso.skeletonAnim;
                    }
                }
            }
        }
    }
}

