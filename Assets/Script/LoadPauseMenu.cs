using Fungus;
using OLJ;
using OLJ.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace OLJ.Manager
{
    public class LoadPauseMenu : Singleton<LoadPauseMenu>
    {
        private GameManager gameManager;

        public bool Ispaused { get; private set; }

        [SerializeField] private ControlManager controlManager;

        public Action onPausedToggled;

        private void OnEnable()
        {
            gameManager = GameManager.Instance;

            controlManager.CharacterInputAction.Player.Pause.performed += TogglePauseViaKey;
            controlManager.CharacterInputAction.UI.Cancel.performed += TogglePauseViaKey;
        }

        private void OnDisable()
        {
            controlManager.CharacterInputAction.Player.Pause.performed -= TogglePauseViaKey;
            controlManager.CharacterInputAction.UI.Cancel.performed -= TogglePauseViaKey;
        }

        private void TogglePauseViaKey(InputAction.CallbackContext _ctx)
        {
            Debug.Log("Pause button pressed");

            if (SceneManager.GetSceneByName(gameManager.TitleMenu).isLoaded) return;

            TogglePause();

            onPausedToggled?.Invoke();
        }

        public void TogglePause()
        {
            Ispaused = !Ispaused;
            Time.timeScale = SetTimeScale();

            if (Ispaused)
                controlManager.SwitchInputAction(controlManager.CharacterInputAction.UI);
            else
                controlManager.SwitchInputAction(controlManager.CharacterInputAction.Player);
        }

        private float SetTimeScale() => Ispaused ? 0f : 1f;
    }
}
