using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsedOnceFieldButton : MonoBehaviour
{
    [SerializeField] float MoveAmount;
    [SerializeField] float Duration;
    [SerializeField] float UnlockDoor = 0;
    public void UsedButton() {
        UnlockDoor += 1;
        if (UnlockDoor == 3)
        {
            StartCoroutine(WaitTime());
        }
    }
    public void BackToPosition() {
        LeanTween.moveX(gameObject, gameObject.transform.position.x - MoveAmount, Duration);
    }
    public void DisableTrigger()
    {
        UnlockDoor -= 1;
    }
    IEnumerator WaitTime() {
        yield return new WaitForSeconds(1);
        LeanTween.moveX(gameObject, gameObject.transform.position.x + MoveAmount, Duration);
    }
}
