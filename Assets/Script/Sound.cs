using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip soundAudio;
    [Range(0,1)]
    public float Volume;
    [Range(0, 1)]
    public float Pitch;
    public bool loop;
    [HideInInspector]
    public AudioSource source;
}
