using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckingPuzzle : MonoBehaviour
{
    public OnComplete onComplete;
    [System.Serializable] public class OnComplete : UnityEvent { }
    [SerializeField] int RequirementToSolve;
    private int Require;
    private void Awake()
    {
        if (onComplete == null) onComplete = new OnComplete();
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("PuzleReq") == RequirementToSolve)
        {
            onComplete.Invoke();
        }
    }
    public void FinishRequirement()
    {
        Require++;
        PlayerPrefs.SetInt("PuzleReq", Require);
    }
}
