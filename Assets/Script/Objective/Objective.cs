using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MyBox;

    public class Objective : MonoBehaviour
    {
        public enum Trigger { PrequisiteComplete, Manual }
        [SerializeField] protected Trigger trigger;
        [SerializeField, ConditionalField(nameof(trigger), false, Trigger.PrequisiteComplete)] Objective prequisite;
        [SerializeField, ConditionalField(nameof(trigger), false, Trigger.PrequisiteComplete)] float prequisiteDelay = 0f;

        public ObjectiveEvent onObjectiveStart;
        public ObjectiveEvent onObjectiveFinish;

        [System.Serializable] public class ObjectiveEvent : UnityEvent<Objective> { }
        
        public bool Active { get; protected set; }
        public bool IsFinished { get; protected set; }

        protected virtual void Awake()
        {
            if (onObjectiveStart == null) onObjectiveStart = new ObjectiveEvent();
            if (onObjectiveFinish == null) onObjectiveFinish = new ObjectiveEvent();

            if(trigger == Trigger.PrequisiteComplete)
			{
                prequisite.onObjectiveFinish.AddListener(delegate
                {
                    if (prequisiteDelay > 0) Invoke(nameof(ObjectiveStart), prequisiteDelay);
                    else ObjectiveStart();
                });
			}
        }

        public virtual void ObjectiveStart()
        {
            if (!Active && !IsFinished)
            {
                Active = true;
                IsFinished = false;

                Debug.Log("Objective" + gameObject.name + "started");
                onObjectiveStart.Invoke(this);
            }
        }

        public virtual void ObjectiveFinished()
        {
            if (Active && !IsFinished)
            {
                Active = false;
                IsFinished = true;

                Debug.Log("Objective" + gameObject.name + "finished");
                onObjectiveFinish.Invoke(this);
            }
        }

        protected virtual void ForceComplete()
        {
            Active = true;
            ObjectiveFinished();
        }
    }

