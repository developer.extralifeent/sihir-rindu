using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;


namespace SihirRindu.Puzzle {   
    public class DoorUnlocker : Objective
    {
        [SerializeField] float MoveAmount;
        [SerializeField] float Duration;
        [SerializeField, ReadOnly] float UnlockDoor = 0;
        [SerializeField] float maxKey = 0;

        public override void ObjectiveStart()
		{
            base.ObjectiveStart();
		}

        public void UsedButton()
        {
            UnlockDoor += 1;
            Debug.Log("Door Key: " + UnlockDoor);
            if (UnlockDoor == maxKey)
            {
                ObjectiveFinished();
            }
        }      
        public void DisableTrigger()
        {
            UnlockDoor -= 1;
        }
       
    }
}
