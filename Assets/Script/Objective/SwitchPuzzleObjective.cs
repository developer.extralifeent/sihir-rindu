using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using Malee.List;
using MyBox;


public class SwitchPuzzleObjective : MonoBehaviour {
	[SerializeField] SwitchPuzzle stone1;
	[SerializeField] SwitchPuzzle stone2;
	[SerializeField] SwitchPuzzle stone3;

	//Events
	[System.Serializable] public class OnStartObjectives : UnityEvent { }
	[System.Serializable] public class OnCompletedObjectives : UnityEvent { }

	public OnStartObjectives onObjectiveStart;
	public OnCompletedObjectives onObjectiveComplete;

	private  void Awake() {
		if (onObjectiveComplete == null) onObjectiveComplete = new OnCompletedObjectives();
		if (onObjectiveStart == null) onObjectiveStart = new OnStartObjectives();
	}

	public void ObjectiveStart() {
		onObjectiveStart.Invoke();
		if (stone1.Toogle == true && stone2.Toogle == true && stone3.Toogle == true) {
			ObjectiveComplete();
		}

	}
	public void ObjectiveComplete() {
		onObjectiveComplete.Invoke();
		Debug.Log("Objective Complete");
    }
}


