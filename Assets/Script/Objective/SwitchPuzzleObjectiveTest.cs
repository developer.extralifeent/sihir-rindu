using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using Malee.List;
using MyBox;

namespace SihirRindu.Puzzle
{
	public class SwitchPuzzleObjectiveTest: Objective
	{

		[SerializeField, Reorderable] protected SwitchStoneList stoneList;

		//public enum Result { Low = 1, Medium = 2, High = 3 };
		//int lowResult = (int)Result.Low;

		bool puzzleComplete = false;

		protected override void Awake()
		{
			base.Awake();
		}


		public override void ObjectiveStart()	
		{
			base.ObjectiveStart();
			
			if (puzzleComplete == true)
			{
				ObjectiveFinished();
			}
			
		}


		protected void Update()
		{
			if (Active && !IsFinished)
			{
				if (puzzleComplete == true)
				{
					ObjectiveFinished();
				}
			}
		}

		[System.Serializable] public class SwitchStoneList : ReorderableArray<SwitchStones> { }
		[System.Serializable] public class SwitchStones
		{
			public SwitchPuzzle stones;
			[ReadOnly] public int limit;
			[ReadOnly] public int maxLimit;
		}

	}
}

