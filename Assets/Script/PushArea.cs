using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static OLJ.UI.UIManager;

public class PushArea : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float pushForce;

    private void OnTriggerStay(Collider other)
    {
        if(other.TryGetComponent(out PlayerController _player))
        {
            _player.transform.position += this.transform.forward * pushForce * Time.deltaTime;
        }
    }

    //public void ChangePushDir() => pushDir *= -1f;

    //public void ResetPushDir() => pushDir = -1f;
}
