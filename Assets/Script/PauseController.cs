using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour
{
    public void UnfrezeeTime() {
        Time.timeScale = 1;
    }
    
    public void ResumeGame() {
        Time.timeScale = 1;
        SceneManager.UnloadSceneAsync("Pause Menu");
    }

    public void RestartLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }
}
