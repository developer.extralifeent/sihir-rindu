using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLJ.Gameplay.Interactable;
using DG.Tweening;

namespace OLJ.UI {
    public class MonsterOutButton : MonoBehaviour {
        [SerializeField] CanvasGroup cg;
        [SerializeField] Image fillIndicator;
        ControlInteract controlInt;
        float valueParam;
        private void Start() {
            controlInt = GetComponentInParent<ControlInteract>();
            controlInt.onPressedButton.AddListener(ActivateButton);
            controlInt.onReleaseButton.AddListener(DeactivateButton);
        }
        void ActivateButton(float value,float holdvalue) {
            cg.DOFade(1, 0.2f);
            valueParam = value / holdvalue;
            fillIndicator.fillAmount = valueParam;
        }
        void DeactivateButton(float value,float holdvalue) {
            cg.DOFade(0, 0.5f);
            
        }
    }
}

