using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    private CursorManager cursorManager;

    private void Start()
    {
        cursorManager = CursorManager.Instance;
        Debug.Log("Cursor Changer Spawned");
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        cursorManager.isCursorHovering = true;
        cursorManager.ChangeHoverCursor();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        cursorManager.isCursorHovering = false;
        cursorManager.ChangeNormalCursor();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        cursorManager.ChangeClickCursor();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (cursorManager.isCursorHovering)
            cursorManager.ChangeHoverCursor();
        else
            cursorManager.ChangeNormalCursor();
    }
}
