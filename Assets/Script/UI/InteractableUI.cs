using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using OLJ.Gameplay.Interactable;
using OLJ.Character;

namespace OLJ.UI {
    public class InteractableUI : MonoBehaviour {
        [SerializeField] CanvasGroup iconParent;
        [SerializeField] Image icon;
        private CanvasGroup characterCanvasGroup;
        private Image characterIndicatorImg;

        private void Start()
        {
            if (GetComponentInParent<InteractableBehaviour>())
            {
                GetComponentInParent<InteractableBehaviour>().onIconShowing += ShowIcon;
                GetComponentInParent<InteractableBehaviour>().onIconHiding += HideIcon;
            }
        }

        private void OnDestroy()
        {
            if (GetComponentInParent<InteractableBehaviour>())
            {
                GetComponentInParent<InteractableBehaviour>().onIconShowing -= ShowIcon;
                GetComponentInParent<InteractableBehaviour>().onIconHiding -= HideIcon;
            }
        }

        private void ShowIcon(CharacterBody _body)
        {
            if (characterCanvasGroup == null)
                characterCanvasGroup = _body.GetComponentInChildren<CanvasGroup>();

            if(characterIndicatorImg == null)
                characterIndicatorImg = characterCanvasGroup.transform.GetChild(1).GetComponentInChildren<Image>();
            
            characterIndicatorImg.sprite = icon.sprite;
            characterCanvasGroup.DOFade(1f, .5f);
        }

        private void HideIcon(CharacterBody _body)
        {
            Debug.Log("Hiding Icon");

            if (characterCanvasGroup == null)
                characterCanvasGroup = _body.GetComponentInChildren<CanvasGroup>();

            if (characterIndicatorImg == null)
                characterIndicatorImg = characterCanvasGroup.transform.GetChild(1).GetComponentInChildren<Image>();

            characterCanvasGroup.DOFade(0f, .5f);
        }

        //void ShowIcon()
        //{
        //    iconParent.DOFade(1, 0.5f);
        //}
        //void HideIcon()
        //{
        //    iconParent.DOFade(0, 0.5f);
        //}
    }

}
