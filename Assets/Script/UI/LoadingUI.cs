using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLJ.Manager;

public class LoadingUI : MonoBehaviour
{
    [SerializeField] Slider loadingbar;
    private void Start() {
        GameManager.Instance.onLoadingProgress += LoadingBar;
    }
    void LoadingBar()
    {
        if(GameManager.Instance)
        loadingbar.value = GameManager.Instance.Proggress / 0.9f;
    }
}
