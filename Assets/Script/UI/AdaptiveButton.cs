using OLJ.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AdaptiveButton : MonoBehaviour, IPointerEnterHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        PauseUIManager.Instance.LastSelectedButton = this.GetComponent<Button>();
        PauseUIManager.Instance.SetAllButtonState();
    }
}
