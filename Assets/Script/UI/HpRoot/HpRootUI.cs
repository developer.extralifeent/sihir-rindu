using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.UI
{
    public class HpRootUI : MonoBehaviour
    {
        [Separator("Root Orientation")]
        [SerializeField] private ROOT_ORIENTATION rootOrientation; 

        public ROOT_ORIENTATION RootOrientation => rootOrientation;

        public Vector2 StartPos { get; private set; }

        private void Start()
        {
            StartPos = this.GetComponent<RectTransform>().position;
        }
    }

    public enum ROOT_ORIENTATION
    {
        Horizontal,
        Vertical,
        Corner
    }
}
