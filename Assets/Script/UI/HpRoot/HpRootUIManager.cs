using DG.Tweening;
using MyBox;
using OLJ.Manager;
using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace OLJ.UI
{
    public class HpRootUIManager : Singleton<HpRootUIManager>
    {
        private HPManager hpManager;

        [Separator("Root Objects")]
        [SerializeField] private HpRootUI[] hpRootUI;
        public HpRootUI[] HpRootUI => hpRootUI;
        private Coroutine rootAnimCoroutine;

        [Separator("Vignette")]
        [SerializeField] private Volume vignetteVolume;
        [SerializeField] private float maxVignetteIntensity = .65f;

        [Separator("Spine Anim")]
        [SpineAnimation, SerializeField] private string hitAnim;
        [SpineAnimation, SerializeField] private string loopAnim;

        private void OnEnable()
        {
            hpManager = HPManager.Instance;
        }

        // Start is called before the first frame update
        void Start()
        {
            hpManager.onHpChanged += MoveRoots;
            hpManager.hpRootUIManager = this;
        }

        private void OnDisable()
        {
            hpManager.onHpChanged += MoveRoots;
        }

        public void MoveRoots(float _currentHp, float _maxHp)
        {
            rootAnimCoroutine = StartCoroutine(MoveRootsCoroutine(_currentHp, _maxHp));
        }
    
        private IEnumerator MoveRootsCoroutine(float _currentHp, float _maxHp)
        {
            float time = 0f;
            float maxTime = _currentHp / _maxHp;
            float hpPercentage = (_maxHp - _currentHp) / 100f;

            while (time < maxTime)
            {

                CheckVignetteVolume((Vignette _vignette) =>
                {
                    _vignette.intensity.value = maxVignetteIntensity * hpPercentage;
                });

                foreach (var root in hpRootUI)
                {
                    Debug.Log(root.RootOrientation + " root : " + root.StartPos);  

                    switch (root.RootOrientation)
                    {
                        case ROOT_ORIENTATION.Vertical:
                            //root.GetComponent<RectTransform>().position = Vector2.Lerp(root.transform.position, new Vector2(root.transform.position.x, screenVerticalCenter.y * (_maxHp - _currentHp) / 100f), 1f - maxTime);
                            root.GetComponent<RectTransform>().DOMoveY(root.StartPos.y - (root.StartPos.y * hpPercentage), 1f - maxTime);
                            break;
                        case ROOT_ORIENTATION.Horizontal:
                            //root.GetComponent<RectTransform>().position = Vector2.Lerp(root.transform.position, new Vector2(Screen.width - (screenHorizontalCenter.x * (_maxHp - _currentHp) / 100f), root.transform.position.y), 1f - maxTime);
                            root.GetComponent<RectTransform>().DOMoveX(root.StartPos.x - (root.StartPos.x * hpPercentage), 1f - maxTime);
                            break;
                        case ROOT_ORIENTATION.Corner:
                            root.GetComponent<RectTransform>().DOMoveY(root.StartPos.y - (root.StartPos.y * hpPercentage), 1f - maxTime);
                            root.GetComponent<RectTransform>().DOMoveX(root.StartPos.x - (root.StartPos.x * hpPercentage), 1f - maxTime);
                            break;
                    }  
                }
                time += Time.deltaTime;
                yield return null;
            }
        }

        private void CheckVignetteVolume(System.Action<Vignette> _onVignetteFound)
        {
            if (vignetteVolume.profile.TryGet(out Vignette _vignette))
            {
                Debug.Log("Vignette Found");
                _onVignetteFound.Invoke(_vignette);
            }
        }

        public void PlaySpineAnim() => StartCoroutine(PlaySpineAnimCoroutine());

        private IEnumerator PlaySpineAnimCoroutine()
        {
            StopCoroutine(PlaySpineAnimCoroutine());

            LoopThroughHpRootUI(hitAnim, false);
            Debug.Log("Played get-hit animation");

            yield return new WaitForSeconds(1f);

            LoopThroughHpRootUI(loopAnim, true);
            Debug.Log("Played loop animation");
        }

        private void LoopThroughHpRootUI(string _animName, bool _loop)
        {
            foreach (HpRootUI hpRoot in hpRootUI)
            {
                hpRoot.GetComponentInChildren<SkeletonAnimation>().AnimationState.SetAnimation(0, _animName, _loop);
            }
        }
    }
}
