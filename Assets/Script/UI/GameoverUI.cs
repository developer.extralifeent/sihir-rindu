using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using OLJ.Manager;

namespace OLJ.UI {
    public class GameoverUI : MonoBehaviour {
        [SerializeField] CanvasGroup gameOverUI;
        private void Start() {
            gameOverUI.alpha = 0;
            if (GameManager.Instance) {
                GameManager.Instance.onGameOver += ShowGameOverPanel;
            }
        }
        void ShowGameOverPanel() {
            gameOverUI.DOFade(1, 1f);
        }
    }
}

