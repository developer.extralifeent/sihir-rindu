using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLJ.Character;
using DG.Tweening;

namespace OLJ.UI {
    public class CharacterUIController : MonoBehaviour {
        [SerializeField] Image iconHolder;
        [SerializeField] Image PickHolder;
        [SerializeField] List<Sprite> iconSprite;    

        public void SetPickIcon(Sprite sp) {
            PickHolder.gameObject.SetActive(true);
            PickHolder.sprite = sp;
        }
        public void UnSetPickIcon() {
            PickHolder.gameObject.SetActive(false);
        }
        public void SetIcon(int index) {
            if (iconSprite.Count > index) {
                iconHolder.gameObject.SetActive(true);
                iconHolder.sprite = iconSprite[index];
            }
        }
        public void HideIcon() {
            iconHolder.gameObject.SetActive(false);
        }
    }
}

