using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using Malee.List;
using System.Linq;

namespace OLJ.UI
{
	public class SelectionMenu : MonoBehaviour
	{
		[SerializeField] protected Button buttonPrefab;
		[SerializeField] protected float fadeDuration = .5f;
		[SerializeField] protected Transform buttonContainer;

		[SerializeField, Reorderable] protected MenuButtonList buttonList;

		public float FadeDuration { get => fadeDuration; }

		protected TitleMenuController tmc;
		protected Dictionary<Button, MenuButton> buttons;
		protected CanvasGroup canvasGroup;

		protected virtual void Awake()
		{
			canvasGroup = GetComponent<CanvasGroup>();
			canvasGroup.alpha = 0f;

			buttons = new Dictionary<Button, MenuButton>();

			InitializeButtons();

			tmc = TitleMenuController.Instance;
		}

		public virtual void Show()
		{
			LeanTween.alphaCanvas(canvasGroup, 1, fadeDuration);
		}

		public virtual void Hide()
		{
			LeanTween.alphaCanvas(canvasGroup, 0, fadeDuration);
		}

		protected virtual void InitializeButtons()
		{
			if (buttonList != null && buttonList.Count > 0)
			{
				foreach (MenuButton mb in buttonList)
				{
                    Button butt = Instantiate(buttonPrefab, buttonContainer);
					butt.name = mb.name;

					List<TextMeshProUGUI> texts = new List<TextMeshProUGUI>(butt.GetComponentsInChildren<TextMeshProUGUI>());
					foreach (TextMeshProUGUI text in texts)
					{
						text.text = mb.name;
					}

					MenuButton temp = mb;
					butt.onClick.AddListener(() =>
					{
						ToggleButtons(false);
						if (temp.onExecute != null)
						{
							temp.onExecute.Invoke();
						}
					});

                    if (butt.name == "Settings")
                        butt.gameObject.SetActive(false);

                    butt.gameObject.SetActive(!mb.hidden);
					buttons.Add(butt, mb);
				}
			}
		}

		public virtual void ToggleButtons(bool set)
		{
			foreach (Button button in buttons.Keys)
			{
				MenuButton mb = buttons[button];

				button.interactable = set && mb.enabled;
				button.gameObject.SetActive(!mb.hidden);
			}
			if (set) SelectFirst();
		}

		public void ToggleMainMenuButton()
		{
            foreach (Button button in buttons.Keys)
            {
                if (button.name == "Settings")
                {
                    button.interactable = false;
                    button.gameObject.SetActive(false);
                }

                button.interactable = true;
                button.gameObject.SetActive(true);
            }

			Debug.Log("Button Toggled");
        }

		public virtual void HighlightButton(Button button)
		{
			if (!tmc) tmc = TitleMenuController.Instance;
			//if (buttons.ContainsKey(button))
			//	tmc.ShowDescription(buttons[button].description);
		}

        void SelectFirst()
        {
            foreach (Button button in buttons.Keys)
            {
                if (button.interactable)
                {
                    button.Select();
                    break;
                }
            }
        }

        [System.Serializable] public class MenuButtonList : ReorderableArray<MenuButton> { }

		[System.Serializable]
		public class MenuButton
		{
			public string name = "Menu Button";
			public bool enabled = true;
			public bool hidden = false;
			public UnityEvent onExecute;
		}
	}
}


