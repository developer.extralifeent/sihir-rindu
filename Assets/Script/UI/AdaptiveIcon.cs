using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using OLJ.Manager;
using MyBox;

namespace OLJ.UI {
    public class AdaptiveIcon : MonoBehaviour,ISerializationCallbackReceiver {
        [SerializeField] Image icon;
        [SerializeField] InputActionReference inputAct;
		[SerializeField, ReadOnly] List<string> ipPath;
		[SerializeField, ReadOnly] string selectedPath;

        private void Start() {
			OnControlSchemeChanged(ControlManager.Instance.CurrentControlScheme);
			ControlManager.Instance.onControlSchemeChanged.AddListener(OnControlSchemeChanged);
        }
      
        public void OnControlSchemeChanged(string bind) {
			Debug.Log("Adaptive Control: " + bind);
			foreach(string ip in ipPath) {
                if (ip.Contains(bind)) {
					selectedPath = ip;
                }
            }
			icon.sprite = ControlManager.Instance.GetIcon(selectedPath);

		}

		public void OnBeforeSerialize() {
			
			if (inputAct && inputAct.action != null) {
				ipPath.Clear();
				foreach(InputBinding ib in inputAct.action.bindings) {
					ipPath.Add(ib.effectivePath);
                }				
			}
		}

		public void OnAfterDeserialize() {
			
		}

	}
}

