using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace OLJ.UI {
    public class CutsceneBorder : MonoBehaviour {
        [SerializeField] RectTransform upperBound;
        [SerializeField] RectTransform lowerBound;
        [SerializeField] float endPosition = 100;
        Vector3 initialUpperBoundPos;
        Vector3 initialLowerBoundPos;
        private void Start() {
            initialUpperBoundPos = upperBound.position;
            initialLowerBoundPos = lowerBound.position;
        }
        public void PlayBorder() {
            upperBound.position = initialUpperBoundPos;
            lowerBound.position = initialLowerBoundPos;
            GetComponent<CanvasGroup>().DOFade(1, 1f);
            upperBound.DOMove(upperBound.position + Vector3.down * endPosition, 1f);
            lowerBound.DOMove(lowerBound.position + Vector3.up * endPosition, 1f);
        }
        public void HideBorder() {
            GetComponent<CanvasGroup>().DOFade(0, 1f);
            upperBound.DOMove(upperBound.position + Vector3.up * endPosition, 1f);
            lowerBound.DOMove(lowerBound.position + Vector3.down * endPosition, 1f);
        }
    }

}

