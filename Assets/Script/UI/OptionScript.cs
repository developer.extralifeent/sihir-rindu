using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionScript : MonoBehaviour
{
	public Toggle fullScreenToggle, vSyncToggle;

	public List<ResItem> resolution = new List<ResItem>();
	private int selectedRes;

	public TextMeshProUGUI resLabel; 

	void Start()
	{
		fullScreenToggle.isOn = Screen.fullScreen;
		if(QualitySettings.vSyncCount == 0)
		{
			vSyncToggle.isOn = false;
		}

		else
		{
			vSyncToggle.isOn = true;
		}

		bool foundRes = false;
		for (int i = 0; i < resolution.Count; i++)
		{
			if(Screen.width == resolution[i].horizontal && Screen.height == resolution[i].vertical)
			{
				foundRes = true;
				selectedRes = i;
				UdpateResText();
			}
		}

		if (!foundRes)
		{
			ResItem newRes = new ResItem();
			newRes.horizontal = Screen.width;
			newRes.vertical = Screen.height;

			resolution.Add(newRes);
			selectedRes = resolution.Count - 1;

			UdpateResText();
		}
	}

	public void ResLeft()
	{
		selectedRes -= 1;
		if (selectedRes < 0)
		{
			selectedRes = 0;
		}

		UdpateResText();
	}

	public void ResRight()
	{
		selectedRes += 1;
		if(selectedRes > resolution.Count - 1)
		{
			selectedRes = resolution.Count -  1;
		}

		UdpateResText();
	}

	public void UdpateResText()
	{
		resLabel.text = resolution[selectedRes].horizontal.ToString() + " X " + resolution[selectedRes].vertical.ToString();
	}

    public void ApplyGraphic()
	{
		Screen.fullScreen = fullScreenToggle.isOn;
		if (vSyncToggle.isOn)
		{ 
			QualitySettings.vSyncCount = 1;
		}
		else
		{
			QualitySettings.vSyncCount = 0;
		}

		Screen.SetResolution(resolution[selectedRes].horizontal, resolution[selectedRes].vertical, fullScreenToggle.isOn);
	}
}

[System.Serializable]
public class ResItem
{
	public int horizontal, vertical;
}
