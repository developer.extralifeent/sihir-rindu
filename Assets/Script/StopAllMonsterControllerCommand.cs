using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.Character;

[CommandInfo("AI",
                "Stops All monster",
                "Stops all monster on the scene.")]
[AddComponentMenu("")]
public class StopAllMonsterControllerCommand : Command
{
    [SerializeField] bool IsEnabled;
    private MonsterController[] MS;
    private void Start()
    {
        MS = FindObjectsOfType<MonsterController>();
    }
    public override void OnEnter()
    {
        if (IsEnabled)
        {
            foreach(MonsterController m in MS)
            {
                Debug.Log("Command: MonsterController true");
                m.Nav.isStopped = false;
                m.DisbleThis(IsEnabled);
               
            }
        }
        else
        {
            foreach (MonsterController m in MS)
            {
                Debug.Log("Command: MonsterController false");
                m.Nav.isStopped = true;
                m.DisbleThis(IsEnabled);
                m.Anime.SetFloat("Speed", 0);
                m.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
        Continue();
    }
}
