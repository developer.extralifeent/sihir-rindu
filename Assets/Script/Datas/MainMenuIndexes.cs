using System.Collections.Generic;
using UnityEngine;

namespace OLJ.PlayerSaveData
{
    [System.Serializable]
    public class MainMenuIndexes
    {
        public int currentSelectedChpt;
        public int currentSelectedSubchpt;
        public int buttonIndex;

        public FieldSO[] selectedChapterSO;
    }
}

