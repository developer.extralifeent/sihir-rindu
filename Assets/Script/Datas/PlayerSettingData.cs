using System;

namespace OLJ.PlayerSaveData
{
    [Serializable]
    public class PlayerSettingData
    {
        public int resolutionIndex;
        public bool vSync;
        public bool fullScreen;
    }
}
