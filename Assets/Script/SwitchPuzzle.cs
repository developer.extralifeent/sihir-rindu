using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class SwitchPuzzle : MonoBehaviour
{
	[SerializeField] public bool Toogle;
	[SerializeField] public GameObject VisualOn, VisualOff;

	bool Pressed;
    private void Update()
	{
		VisualControl();		
	}
	void VisualControl() {
       
		if (Toogle) {
			VisualOn.SetActive(true);
			VisualOff.SetActive(false);
		}
		else{
			VisualOff.SetActive(true);
			VisualOn.SetActive(false);
		}
    }
	public void OnButton()
	{
        if (Pressed==false) {
			Toogle = !Toogle;
			GetComponentInParent<SwitchPuzzleObjective>().ObjectiveStart();
			Debug.Log("Button Pressed: " + gameObject.name);
			Pressed = true;
		}
		
	}
	public void OffButton() {
		Pressed = false;
	}
	
	
}
