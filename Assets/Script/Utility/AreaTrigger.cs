using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AreaTrigger : MonoBehaviour
{
	public string tagFilter = "";
	public string layerFilter = "";

	public enum DisableType { None, OnEnter, OnExit }
	public DisableType disableOn = DisableType.None;

	public OnEnter onEnter;
	public OnStay onStay;
	public OnExit onExit;

	[System.Serializable] public class OnEnter : UnityEvent<Transform> { }
	[System.Serializable] public class OnStay : UnityEvent<Transform> { }
	[System.Serializable] public class OnExit : UnityEvent<Transform> { }

	void Awake() {
		if (onEnter == null) onEnter = new OnEnter();
		if (onExit == null) onExit = new OnExit();
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (IsValid(other.gameObject)) {
			Enter(other.transform);
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (IsValid(other.gameObject)) {
			Enter(other.transform);
		}
	}

    private void OnTriggerStay(Collider other)
    {
        if(IsValid(other.gameObject))
		{
			Stay(other.transform);
		}
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (IsValid(other.gameObject))
        {
            Stay(other.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
		if (IsValid(other.gameObject)) {
			Exit(other.transform);
		}
	}

	private void OnTriggerExit(Collider other) {
		if (IsValid(other.gameObject)) {
			Exit(other.transform);
		}
	}

	private bool IsValid(GameObject other) {
		return (tagFilter == "" || other.CompareTag(tagFilter)) &&
			(layerFilter == "" || other.layer == LayerMask.NameToLayer(layerFilter));
	}

	protected virtual void Enter(Transform transform) {
		onEnter.Invoke(transform);
		if (disableOn == DisableType.OnEnter) gameObject.SetActive(false);
	}

    protected virtual void Stay(Transform transform)
    {
        onStay.Invoke(transform);
    }

    protected virtual void Exit(Transform transform) {
		onExit.Invoke(transform);
		if (disableOn == DisableType.OnExit) gameObject.SetActive(false);
	}
}
