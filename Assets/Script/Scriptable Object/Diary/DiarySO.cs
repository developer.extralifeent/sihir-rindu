using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DiarySO", menuName = "OLJSO/DiarySO")]
public class DiarySO : ScriptableObject
{
    [TextArea(0,50)]
    public string description;
    public Sprite imageContain;
}
