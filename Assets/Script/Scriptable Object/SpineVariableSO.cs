using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

namespace OLJ.Utility {
    [CreateAssetMenu(fileName = "Spine Variable SO", menuName = "OLJSO/Spine Variable S0")]
    public class SpineVariableSO : ScriptableObject {
        public string id;
        public SkeletonDataAsset skeletonAnim;
    }
}

