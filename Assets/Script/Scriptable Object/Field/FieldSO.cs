using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FieldSO", menuName = "OLJSO/FielSO")]
public class FieldSO : ScriptableObject
{
    public int id;
    public GameObject fieldPrefab;
    [TextArea(3,10)]
    public string description;

    [Separator("Field Information")]
    public int chapterNumber = 1;
    public string chapterTitle = "Ini judul chapter";
    [TextArea(3, 10)] public string chapterSubtitle = "Di suatu malam yang gelap";
    public Sprite chapterImage;

    [Separator("Field Collectible")]
    public int totalCollectible = 4;
    public int collectibleCount;

    [Separator("Chapter config")]
    public bool isFinalPart;
}
