using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

[CreateAssetMenu(fileName = "InputSO", menuName = "OLJSO/InputSO")]
public class InputSO : ScriptableObject
{
	public string group;
	public List<ControlIcon> controlIcons;

}
[System.Serializable]
public class ControlIcon {
	public string path = "mouse/meow meow";     // Effective control path 
	public Sprite icon;                         // Sprite icon to be displayed
	public Sprite iconPressed;                  // Sprite icon to be displayed when the key is pressed
	public string altName = "Meow Meow";        // Alt name to be displayed above alt sprite if icon is null
}

