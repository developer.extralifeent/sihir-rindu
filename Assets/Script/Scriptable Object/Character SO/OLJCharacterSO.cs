using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterSO", menuName = "OLJSO/CharacterSO")]
public class OLJCharacterSO : ScriptableObject {  
    public string charName;
    public List<Sprite> sprite;

}


