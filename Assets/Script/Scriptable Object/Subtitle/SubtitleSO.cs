using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CreateAssetMenu(fileName = "SubtitleSO", menuName = "OLJSO/subtitleSO")]
public class SubtitleSO : ScriptableObject
{
    public List<SubtitleController> subController;
    [TextArea(3, 10)]
    public string description;
}
