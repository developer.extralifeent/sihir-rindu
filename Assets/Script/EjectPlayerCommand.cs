using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.Character;
[CommandInfo("Scripting",
               "Controlled Monster Move",
               "Eject Player From Monster")]
[AddComponentMenu("")]
public class EjectPlayerCommand : Command
{
    [SerializeField] float SpeedMove;
    [SerializeField] Transform PositionToMove;
    MonsterController[] Monsters;
    public override void OnEnter() {
        Monsters = FindObjectsOfType<MonsterController>();
        foreach(MonsterController monster in Monsters) {
            if (monster.ControlledByPlayer) {
                LeanTween.move(monster.gameObject, PositionToMove.position, SpeedMove);
            }
        }
        Continue();
    }
}
