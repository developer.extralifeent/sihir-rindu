using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Interface
{
    public interface IDestructible
    {
        public abstract void OnTakeDamage(float _damage);

        public abstract void OnDestroyed();
    }
}

