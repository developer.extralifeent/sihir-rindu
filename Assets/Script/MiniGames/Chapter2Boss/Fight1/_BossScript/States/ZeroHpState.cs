using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class ZeroHpState : BaseBossState
    {
        protected override IEnumerator CurrentStateCoroutine()
        {
            Debug.Log("Boss dead");
            yield return null;
        }
    }
}

