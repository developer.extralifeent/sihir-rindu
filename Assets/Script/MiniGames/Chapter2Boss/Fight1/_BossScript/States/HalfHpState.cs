using Fungus;
using OLJ.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class HalfHpState : BaseBossState
    {
        private const string halfHpBlockName = "Half Hp QTE";
        [SerializeField] private Flowchart halfHpFlowchart;

        protected override IEnumerator CurrentStateCoroutine()
        {
            IsCoroutineActive = true;

            Fight1GameManager.Instance.GameState = GameState.QTE;
            ControlManager.Instance.SwitchInputAction(ControlManager.Instance.CharacterInputAction.QTE);

            halfHpFlowchart.ExecuteBlock(halfHpBlockName);
            Debug.Log("Robot Weakened! Start Fungus Flowchart");
            yield return null;
        }
    }
}

