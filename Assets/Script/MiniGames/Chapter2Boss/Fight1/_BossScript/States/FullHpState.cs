using MyBox;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


namespace OLJ.MiniGame.Fight1
{
    public class FullHpState : BaseBossState
    {
        protected override IEnumerator CurrentStateCoroutine()
        {
            IsCoroutineActive = true;
            yield return new WaitForSeconds(attackCoolDown);

            //ATTACK LAUNCH MISSILE
            LaunchMissile();

            IsCoroutineActive = false;

        }
    }
}
