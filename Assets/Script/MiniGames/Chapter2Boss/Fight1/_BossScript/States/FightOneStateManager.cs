using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class FightOneStateManager : Singleton<FightOneStateManager>
    {
        [Separator("Class References")]
        [SerializeField] private GatlingGun playerTarget;
        public GatlingGun PlayerTarget => playerTarget;

        [Separator("States")]
        [SerializeField] private BaseBossState entryState;

        private BaseBossState currentState;
        public BaseBossState CurrentState
        {
            get { return currentState; }
            set { currentState = value; }
        }


        // Update is called once per frame
        void Start()
        {
            currentState = entryState;

            StartCoroutine(RunStateMachine());
        }

        //Run the state machine
        private IEnumerator RunStateMachine()
        {
            while(true)
            {
                if(!currentState.IsCoroutineActive)
                {
                    BaseBossState nextState = currentState?.RunCurrentState();

                    if (nextState != null)
                        currentState = nextState;
                }

                yield return null;
            }
        }
    }
}
