using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class OneThirdHpState : BaseBossState
    {
        protected override IEnumerator CurrentStateCoroutine()
        {
            IsCoroutineActive = true;

            if (useFirstTime)
            {
                useFirstTime = false;

                //ATTACK LAUNCH MISSILE
                for (int i = 0; i < missileAmount; i++)
                {
                    LaunchMissile();
                    yield return new WaitForSeconds(missileDelay);
                }
            }
            else
            {
                yield return new WaitForSeconds(attackCoolDown);
                LaunchMissile();

                yield return new WaitForSeconds(nextAttackCooldown);
                ShotMachineGun();
            }

            IsCoroutineActive = false;
        }
    }
}

