using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class QuarterHpState : BaseBossState
    {
        protected override IEnumerator CurrentStateCoroutine()
        {
            IsCoroutineActive = true;

            if (useFirstTime)
            {
                useFirstTime = false;

                //ATTACK LAUNCH MISSILE
                for (int i = 0; i < missileAmount; i++)
                {
                    LaunchMissile();
                    yield return new WaitForSeconds(missileDelay);
                }
            }
            else
            {
                yield return new WaitForSeconds(attackCoolDown);
                for (int i = 0; i < missileAmount; i++)
                {
                    LaunchMissile();
                    yield return new WaitForSeconds(missileDelay);
                }

                yield return new WaitForSeconds(nextAttackCooldown);
                ShotMachineGun();
            }

            IsCoroutineActive = false;
        }
    }
}
