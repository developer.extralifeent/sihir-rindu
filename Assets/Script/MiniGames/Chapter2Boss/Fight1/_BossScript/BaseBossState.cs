using Fungus;
using MyBox;
using OLJ.MiniGame.Fight1;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public abstract class BaseBossState : MonoBehaviour
    {
        [Separator("Switchable State")]
        [SerializeField] protected BaseBossState nextState;

        [Separator("States Config")]
        [SerializeField] protected bool useFirstTime = true;
        [SerializeField] protected bool containMultipleMissile = false;
        [SerializeField] protected float attackCoolDown = 10f;
        [SerializeField, ConditionalField(nameof(useFirstTime), false)] protected float nextAttackCooldown = 7f;
        [SerializeField, ConditionalField(nameof(containMultipleMissile), false)] protected float missileDelay = .5f;
        [SerializeField, ConditionalField(nameof(containMultipleMissile), false)] protected int missileAmount = 2;
        [SerializeField] protected float nextStateHpPercentage = 75;
        [SerializeField] protected Flowchart onStateChangeFlowchart;

        [Separator("Class References")]
        [SerializeField] protected RobotBoss robotBos;


        protected bool isTransitible;
        public bool IsTransitible => isTransitible;
        public bool IsCoroutineActive { get; set; }

        protected virtual void Start()
        {
            if (!robotBos)
                robotBos = this.GetComponentInParent<RobotBoss>();

            Fight1GameManager.Instance.GameState = GameState.Normal;
        }

        private void Update()
        {
            isTransitible = robotBos.CurrentHp <= (robotBos.MaxHp * nextStateHpPercentage / 100f);
            if (isTransitible)
            {
                IsCoroutineActive = false;
                StopAllCoroutines();
            }
        }

        protected void LaunchMissile() => robotBos.LaunchMissile();

        protected void ShotMachineGun() => robotBos.ShotMachineGun();
         
        public virtual BaseBossState RunCurrentState()
        {
            StartCoroutine(CurrentStateCoroutine());
            if (!isTransitible)
            {
                Debug.Log("Not Transitible");
                return this;
            }
            else
            {
                StopAllCoroutines();
                onStateChangeFlowchart.gameObject.SetActive(true);
                return nextState;
            }
        }

        protected abstract IEnumerator CurrentStateCoroutine();
    }
}

