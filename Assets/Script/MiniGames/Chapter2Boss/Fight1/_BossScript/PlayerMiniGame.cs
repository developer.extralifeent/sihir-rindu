using Fungus;
using MyBox;
using OLJ.Character;
using OLJ.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace OLJ.MiniGame.Fight1
{
    public class PlayerMiniGame : MonoBehaviour
    {
        [Separator("Class References")]
        [SerializeField] private GatlingGun gatlingGun;

        [Separator("Player State")]
        [SerializeField] private PlayerState playerState;
        public PlayerState PlayerState => playerState;

        /*[Separator("Player Indicaor")]
        [SerializeField] private SpriteRenderer stateIndicatorImg;
        [SerializeField] private Color hideIndicatorColor;
        [SerializeField] private Color standIndicatorColor;*/

        [Separator("Hiding Flowchart")]
        [SerializeField] private Flowchart HidingFlowchart;
        private string hidingBlock = "Hiding";

        // Start is called before the first frame update
        void Start()
        {
            //StartCoroutine(PlayerStateSwitcher());
            ControlManager.Instance.CharacterInputAction.Fight1MiniGame.HideStand.performed += (InputAction.CallbackContext _ctx) =>
            {
                Debug.Log("Context phase : Performed");
                playerState = PlayerState.Hiding;
                gatlingGun.IsCanShoot = false;
                HidingFlowchart.gameObject.SetActive(true);
                HidingFlowchart.ExecuteBlock(hidingBlock);

                //stateIndicatorImg.color = hideIndicatorColor;
            };

            ControlManager.Instance.CharacterInputAction.Fight1MiniGame.HideStand.canceled += (InputAction.CallbackContext _ctx) =>
            {
                Debug.Log("Context phase : Canceled");
                playerState = PlayerState.Standing;
                gatlingGun.IsCanShoot = true;
                HidingFlowchart.StopBlock(hidingBlock);
                HidingFlowchart.gameObject.SetActive(false);

                //stateIndicatorImg.color = standIndicatorColor;
            };
        }

        private void OnDisable()
        {
            ControlManager.Instance.CharacterInputAction.Fight1MiniGame.HideStand.performed -= (InputAction.CallbackContext _ctx) =>
            {
                Debug.Log("Context phase : Performed");
                playerState = PlayerState.Hiding;
                gatlingGun.IsCanShoot = false;
                HidingFlowchart.gameObject.SetActive(true);
                HidingFlowchart.ExecuteBlock(hidingBlock); 

                //stateIndicatorImg.color = hideIndicatorColor;
            };

            ControlManager.Instance.CharacterInputAction.Fight1MiniGame.HideStand.canceled -= (InputAction.CallbackContext _ctx) =>
            {
                Debug.Log("Context phase : Canceled");
                playerState = PlayerState.Standing;
                gatlingGun.IsCanShoot = true;
                HidingFlowchart.StopBlock(hidingBlock);
                HidingFlowchart.gameObject.SetActive(false);

                //stateIndicatorImg.color = standIndicatorColor;
            };
        }
    }

    public enum PlayerState
    {
        Standing,
        Hiding
    }
}
