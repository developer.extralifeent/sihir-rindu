using Fungus;
using MyBox;
using OLJ.Manager;
using OLJ.MiniGame.Fight1;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Unmodular",
                   "Chapter 2 Boss 1 State Switcher",
                   "Switch State For Boss Fight In Chapter 2")]
    public class AIStateSwitcherFungusExtension : Command
    {
        [Separator("State Managers")]
        [SerializeField] private bool isChapter1;
        [SerializeField, ConditionalField(nameof(isChapter1), false)] private FightOneStateManager fightOneStateManager;
        [SerializeField, ConditionalField(nameof(isChapter1), true)] private FightOneStateManager fightTwoStateManager;
        private ControlManager controlManager;

        [Separator("State To Switch")]
        [SerializeField] private BaseBossState stateToSwitch;

        private void Start()
        {
            fightOneStateManager = FightOneStateManager.Instance;
            controlManager = ControlManager.Instance;
        }

        public override void OnEnter()
        {
            base.OnEnter();

            //Time.timeScale = 1f;
            //Fight1GameManager.Instance.GameState = GameState.Normal;

            fightOneStateManager.CurrentState.IsCoroutineActive = false;
            fightOneStateManager.CurrentState = stateToSwitch;

            controlManager.SwitchInputAction(
                isChapter1 ?
                controlManager.CharacterInputAction.Fight1MiniGame :
                controlManager.CharacterInputAction.Fight1MiniGame //Change this to fight 2 mini game later
                );

            Continue();
        }
    }
}

