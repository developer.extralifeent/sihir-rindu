using DG.Tweening;
using Fungus;
using MyBox;
using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class RobotBoss : BaseBoss
    {
        [Separator("State flowcharts")]
        [SerializeField] private HpState hpState;
        [SerializeField] private Flowchart fullHpFlowchart;
        [SerializeField] private Flowchart firstOneThirdHpFlowchart;
        [SerializeField] private Flowchart firstHalfHpFlowchart;
        [SerializeField] private Flowchart firstQuarterHpFlowchart;
        [SerializeField] private Flowchart onDeadFlowchart;

        [Separator("Missile Launcher")]
        [SerializeField] private BossMissileBehaviour missilePrefabs;
        [SerializeField] private Transform[] missileSpots;
        [SerializeField] private Transform missileSpawnPos;

        [Separator("Machine Gun")]
        [SerializeField] private Transform shootingArea;
        [SerializeField] private Transform[] shootingStartEndPos;
        [SerializeField] private float bulletPerSecond = 10f;
        [SerializeField] private float damageEachBullet = 1f;
        [SerializeField] private float shootingDuration = 5f;

        [Separator("Machine Gun VFX")]
        [SerializeField] private GameObject machineGunVfxPrefab;
        private GameObject activeMachineGunVfx;
        private Tween movementTween;
        public float ShootingDuration => shootingDuration;

        public bool IsShootingAreaOnTheLeft { get; set;}

        private List<BossMissileBehaviour> activeMissiles = new List<BossMissileBehaviour>();

        private bool previousHalfHpState = false;
        private bool previousZeroHpState = false;

        private void Update()
        {
            bool fullHp = currentHp >= (75f * maxHp / 100f);
            bool oneThirdHp = currentHp <= (75f * maxHp / 100f) && currentHp > (50f * maxHp / 100f);
            bool halfHp = currentHp <= maxHp / 2f && currentHp > (25f * maxHp / 100f);
            bool quarterHp = currentHp > 0 && currentHp <= (25f * maxHp / 100f);
            bool zeroHp = currentHp <= 0f;

            if(fullHp)
            {
                if (fullHpFlowchart == null) return;
                fullHpFlowchart.gameObject.SetActive(true);
            }
            else if(oneThirdHp)
            {
                if(firstOneThirdHpFlowchart == null) return;
                fullHpFlowchart.gameObject.SetActive(false);
                firstOneThirdHpFlowchart.gameObject.SetActive(true);
            }
            else if(halfHp && !previousHalfHpState)
            {
                if (firstHalfHpFlowchart == null) return;
                CleanupMissiles();
                firstOneThirdHpFlowchart.gameObject.SetActive(false);
                firstHalfHpFlowchart.gameObject.SetActive(true);
                previousHalfHpState = true;
            }
            else if(quarterHp)
            {
                if (firstQuarterHpFlowchart == null) return;
                firstHalfHpFlowchart.gameObject.SetActive(false);
                firstQuarterHpFlowchart.gameObject.SetActive(true);
            }
            else if(zeroHp && !previousZeroHpState)
            {
                if(onDeadFlowchart == null) return;
                CleanupMissiles();
                firstQuarterHpFlowchart.gameObject.SetActive(false);
                onDeadFlowchart.gameObject.SetActive(true);
                previousZeroHpState = true;
            }

            if (!halfHp) previousHalfHpState = false;
            if (!zeroHp) previousZeroHpState = false;
        }

        public void ShotMachineGun()
        {
            //Determine Machine Gun Direction
            //float randomNum = Random.Range(-1f, 1f);
            //isShootingAreaOnTheLeft = randomNum <= 0 ? true : false;
;
            Transform initShootingAreaPos = IsShootingAreaOnTheLeft ? shootingStartEndPos[0] : shootingStartEndPos[1];
            Transform finalShootingAreaPos = IsShootingAreaOnTheLeft ? shootingStartEndPos[1] : shootingStartEndPos[0];

            shootingArea.position = initShootingAreaPos.position;
            if (machineGunVfxPrefab != null && activeMachineGunVfx == null)
            {
                activeMachineGunVfx = Instantiate(
                    machineGunVfxPrefab,
                    shootingArea.position,
                    machineGunVfxPrefab.transform.rotation
                );
            }

            movementTween = shootingArea.DOMoveX(finalShootingAreaPos.position.x, shootingDuration)
                .SetEase(Ease.Linear)
                .OnUpdate(() => 
                {

                    if (activeMachineGunVfx != null)
                    {
                        activeMachineGunVfx.transform.position = shootingArea.position;
                    }
                })
                .OnComplete(() => 
                {
                    if (activeMachineGunVfx != null)
                    {
                        Destroy(activeMachineGunVfx);
                        activeMachineGunVfx = null;
                    }
                });

            StartCoroutine(MachineGunCoroutine());
        }

        private IEnumerator MachineGunCoroutine()
        {
            float time = 0f;

            while(time < shootingDuration)
            {
                //Shot Machine Gun
                Collider2D[] collider2Ds = Physics2D.OverlapBoxAll(shootingArea.position, shootingArea.localScale, 0f);

                for (int i = 0; i < bulletPerSecond; i++)
                {
                    foreach (Collider2D colls in collider2Ds)
                    {
                        if (colls.TryGetComponent(out PlayerMiniGame _playerMiniGame))
                        {
                            PlayerState playerState = _playerMiniGame.PlayerState;

                            switch (playerState)
                            {
                                case PlayerState.Standing:
                                    _playerMiniGame.GetComponent<PlayerHp>().TakeDamage(damageEachBullet);
                                    Debug.Log("Player damaged");
                                    break;
                                case PlayerState.Hiding:
                                    Debug.Log("Player hiding. No damage taken");
                                    break;
                            }
                        }
                    }

                    yield return new WaitForSeconds(1f / bulletPerSecond);
                }   
            }

            if(activeMachineGunVfx != null)
            {
                Destroy(activeMachineGunVfx);
                activeMachineGunVfx = null;
            }
        }

        public void LaunchMissile()
        {
            int randomIndex = Random.Range(0, missileSpots.Length);

            BossMissileBehaviour missile = Instantiate(missilePrefabs, missileSpawnPos.position, Quaternion.identity);
            missile.PlayerTarget = FightOneStateManager.Instance.PlayerTarget.transform;
            missile.RandomMissileSpot = missileSpots[randomIndex].transform;

            activeMissiles.Add(missile);
        }

        private void CleanupMissiles()
        {
            foreach (BossMissileBehaviour missile in activeMissiles)
            {
                if (missile != null)
                {
                    Destroy(missile.gameObject);
                }
            }
            activeMissiles.Clear();
        }



        public override void OnDestroyed()
        {
            //this.gameObject.SetActive(false);
        }

        public override void OnTakeDamage(float _damage)
        {
            base.OnTakeDamage(_damage);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(shootingArea.position, shootingArea.localScale * 2f);
        }

        private void OnDisable()
        {
            if (movementTween != null && movementTween.IsActive())
            {
                movementTween.Kill();
            }

            if (activeMachineGunVfx != null)
            {
                Destroy(activeMachineGunVfx);
                activeMachineGunVfx = null;
            }
        }

    }

    public enum HpState
    {
        Full,
        FirstTimeOneThird,
        OneThird,
        Half,
        FirstTimeQuerter,
        Quarter,
        Empty
    }
}
