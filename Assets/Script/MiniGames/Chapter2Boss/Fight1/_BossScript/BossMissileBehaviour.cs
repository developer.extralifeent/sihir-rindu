using DG.Tweening;
using MyBox;
using OLJ.Character;
using OLJ.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class BossMissileBehaviour : MonoBehaviour, IDestructible
    {
        [Separator("Missile Values")]
        [SerializeField] private float missileHp = 50f;
        [SerializeField] private float missileDuration = 15f;
        [SerializeField] private float missileRadius = 3f;

        [Separator("Missile Scales")]
        [SerializeField] private float initialScale = .5f;
        [SerializeField] private float finalScale = 1.5f;

        [Separator("Missile Objects")]
        [SerializeField] private Transform[] missileVisuals;

        [Separator("Missile Damages")]
        [SerializeField] private float missileFullDamage = 50f;
        [SerializeField] private float missileHalfDamage = 25f;

        [Separator("Explosion VFX")]
        [SerializeField] private GameObject explosionVfxPrefab;
        [SerializeField] private float explosionDuration = 1.5f;

        public Transform PlayerTarget { get; set; }
        public Transform RandomMissileSpot { get; set; }

        // Start is called before the first frame update
        void Start()
        {
            this.transform.localScale = Vector3.one * initialScale;

            StartCoroutine(LaunchMissileUpward());
        }

        private IEnumerator LaunchMissileUpward()
        {
            float moveDuration = 1f;
            ActivateOneMissileVisual(0);
            this.transform.DOMoveY(this.transform.position.y + 13f, moveDuration).SetEase(Ease.Linear);
            
            yield return new WaitForSeconds(moveDuration + 1f);
            this.transform.DOMoveX(RandomMissileSpot.position.x, .01f);
            ActivateOneMissileVisual(1);
            this.transform.DOMove(RandomMissileSpot.position, moveDuration).SetEase(Ease.Linear);
            
            yield return new WaitForSeconds(moveDuration);
            ActivateOneMissileVisual(2);

            StartCoroutine(LaunchMissileTowardPlayer());
        }

        private IEnumerator LaunchMissileTowardPlayer()
        {
            //Scale Missile
            this.transform.DOScale(Vector3.one * finalScale, missileDuration).SetEase(Ease.Linear);

            //Move Missile
            this.transform.DOMove(PlayerTarget.position, missileDuration * 4f).SetEase(Ease.Linear);
            
            yield return new WaitForSeconds(missileDuration / 2f);

            //Change mid missile rotation
            RotateMissileToward(PlayerTarget.position, missileVisuals[3]);
            ActivateOneMissileVisual(3);

            yield return new WaitForSeconds(missileDuration / 2f);

            //Change final missile rotation
            RotateMissileToward(PlayerTarget.position, missileVisuals[4]);
            ActivateOneMissileVisual(4);

            ChangeTrajectory();
        }

        private void ChangeTrajectory()
        {
            missileVisuals[4].DOMove(PlayerTarget.position, 1f).SetEase(Ease.Linear);

            Invoke(nameof(MissileHit), 1f);
        }

        private void StopMissile()
        {
            this.transform.DOKill();
            StopAllCoroutines();
        }

        private void MissileHit()
        {
            Collider2D[] collider2Ds = Physics2D.OverlapCircleAll(missileVisuals[4].transform.position, missileRadius);

            foreach (Collider2D colls in collider2Ds)
            {
                if (colls.TryGetComponent(out PlayerMiniGame _playerMiniGame))
                {
                    PlayerState playerState = _playerMiniGame.PlayerState;

                    switch (playerState)
                    {
                        case PlayerState.Standing:
                            _playerMiniGame.GetComponent<PlayerHp>().TakeDamage(missileFullDamage);
                            Debug.Log("Player damaged");
                            break;
                        case PlayerState.Hiding:
                            _playerMiniGame.GetComponent<PlayerHp>().TakeDamage(missileHalfDamage);
                            Debug.Log("Player hiding. No damage taken");
                            break;
                    }
                }
            }

            OnDestroyed();
        }

        private void RotateMissileToward(Vector3 _lookTarget, Transform _ObjToRotate)
        {
            //_lookTarget.Normalize();

            //float angle = Mathf.Atan2(_lookTarget.y, _lookTarget.x) * Mathf.Rad2Deg - 90f;

            //_ObjToRotate.rotation = Quaternion.Euler(0f, 0f, angle);
            Quaternion missileRot = Quaternion.LookRotation
            (
                _lookTarget - this.transform.position,
                _ObjToRotate.transform.TransformDirection(Vector3.up)
            );

            _ObjToRotate.transform.rotation = new Quaternion(0f, 0f, -missileRot.z, missileRot.w);
        }

        public void OnTakeDamage(float _damage)
        {
            missileHp -= _damage;
            if(missileHp <= 0f)
            {
                missileHp = 0f;
                OnDestroyed();
            }
        }

        private void TriggerExplosion()
        {
            if (explosionVfxPrefab != null)
            {
                GameObject explosion = Instantiate(explosionVfxPrefab, missileVisuals[4].position, Quaternion.identity);
                Destroy(explosion, explosionDuration);
            }
        }

        /// <summary>
        /// Missile Index :
        /// 0 = upward
        /// 1 = downward
        /// 2 = forward
        /// 3 = sideway
        /// 4 = toward player
        /// </summary>
        /// <param name="_index"></param>
        private void ActivateOneMissileVisual(int _index)
        {
            for (int i = 0; i < missileVisuals.Length; i++)
            {
                missileVisuals[i].gameObject.SetActive(i == _index);
            }
        }

        public void OnDestroyed()
        {
            StopAllCoroutines();
            TriggerExplosion();
            Destroy(this.gameObject);
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(missileVisuals[missileVisuals.Length - 1].transform.position, missileRadius);
        }
    }
}

