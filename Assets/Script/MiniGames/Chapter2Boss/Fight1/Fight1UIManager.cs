using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OLJ.MiniGame.Fight1
{
    public class Fight1UIManager : MonoBehaviour
    {
        [Separator("Class References")]
        [SerializeField] private GatlingGun gatlingGun;
        [SerializeField] private RobotBoss robotBoss;

        [Separator("Player Indicators / UI")]
        [SerializeField] private Slider heatSlider;

        [Separator("Boss Indicators / UI")]
        [SerializeField] private Slider bossHpSlider;

        private void Awake()
        {
            heatSlider.maxValue = gatlingGun.MaxHeat;
            bossHpSlider.maxValue = robotBoss.MaxHp;
            bossHpSlider.value = bossHpSlider.maxValue;
        }

        // Start is called before the first frame update
        void Start()
        {
            gatlingGun.onHeatChanged += UpdateGunHeatBar;
            robotBoss.OnTakeDamageEvent += OnBossTakeDamage;
        }

        private void OnDestroy()
        {
            gatlingGun.onHeatChanged -= UpdateGunHeatBar;
        }

        private void UpdateGunHeatBar(float _currentHeat)
        {
            if (!heatSlider)
            {
                Debug.LogError("Heat Slider is not assigned");
                return;
            }

            heatSlider.value = _currentHeat;
        }

        private void OnBossTakeDamage(object sender, BaseBoss.OnTakeDmgEventArgs e)
        {
            bossHpSlider.value = e.currentHp;
        }
    }
}
