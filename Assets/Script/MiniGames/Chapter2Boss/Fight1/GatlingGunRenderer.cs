using MyBox;
using System.Collections;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class GatlingGunRenderer : MonoBehaviour
    {
        [Separator("Class References")]
        [SerializeField] private CrosshairManager crossHairManager;

        private void Start()
        {
            StartCoroutine(CheckCrosshairPosCoroutine());
        }

        private IEnumerator CheckCrosshairPosCoroutine()
        {
            while(true)
            {
                CheckCrosshairPosition();
                yield return null;
            }
        }

        private void CheckCrosshairPosition()
        {
            // Get the direction from the current object to the target
            Vector2 toTarget = crossHairManager.CrosshairObj.transform.position - this.transform.position;

            // Get the "forward" direction of the object (it faces up)
            Vector2 upward = transform.up;

            // Calculate the cross product (only the Z component is relevant in 2D)
            float crossProduct = upward.x * toTarget.y - upward.y * toTarget.x;

            if (crossProduct >= 1)
            {
                //Debug.Log("Target is to the LEFT");
            }
            else if (crossProduct <= -1)
            {
                //Debug.Log("Target is to the RIGHT");
            }
            else
            {
                //Debug.Log("Target is STRAIGHT AHEAD or BEHIND");
            }
        }
    }
}
