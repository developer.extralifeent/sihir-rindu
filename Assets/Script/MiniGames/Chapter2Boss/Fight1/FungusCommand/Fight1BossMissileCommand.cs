using Fungus;
using MyBox;
using OLJ.MiniGame.Fight1;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Unmodular",
                   "Chapter 2 Boss 1 Shot Missile",
                   "Shot Missile from boss 1 chapter 2")]
    public class Fight1BossMissileCommand : Command
    {
        [Separator("Class References")]
        [SerializeField] private RobotBoss robotBoss;

        public override void OnEnter()
        {
            base.OnEnter();

            robotBoss.LaunchMissile();

            Continue();
        }
    }
}
