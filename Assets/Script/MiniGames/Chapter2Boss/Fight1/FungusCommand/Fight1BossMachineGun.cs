using Fungus;
using MyBox;
using OLJ.MiniGame.Fight1;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Unmodular",
                   "Chapter 2 Boss 1 Shot Machine Gun",
                   "Shot Machine Gun from boss 1 chapter 2")]
    public class Fight1BossMachineGun : Command
    {
        [Separator("Class References")]
        [SerializeField] private RobotBoss robotBoss;

        [Separator("Boss States")]
        [SerializeField] private bool isShootingAreaOnTheLeft;

        public override void OnEnter()
        {
            base.OnEnter();

            robotBoss.IsShootingAreaOnTheLeft = isShootingAreaOnTheLeft;
            robotBoss.ShotMachineGun();

            Invoke(nameof(ContinueLine), robotBoss.ShootingDuration);
        }

        private void ContinueLine() => Continue();
    }
}
