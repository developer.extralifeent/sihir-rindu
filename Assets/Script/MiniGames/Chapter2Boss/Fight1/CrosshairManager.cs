using MyBox;
using OLJ.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace OLJ.MiniGame.Fight1
{
    public class CrosshairManager : MonoBehaviour
    {
        private ControlManager controlManager;

        [Separator("Crosshair Movement")]
        [SerializeField] private Camera mainCam;
        [SerializeField] private GameObject crosshairObj;
        [SerializeField] private float crosshairSpeed = 2f;
        private bool isUsingMouse;
        public Vector3 GetCrosshairPosition => crosshairObj.transform.position;
        public GameObject CrosshairObj => crosshairObj;

        private void Awake()
        {
            controlManager = ControlManager.Instance;
        }

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("Cursor State : " + Cursor.lockState);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = false;

            isUsingMouse = controlManager.CurrentControlScheme == "Keyboard" ? true : false;
            ChangeCrosshairControl(true);
        }

        private void OnDestroy()
        {
            ChangeCrosshairControl(false);
        }

        // Update is called once per frame
        void Update()
        {
            MoveCrosshair();
        }

        private void ChangeCrosshairControl(bool _isSubscribing)
        {
            if (_isSubscribing)
            {
                controlManager.onControlScemeChanged += (string _controlScheme) =>
                {
                    if (controlManager.CurrentControlScheme == "Keyboard") isUsingMouse = true;
                    else isUsingMouse = false;
                };
            }
            else
            {
                controlManager.onControlScemeChanged -= (string _controlScheme) =>
                {
                    if (controlManager.CurrentControlScheme == "Keyboard") isUsingMouse = true;
                    else isUsingMouse = false;
                };
            }
        }

        private void MoveCrosshair()
        {
            if (isUsingMouse)
            {
                Vector2 mousePos = Mouse.current.position.ReadValue();
                Vector3 mouseWorldPos = mainCam.ScreenToWorldPoint(mousePos);
                mouseWorldPos.z = 0f;
                crosshairObj.transform.position = mouseWorldPos;
            }
            else
            {
                Vector2 inputVector = ControlManager.Instance.CharacterInputAction.Fight1MiniGame.Movement.ReadValue<Vector2>();
                Vector2 moveDir = new Vector2(inputVector.x, inputVector.y);
                crosshairObj.transform.position += crosshairSpeed * Time.deltaTime * (Vector3)moveDir.normalized;
            }
        }
    }
}
