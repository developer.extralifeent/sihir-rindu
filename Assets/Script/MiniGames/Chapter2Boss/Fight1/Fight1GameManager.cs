using Fungus;
using OLJ.Manager;
using OLJ.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight1
{
    public class Fight1GameManager : OLJ.Singleton<Fight1GameManager>
    {
        [SerializeField] private GameState gameState;
        public GameState GameState
        {
            get { return gameState; }
            set { gameState = value; }
        }

        private QTEManager qteManager;

        protected override void Awake()
        {
            base.Awake();

            gameState = GameState.Normal;
        }

        private void Start()
        {
            StartCoroutine(CheckGameState());

            qteManager = QTEManager.Instance;

            qteManager.onQTEfinished.AddListener(QTEManager_OnQteFinished);
        }

        private void QTEManager_OnQteFinished(int _index)
        {
            qteManager.onQTEfinished.RemoveListener(QTEManager_OnQteFinished);

            gameState = GameState.Normal;
        }

        private IEnumerator CheckGameState()
        {
            while (true)
            {
                switch (gameState)
                {
                    case GameState.Normal:
                        yield return null;
                        break;
                    case GameState.QTE:
                        Time.timeScale = 0f;
                        break;
                }

                yield return null;
            }
        }
    }

    public enum GameState
    {
        Normal,
        QTE
    }
}