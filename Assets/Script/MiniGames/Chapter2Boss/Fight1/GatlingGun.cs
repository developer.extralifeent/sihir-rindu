using Fungus;
using MyBox;
using OLJ.Gameplay;
using OLJ.Interface;
using OLJ.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace OLJ.MiniGame.Fight1
{
    public class GatlingGun : MonoBehaviour
    {
        [Separator("Class References")]
        [SerializeField] private CrosshairManager crossHairManager;
        [SerializeField] private PlayerMiniGame player;
        [SerializeField] private CameraShake camShake;

        private ControlManager controlManager;

        [Separator("Gatling Gun States")]
        [SerializeField] private GatlingGunState gunState;
        public bool IsCanShoot { get; set; } = true;

        [Separator("VFX")]
        [SerializeField] private GameObject vfxPrefab;
        private GameObject vfxGunInstance;
        private GameObject vfxImpactInstance;
        private GatlingGunState previousState;

        [Separator("State Parameters")]
        [SerializeField] private float firingDelay = 1f;
        private float readyTime = 0f;
        private bool isFireCoroutineStarted = false;
        private bool isCoolingCoroutineStarted = false;

        [Separator("Heat Parameters")]
        [SerializeField] private float heatMultiplier = 2f;
        [SerializeField] private float maxHeat = 100f;
        private float currentHeat = 0f;
        public Action<float> onHeatChanged;

        public float MaxHeat => maxHeat;

        [Separator("Fungus Flowchart")]
        [SerializeField] private Flowchart overheatingFlowchart;
        [SerializeField] private string overheatBlock = "Heating";


        [Separator("Firing Parameters")]
        [SerializeField] private float bulletPerSecond = 10f;
        [SerializeField] private float gunDamage = 1f;
        [SerializeField] private float overlapCircleRadius = 1;

        // Start is called before the first frame update
        void Start()
        {
            controlManager = ControlManager.Instance;

            controlManager.CharacterInputAction.Fight1MiniGame.Enable();

            previousState = gunState;

            StartCoroutine(StateSwitcher());
        }

        private void Update()
        {
            if(vfxImpactInstance != null && gunState == GatlingGunState.Firing)
            {
                vfxImpactInstance.transform.position = crossHairManager.CrosshairObj.transform.position;
            }
        }

        private IEnumerator StateSwitcher()
        {
            while (true)
            {
                if(player.PlayerState == PlayerState.Standing)
                {
                    GatlingGunState currentState = gunState;

                    switch (gunState)
                    {
                        case GatlingGunState.Ready:
                            GetGunReady();
                            CoolGun();
                            break;
                        case GatlingGunState.Firing:
                            FireGun();
                            if (vfxImpactInstance == null)
                            {
                                vfxImpactInstance = Instantiate(vfxPrefab, crossHairManager.CrosshairObj.transform.position, Quaternion.identity);
                            }

                            if (vfxGunInstance == null)
                            {
                                vfxGunInstance = Instantiate(vfxPrefab, transform.position, transform.rotation);
                                vfxGunInstance.transform.parent = transform;
                            }
                            break;
                        case GatlingGunState.Releasing:
                            ReleaseGun();
                            CoolGun();
                            break;
                        case GatlingGunState.Cooling:
                            CoolGun();
                            break;
                    }

                    if (previousState == GatlingGunState.Firing && currentState != GatlingGunState.Firing)
                    {
                        if (vfxGunInstance != null)
                        {
                            Destroy(vfxGunInstance);
                            vfxGunInstance = null;
                        }

                        if (vfxImpactInstance != null)
                        {
                            Destroy(vfxImpactInstance);
                            vfxImpactInstance = null;
                        }
                    }

                    if (currentState == GatlingGunState.Cooling && previousState != GatlingGunState.Cooling)
                    {
                        overheatingFlowchart.gameObject.SetActive(true);
                        overheatingFlowchart.ExecuteBlock(overheatBlock);
                    }
                    else if (previousState == GatlingGunState.Cooling && currentState != GatlingGunState.Cooling)
                    {
                        overheatingFlowchart.StopBlock(overheatBlock);
                        overheatingFlowchart.gameObject.SetActive(false);
                    }

                    previousState = currentState;
                }
                else
                {
                    ReleaseGun();
                    CoolGun();
                }
                yield return null;
            }
            
        }

        private void GetGunReady()
        {
            if (UnityEngine.InputSystem.Mouse.current.leftButton.wasPressedThisFrame || controlManager.CharacterInputAction.Fight1MiniGame.Firing.WasPressedThisFrame())
            {
                StartCoroutine(GetGunReadyCoroutine());
            }
        }

        private IEnumerator GetGunReadyCoroutine()
        {
            readyTime = 0f;
            while (readyTime < firingDelay)
            {
                readyTime += Time.deltaTime;

                if (UnityEngine.InputSystem.Mouse.current.leftButton.wasReleasedThisFrame || controlManager.CharacterInputAction.Fight1MiniGame.Firing.WasReleasedThisFrame())
                {
                    gunState = GatlingGunState.Releasing;
                    yield break;
                }

                yield return null;
            }
            gunState = GatlingGunState.Firing;
        }

        private void FireGun()
        {
            if(!IsCanShoot) return;

            if(!isFireCoroutineStarted)
                StartCoroutine(FireGunCoroutine());

            isCoolingCoroutineStarted = false;

            if (UnityEngine.InputSystem.Mouse.current.leftButton.wasReleasedThisFrame || controlManager.CharacterInputAction.Fight1MiniGame.Firing.WasReleasedThisFrame())
            {
                isFireCoroutineStarted = false;
                StopCoroutine(FireGunCoroutine());
                gunState = GatlingGunState.Releasing;
            }

            IEnumerator FireGunCoroutine()
            {
                isFireCoroutineStarted = true;
                while(gunState == GatlingGunState.Firing)
                {
                    CheckTarget();

                    currentHeat += heatMultiplier;
                    onHeatChanged?.Invoke(currentHeat);
                    if (currentHeat >= maxHeat)
                    {
                        isFireCoroutineStarted = false;
                        gunState = GatlingGunState.Cooling;
                        yield break;
                    }

                    float waitDuration = 1f / bulletPerSecond;
                    camShake.ShakeCam(waitDuration, 1.5f);
                    yield return new WaitForSeconds(1f / bulletPerSecond);
                }
            }
        }

        private void CheckTarget()
        {
            Collider2D[] col = Physics2D.OverlapCircleAll(crossHairManager.CrosshairObj.transform.position, overlapCircleRadius);

            foreach(Collider2D col2 in col)
            {
                if(col2.TryGetComponent(out IDestructible _IDestructible))
                {
                    _IDestructible.OnTakeDamage(gunDamage);
                }
            }
        }

        private void ReleaseGun()
        {
            readyTime -= Time.deltaTime;
            if(readyTime <= 0f && currentHeat <= (maxHeat - (maxHeat * 20f / 100f)))
            {
                gunState = GatlingGunState.Ready;
            }
        }

        private void CoolGun()
        {
            if(!isCoolingCoroutineStarted)
                StartCoroutine(CoolDownGunCoroutine());

            IEnumerator CoolDownGunCoroutine()
            {
                isCoolingCoroutineStarted = true;
                while(currentHeat > 0f)
                {
                    currentHeat--;
                    onHeatChanged?.Invoke(currentHeat);
                    yield return new WaitForSeconds(1f / 20f);

                    if(gunState == GatlingGunState.Firing)
                    {
                        isCoolingCoroutineStarted = false;
                        yield break;
                    }
                    else if(currentHeat <= (maxHeat - (maxHeat * 20f / 100f)))
                    {
                        StopCoroutine(CoolDownGunCoroutine());
                        isCoolingCoroutineStarted = false;
                        gunState = GatlingGunState.Ready;
                        yield break;
                    }
                }
                isCoolingCoroutineStarted = false;
            }
        }
    }

    public enum GatlingGunState
    {
        Ready,
        Firing,
        Releasing,
        Cooling
    }
}
