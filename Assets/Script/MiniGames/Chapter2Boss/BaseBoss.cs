using OLJ.Interface;
using OLJ.MiniGame.Fight1;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame
{
    public abstract class BaseBoss : MonoBehaviour, IDestructible
    {
        [SerializeField] protected float maxHp;
        protected float currentHp;

        public event EventHandler<OnTakeDmgEventArgs> OnTakeDamageEvent;
        public class OnTakeDmgEventArgs
        {
            public float currentHp;
            public OnTakeDmgEventArgs(float _currentHp)
            {
                this.currentHp = _currentHp;
            }
        }

        public float CurrentHp => currentHp;
        public float MaxHp => maxHp;

        protected virtual void Start()
        {
            currentHp = maxHp;
        }

        public abstract void OnDestroyed();

        public virtual void OnTakeDamage(float _damage)
        {
            currentHp -= _damage;

            OnTakeDamageEvent?.Invoke(this, new OnTakeDmgEventArgs(currentHp));

            if(currentHp <= 0)
            {
                currentHp = 0;
                OnDestroyed();
            }
        }
    }
}
