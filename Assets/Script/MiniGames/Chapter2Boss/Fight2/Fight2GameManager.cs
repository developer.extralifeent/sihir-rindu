using Fungus;
using MyBox;
using OLJ.Gameplay.Interactable;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

namespace OLJ.MiniGame.Fight2
{
    public class Fight2GameManager : Singleton<Fight2GameManager>
    {
        [Separator("Class References")]
        [SerializeField] private Fight2PlayerState playerState;

        [Separator("Missile Launcher")]
        [SerializeField] private Fight2MissileBehaviour missilePrefabs;
        [SerializeField] private MissileSpots[] missileSpots;
        [SerializeField] private float missileYStartpos = 60f;
        [SerializeField] private MissileSpotState[] missileSpotStates;
        private int randomMissileSpotIndex;
        private List<GameObject> spawnedMissileList = new List<GameObject>();

        [Separator("Events")]
        [SerializeField] private Flowchart kancilTalkFallingMissileFlowchart;
        [SerializeField] private Flowchart kancilTalkShotCannonFlowchart;

        private void Start()
        {
            missileSpotStates = new MissileSpotState[missileSpots.Length];

            for (int i = 0; i < missileSpotStates.Length; i++)
            {
                missileSpots[i].LocalSpotIndex = i;
            }
        }

        [ButtonMethod]
        public void ShotMissile()
        {
            if (IsSpotFull()) return;

            randomMissileSpotIndex = UnityEngine.Random.Range(0, missileSpotStates.Length);

            if (missileSpotStates[randomMissileSpotIndex] == MissileSpotState.Occupied)
            {
                ShotMissile();
                return;
            }

            Fight2MissileBehaviour missile = Instantiate(missilePrefabs, missileSpots[randomMissileSpotIndex].transform.position, Quaternion.identity);
            missile.transform.position = new Vector3(missileSpots[randomMissileSpotIndex].transform.position.x, missileYStartpos, missileSpots[randomMissileSpotIndex].transform.position.z);
            missile.GetComponent<MissileInteract>().MissileSpotIndex = randomMissileSpotIndex;

            spawnedMissileList.Add(missile.gameObject);

            if(missile.MissileConidtion == MissileCondition.Malfunction)
                missileSpotStates[randomMissileSpotIndex] = MissileSpotState.Occupied;
        }

        private bool IsSpotFull()
        {
            for (int i = 0; i < missileSpotStates.Length; i++)
            {
                if (missileSpotStates[i] == MissileSpotState.Empty) return false;
            }

            return true;
        }

        public void EmptyMissileSpot(int _spotIndex)
        {
            if (missileSpotStates[_spotIndex] == MissileSpotState.Empty) return;

            missileSpotStates[_spotIndex] = MissileSpotState.Empty;
        }

        public Fight2PlayerState GetPlayerState() => playerState;
       
        public PlayerMissileState GetPlayerMissileState() => playerState.PlayerMissileState;

        public void AddBrokenMissile(Fight2MissileBehaviour _missileToAdd)
        {
            spawnedMissileList.Add(_missileToAdd.gameObject);
        }

        public void RemoveBrokenMissile(Fight2MissileBehaviour _missileToRemove)
        {
            spawnedMissileList.Remove(_missileToRemove.gameObject);
        }

        public void ClearMissile()
        {
            for (int i = 0; i < spawnedMissileList.Count; i++)
            {
                Destroy(spawnedMissileList[i].gameObject);
            }

            for (int i = 0; i < missileSpotStates.Length; i++)
            {
                missileSpotStates[i] = MissileSpotState.Empty;
            }

            spawnedMissileList.Clear();
        }

        public void ShotCannonKancil() => kancilTalkShotCannonFlowchart.gameObject.SetActive(true);
    }

    public enum MissileSpotState
    {
        Empty,
        Occupied
    }
}