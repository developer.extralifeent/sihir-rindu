using MyBox;
using OLJ.MiniGame.Fight2;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OLJ.Manager
{
    public class Fight2UiManager : MonoBehaviour
    {
        [Separator("Class References")]
        [SerializeField] private Fight2Boss fight2Boss;

        //[Separator("Carrying Bullet Indicator")]
        //[SerializeField] private Sprite carryingBulletIndicator;

        [Separator("Enemy Hp")]
        [SerializeField] private Image enemyHpBar;

        private void Start()
        {
            fight2Boss.onTakeDamageEvent += OnBossTakeDamage;
        }

        private void OnDestroy()
        {
            fight2Boss.onTakeDamageEvent -= OnBossTakeDamage;
        }

        public void OnBossTakeDamage(object _sender, Fight2Boss.OnTakeDamageArgs _args)
        {
            enemyHpBar.fillAmount = _args.currentHp / _args.maxHp;
        }
    }
}

