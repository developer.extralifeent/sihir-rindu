using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight2
{
    public class Fight2PlayerState : MonoBehaviour
    {
        [SerializeField] private PlayerMissileState playerMissileState;

        public PlayerMissileState PlayerMissileState => playerMissileState;

        public void OnMissileTaken() => playerMissileState = PlayerMissileState.CarryingMissile;

        public void OnMissileReleased() => playerMissileState = PlayerMissileState.None;
    }

    public enum PlayerMissileState
    {
        None,
        CarryingMissile
    }
}