using Fungus;
using MyBox;
using OLJ.Character;
using OLJ.Gameplay.Interactable;
using OLJ.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight2
{
    public class Fight2MissileBehaviour : MonoBehaviour
    {
        [Separator("Missile Condition")]
        [SerializeField] private MissileCondition missileCondition;
        [SerializeField] private int malfunctionRate = 45;
        private int randomConditionNum;

        [Separator("Ground Checker")]
        [SerializeField] private Transform groundCheckerPos;
        [SerializeField] private float groundCheckerRayLength;
        [SerializeField] private LayerMask groundLayerMask;

        [Separator("Missile Explosion")]
        [SerializeField] private float explosionRadius;
        [SerializeField] private float explosionDamage;
        [SerializeField] private Flowchart explosionFlowchart;

        [Separator("Missile Malfunction")]
        [SerializeField] private MissileInteract missileInteract;
        [SerializeField] private Flowchart malfunctionFlowchart;

        public MissileCondition MissileConidtion => missileCondition;

        private void Awake()
        {
            randomConditionNum = UnityEngine.Random.Range(0, 101);
            missileCondition = randomConditionNum <= malfunctionRate ? MissileCondition.Malfunction : MissileCondition.Normal;
        
            if(missileCondition == MissileCondition.Normal) Destroy(missileInteract);
        }

        // Update is called once per frame
        void Update()
        {
            Debug.DrawRay(groundCheckerPos.position, Vector3.down * groundCheckerRayLength, Color.blue);

            CheckMissileConditionOnLand();
        }

        private void CheckMissileConditionOnLand()
        {
            Debug.Log("Missile Grounded : " + IsGrounded());
            if (IsGrounded())
            {
                switch (missileCondition)
                {
                    case MissileCondition.Normal:
                        ExplodeMissile();
                        break;
                    case MissileCondition.Malfunction:
                        PlugMissileIntoGrond();
                        break;
                }
            }
        }

        private void ExplodeMissile()
        {
            Collider[] colliders = Physics.OverlapSphere(this.transform.position, explosionRadius);
            foreach(Collider col in colliders)
            {
                if(col.TryGetComponent(out PlayerHp _playerHp))
                {
                    _playerHp.TakeDamage(explosionDamage);
                }
            }

            Flowchart impactMissileFlowchart = Instantiate(explosionFlowchart, this.transform.position, Quaternion.identity);
            Destroy(impactMissileFlowchart.gameObject, 8f);

            Fight2GameManager.Instance.RemoveBrokenMissile(this);

            Destroy(this.gameObject);
        }

        private void PlugMissileIntoGrond()
        {
            missileInteract.enabled = true;

            malfunctionFlowchart.gameObject.SetActive(true);
        }

        private bool IsGrounded()
        {
            return Physics.Raycast(groundCheckerPos.position, Vector3.down, groundCheckerRayLength, groundLayerMask);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(this.transform.position, explosionRadius);
        }
    }

    public enum MissileCondition
    {
        None,
        Normal,
        Malfunction
    }
}

