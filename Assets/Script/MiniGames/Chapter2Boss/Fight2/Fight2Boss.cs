using Fungus;
using MyBox;
using OLJ.Manager;
using OLJ.MiniGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight2
{
    public class Fight2Boss : BaseBoss
    {
        [Separator("Missile cooldown based on hp")]
        [SerializeField] private float firstCooldown = 5f;
        [SerializeField] private float secondCooldown = 3f;

        [Separator("Missile amount base on hp")]
        [SerializeField] private int hp5MissileAmount = 3;
        [SerializeField] private int hp3MissileAmount = 4;
        [SerializeField] private int hp1MissileAmount = 5;

        [Separator("On Boss Dead")]
        [SerializeField] private Flowchart deadFlowchart;

        private bool hasFiredOnce = false;
        [SerializeField] private float timeElapsed;

        public EventHandler<OnTakeDamageArgs> onTakeDamageEvent; 
        public class OnTakeDamageArgs : EventArgs
        {
            public float maxHp;
            public float currentHp;

            public OnTakeDamageArgs(float _maxHp, float _currentHp)
            {
                this.maxHp = _maxHp;
                this.currentHp = _currentHp;
            }
        }

        public System.EventHandler onDead;

        private void Update()
        {
            CheckBossState();
        }

        private void CheckBossState()
        {
            Debug.Log("Fight 2 Boss : Checking Boss State");

            if(currentHp <= 5 && currentHp > 3) LaunchMissile(hp5MissileAmount);
            else if(currentHp <= 3 &&  currentHp > 1) LaunchMissile(hp3MissileAmount);
            else LaunchMissile(hp1MissileAmount);
        }

        private void LaunchMissile(int _missileAmount)
        {
            timeElapsed += Time.deltaTime;

            if(timeElapsed > firstCooldown && !hasFiredOnce)
            {
                ShotMultipleMissile();
                hasFiredOnce = true;
            }
            else if(timeElapsed > secondCooldown && hasFiredOnce)
            {
                ShotMultipleMissile();
                hasFiredOnce = false;
            }

            void ShotMultipleMissile()
            {
                for (int i = 0; i < _missileAmount; i++)
                {
                    Fight2GameManager.Instance.ShotMissile();
                }
                ResetTimer();
            }
        }

        public override void OnTakeDamage(float _damage)
        {
            base.OnTakeDamage(_damage);
            ResetTimer();

            onTakeDamageEvent?.Invoke(this, new OnTakeDamageArgs(maxHp, currentHp));
        }

        public override void OnDestroyed()
        {
            deadFlowchart.gameObject.SetActive(true);
            onDead?.Invoke(this, EventArgs.Empty);
        }

        public void ResetTimer() => timeElapsed = 0f;
    }
}
