using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.MiniGame.Fight2
{
    public class CannonBehaviour : MonoBehaviour
    {
        [SerializeField] private CannonState cannonState;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

    public enum CannonState
    {
        Empty,
        Loaded
    }
}
