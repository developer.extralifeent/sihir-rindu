using Fungus;
using MyBox;
using OLJ.Gameplay;
using OLJ.MiniGame.Fight2;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Unmodular",
                   "Chapter 2 Boss 2 On Take Damage",
                   "Give Damage to boss 2 chapter 2")]
    public class Fight2BossHpFungusCommand : Command
    {
        [Separator("Class References")]
        [SerializeField] private CameraShake camShake;

        [Separator("Boss stuff")]
        [SerializeField] private Fight2Boss fight2Boss;

        public override void OnEnter()
        {
            base.OnEnter();

            camShake.ShakeCam();
            fight2Boss.OnTakeDamage(1f);

            Continue();
        }
    }
}