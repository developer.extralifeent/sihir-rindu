using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipCutsecen : MonoBehaviour
{
    [SerializeField] GameObject[] ObjectToStop, ObjectToInstance;
    private void Update() {
        if (Input.GetAxis("Cancel") > 0) {
            foreach(GameObject Flow in ObjectToStop) {
                Flow.SetActive(false);
            }
           foreach(GameObject Flow in ObjectToInstance) {
                Flow.SetActive(true);
            }
           
        }
    }
}
