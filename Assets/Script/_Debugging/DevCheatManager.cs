using OLJ;
using OLJ.Character;
using OLJ.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace OLJ.DevCheat
{
    public class DevCheatManager : Singleton<DevCheatManager>
    {
        [Header("HotKeys")]
        [SerializeField] private KeyCode togglerButton;
        [SerializeField] private KeyCode timeScaleBtn;

        [Header("Interactable")]
        [SerializeField] private GameObject[] interactables;
        private List<GameObject> spawnedItem = new List<GameObject>();

        [Header("AI")]
        private bool isAgentActive = true;
        public bool IsAgentActive => isAgentActive;

        [Header("Collider visual")]
        private bool isColliderVisualActive = false;
        public bool IsColliderVisualActive => isColliderVisualActive;

        [Header("Time scaler")]
        [SerializeField] private float timeScaleMultiplier = 5f;
        private bool isSpeedUp;

        public Action toggleCheatUI;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(togglerButton))
            {
               toggleCheatUI?.Invoke();
                
               PauseGameOnUIOpen();
            }

            SetTimeScaler();
        }

        public void SetTimeScaler()
        {
            if (Input.GetKeyDown(timeScaleBtn))
            {
                isSpeedUp = !isSpeedUp;
            }

            if (isSpeedUp && !LoadPauseMenu.Instance.Ispaused) Time.timeScale = timeScaleMultiplier;
            else
            {
                if (LoadPauseMenu.Instance.Ispaused) return;
                Time.timeScale = 1f;
            }
        }

        public void PauseGameOnUIOpen()
        {
            if (Time.timeScale != 0) Time.timeScale = 0f;
            else Time.timeScale = 1f;
        }

        public void SpawnInteractableItem(int _interactableIndex)
        {
            PlayerController player = FindObjectOfType<PlayerController>();

            GameObject spawnedInteractable = Instantiate(interactables[_interactableIndex]);
            spawnedInteractable.transform.position = player.transform.position + -Vector3.forward * 15f;
            spawnedItem.Add(spawnedInteractable);
        }

        public void ToggleAnimState()
        {
            PlayerStateDisplayer _playerStateDisplayer = FindObjectOfType<PlayerStateDisplayer>();
            _playerStateDisplayer.ToggleAnimState();
        }

        public void ToggleMonsterNavmesh()
        {
            NavMeshAgent[] agents = FindObjectsOfType<NavMeshAgent>();

            if(agents.Length <= 0) return;

            isAgentActive = !isAgentActive;

            Debug.Log(agents[0].name);

            foreach(NavMeshAgent agent in agents)
            {
                agent.enabled = isAgentActive;
            }
        }

        public void ToggleColliderVisual()
        {
            PlayerController player = FindObjectOfType<PlayerController>();
            Transform playerCollider = player.transform.Find("PlayerCapsule");

            if(playerCollider == null) return;

            Debug.Log(playerCollider.name);
            isColliderVisualActive = !isColliderVisualActive;
            playerCollider.gameObject.SetActive(isColliderVisualActive);  
        }

        public void ResetCondition()
        {
            #region Destroying All Spawn Interactables
            foreach (GameObject spawnedInteractable in spawnedItem)
            {
                Destroy(spawnedInteractable.gameObject);
            }
            spawnedItem.Clear();
            #endregion

            #region Turn Off Anim State Displayer
            PlayerStateDisplayer _playerStateDisplayer = FindObjectOfType<PlayerStateDisplayer>();
            _playerStateDisplayer.IsTextActive = true;
            _playerStateDisplayer.ToggleAnimState();
            #endregion

            #region Turn On Monster Navmesh
            NavMeshAgent[] agents = FindObjectsOfType<NavMeshAgent>();
            isAgentActive = true;

            foreach (NavMeshAgent agent in agents)
            {
                agent.enabled = isAgentActive;
            }
            #endregion

            #region Collider Visual
            PlayerController player = FindObjectOfType<PlayerController>();
            Transform playerCollider = player.transform.Find("PlayerCapsule");

            if (playerCollider == null) return;

            isColliderVisualActive = false;
            playerCollider.gameObject.SetActive(false);
            #endregion
        }

        public void SetHpAmount(string _hpAmount)
        {
            PlayerHp player = FindObjectOfType<PlayerHp>();
            player.SetHp(_hpAmount);
        }

        public void ToggleBrokenHp()
        {
            PlayerHp playerHp = FindObjectOfType<PlayerHp>();
            playerHp.IsBroken = !playerHp.IsBroken;
        }
    }
}
