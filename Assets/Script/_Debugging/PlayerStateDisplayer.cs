using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerStateDisplayer : MonoBehaviour
{
    [Header("Animator states")]
    [SerializeField] private Animator playerAnimator;
    [SerializeField] private TMP_Text currentAnimText;

    [Header("Cheat Sheet")]
    [SerializeField] private KeyCode toggleTextButton;
    private bool isTextActive = false;
    public bool IsTextActive
    {
        get { return isTextActive; }
        set { isTextActive = value; }
    }

    private void Awake()
    {
        playerAnimator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(toggleTextButton))
        {
            ToggleAnimState();
        }
        currentAnimText.text = playerAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.name;
    }

    public void ToggleAnimState()
    {
        isTextActive = !isTextActive;
        currentAnimText.gameObject.SetActive(isTextActive);
    }
}
