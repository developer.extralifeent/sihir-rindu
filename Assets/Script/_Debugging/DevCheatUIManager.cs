using DG.Tweening;
using OLJ.Character;
using OLJ.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine;

namespace OLJ.DevCheat
{
    public class DevCheatUIManager : Singleton<DevCheatUIManager> {

        [Header("Arena Skipping")]
        [SerializeField] CanvasGroup arenaSkippingPanel;
        [SerializeField] private TMP_InputField idInputField;

        public TMP_InputField IdInputField => idInputField;

        [Header("Cheat UI Panel")]
        [SerializeField] private GameObject cheatUI;
        [SerializeField] private TMP_Text cheatMessageText;
        private bool isCheatUIActive;

        [Header("Interactable UI")]
        [SerializeField] private GameObject interactableListUI;
        private bool isInteractableUIActive;

        [Header("Hp Control UI")]
        [SerializeField] private TMP_InputField hpInputField;



        private DevCheatManager devCheatManager;

        private bool isOpenDevPanel;

        // Start is called before the first frame update
        void Start()
        {
            devCheatManager = DevCheatManager.Instance;

            devCheatManager.toggleCheatUI += () =>
            {
                isCheatUIActive = !isCheatUIActive;
                cheatUI.SetActive(isCheatUIActive);
                cheatMessageText.text = string.Empty;
            };
            if (GameManager.Instance.DevMode) {

                ControlManager.Instance.CharacterInputAction.DevButton.SkipArena.Enable();
                ControlManager.Instance.CharacterInputAction.DevButton.SkipArena.performed += ToogleDevPanel;
            } else {
                ControlManager.Instance.CharacterInputAction.DevButton.SkipArena.Disable();
                ControlManager.Instance.CharacterInputAction.DevButton.SkipArena.performed -= ToogleDevPanel;
            }
        }

        private void OnDisable()
        {
            devCheatManager.toggleCheatUI -= () =>
            {
                isCheatUIActive = !isCheatUIActive;
                cheatUI.SetActive(isCheatUIActive);
            };
            //ControlManager.Instance.CharacterInputAction.DevButton.SkipArena.Disable();
            //ControlManager.Instance.CharacterInputAction.DevButton.SkipArena.performed -= ToogleDevPanel;
        }

        private void SetCheatMessageText(string _message)
        {
            cheatMessageText.text = _message;
            StartCoroutine(CheatMessageFade());
        }
        private void ToogleDevPanel(InputAction.CallbackContext context) {
            if (isOpenDevPanel) {
                HideDevPanel();
                isOpenDevPanel = false;
            } else {
                ShowDevPanel();
                isOpenDevPanel = true;
            }
        }

        public void ShowDevPanel() {
            arenaSkippingPanel.DOFade(1, 1f);
        }
        public void HideDevPanel() {
            arenaSkippingPanel.DOFade(0, 1f);
        }
        private IEnumerator CheatMessageFade()
        {
            cheatMessageText.DOFade(1f, .5f);
            yield return new WaitForSeconds(3f);
            cheatMessageText.DOFade(0f, .5f);
        }

        public void ToggleInteractableUI()
        {
            isInteractableUIActive = !isInteractableUIActive;
            interactableListUI.SetActive(isInteractableUIActive);
        }

        public void OnSpawnInteractableButton(int _interactableIndex)
        {
            devCheatManager.SpawnInteractableItem(_interactableIndex);
            SetCheatMessageText("Interactable " + _interactableIndex + " spawned");
        }

        public void OnToggleAnimStateBtn()
        {
            devCheatManager.ToggleAnimState();

            PlayerStateDisplayer playerState = FindObjectOfType<PlayerStateDisplayer>();
            SetCheatMessageText("Anim state toggled: " + playerState.IsTextActive);
        }

        public void OnToggleNavMesh()
        {
            devCheatManager.ToggleMonsterNavmesh();
            SetCheatMessageText("Navmesh toggled: " + devCheatManager.IsAgentActive);
        }

        public void OnToggleColliderVisual()
        {
            devCheatManager.ToggleColliderVisual();
            SetCheatMessageText("Collider visual toggled: " + devCheatManager.IsColliderVisualActive);
        }

        public void OnResetConditionBtn()
        {
            devCheatManager.ResetCondition();
            SetCheatMessageText("Resetting condition");
        }

        public void OnSetHpBtn()
        {
            devCheatManager.SetHpAmount(hpInputField.text);

            if(float.Parse(hpInputField.text) > HPManager.Instance.MaxHp)
                SetCheatMessageText("Cannot set hp more than max hp (" + HPManager.Instance.MaxHp +")");
            else
                SetCheatMessageText("Hp set to " + hpInputField.text);
        }

        public void OnToggleBrokenHp()
        {
            devCheatManager.ToggleBrokenHp();

            PlayerHp playerHp = FindObjectOfType<PlayerHp>();
            SetCheatMessageText("Broken hp toggled: " + playerHp.IsBroken);
        }
    }
}
