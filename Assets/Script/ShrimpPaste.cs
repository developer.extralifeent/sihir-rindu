using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using OLJ.Field;

public class ShrimpPaste : MonoBehaviour
{
    [Header("Shrimp paste values")]
    [SerializeField] private float damage = 50f;
    [SerializeField] private float knockBackForce = 350f;
    

    [Header("Shrimp Paste Config")]
    public OnShrimpPasteEvent onTouchingShrimpPaste;
    float initY;
    [SerializeField] float maxRadius = 40;
    [SerializeField] float spreadDuration;
    [SerializeField] float restartDuration = 1;
    [SerializeField] float fallHeight;
    [SerializeField] int checkpointIndex = 0;
    [SerializeField] GameObject particleToActive;
    [SerializeField] SpriteRenderer lavaSprite;
    Transform visual;
    Transform indicator;
    SphereCollider sphereCollider;

    [System.Serializable] public class OnShrimpPasteEvent : UnityEvent { }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharacterBody>() && other.CompareTag("Player"))
        {
            //if (StageControl.Instance) {
            //    StageControl.Instance.ResetPlayerPosition(checkpointIndex, false);
            //    onTouchingShrimpPaste?.Invoke();
            //} else {
            //    Debug.Log("Failed Call Teleport");
            //}

            CharacterBody cb = other.GetComponent<CharacterBody>();
            cb.Knockback(-cb.LastInput, knockBackForce);

            PlayerHp playerHp = cb.GetComponent<PlayerHp>();
            playerHp.TakeDamage(damage);
        }
    }

    public void Start()
    {
        visual = transform.parent.Find("Visual");
        indicator = transform.parent.Find("Indicator");
        sphereCollider = GetComponent<SphereCollider>();

        initY = visual.position.y;
        StartCoroutine(DroppingRoutine());
    }

    public void OnEnable()
    {
        StartCoroutine(DroppingRoutine());
    }

    IEnumerator DroppingRoutine()
    {
        visual.GetComponent<SpriteRenderer>().color = Color.clear;
        yield return new WaitForSeconds(Random.Range(0f, 2f));
        while (true)
        {
            //naik ke atas
            particleToActive.SetActive(false);
            lavaSprite.transform.localScale = Vector3.zero;
            Color lavaSpriteEndColor= new Color(lavaSprite.color.r, lavaSprite.color.g, lavaSprite.color.b, 1);
            lavaSprite.color = lavaSpriteEndColor;
            visual.gameObject.SetActive(true);
            visual.GetComponent<SpriteRenderer>().color = Color.white;
            visual.position = new Vector3(visual.position.x, initY + fallHeight, visual.position.z);

            //animasi turun ke bawah
            indicator.gameObject.SetActive(true);
            indicator.GetComponent<SpriteRenderer>().color = Color.clear;
            indicator.GetComponent<SpriteRenderer>().DOColor(new Color(0,0,0,0.5f), 0.125f);
            visual.DOMoveY(initY, 1f).SetEase(Ease.InExpo);
            yield return new WaitForSeconds(1f);

            sphereCollider.enabled = true;
            particleToActive.SetActive(true);
            visual.gameObject.SetActive(false);
            lavaSprite.transform.DOScale(maxRadius, spreadDuration);
            indicator.GetComponent<SpriteRenderer>().color = Color.clear;

            //animasi hilang
            yield return new WaitForSeconds(spreadDuration);
            lavaSpriteEndColor = new Color(lavaSprite.color.r, lavaSprite.color.g, lavaSprite.color.b, 0);
            lavaSprite.DOColor(lavaSpriteEndColor, 1);
            visual.GetComponent<SpriteRenderer>().DOColor(Color.clear, 1f);
            sphereCollider.enabled = false;
            yield return new WaitForSeconds(restartDuration);
        }
    }
}
