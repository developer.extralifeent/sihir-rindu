using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthRendering : MonoBehaviour
{
    MeshRenderer ms;
    private void Start()
    {
        ms = GetComponent<MeshRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        ms.sortingOrder = (int)Camera.main.WorldToScreenPoint(this.transform.position).z;
    }
}
