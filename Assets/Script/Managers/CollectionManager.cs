using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Manager {
    public class CollectionManager : Singleton<CollectionManager> {
        public Action onDiaryAdded;
        private List<DiarySO> diaryData = new List<DiarySO>();
        public List<DiarySO> DiaryData {
            get { return diaryData; }
            set { diaryData = value; }
        }
        public void AddToCollection(DiarySO diary) {
            diaryData.Add(diary);
            onDiaryAdded?.Invoke();
        }
        public void RemoveCollection(DiarySO diary) {
            diaryData.Remove(diary);
        }
    }
}

