using MyBox;
using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : Singleton<SpawnerManager>
{
    [Header("Enemy prefabs")]
    [SerializeField] private SuckaSuckaBehaviour suckaSucka;

    public void SpawnSuckaSucka()
    {
        PlayerController cain = FindObjectOfType<PlayerController>();

        float randomXPos = cain.transform.position.x + Random.Range(-50f, 50f);
        float randomZPos = cain.transform.position.z + Random.Range(-12f, 60f);
        Vector3 randomPosition = new Vector3(randomXPos, this.transform.position.y, randomZPos);

        Instantiate(suckaSucka, randomPosition, Quaternion.identity);
    }
}
