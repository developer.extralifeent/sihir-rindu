using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Character;
using OLJ.Manager;
using UnityEngine.SceneManagement;
using TMPro;


public class TestManager : MonoBehaviour
{
    [Header("Class Reference")]
    [SerializeField] private PlayerController pl;

    private void Start() {
        ControlManager.Instance.CurrentBody = pl;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            RestartScene();
        }
    }

    public void PlayFlowchart(Fungus.Flowchart flowchart) {
        if (CutsceneManager.Instance) {
            CutsceneManager.Instance.SetActiveFlowchart(flowchart);
        }
    }

    public void RestartScene()
    {
        SceneManager.UnloadSceneAsync("Testing Level");
        SceneManager.LoadScene("Testing Level", LoadSceneMode.Additive);
    }
}
