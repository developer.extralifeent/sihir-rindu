using DG.Tweening;
using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace OLJ.Manager
{
    public class PauseUIManager : Singleton<PauseUIManager>
    {
        private LoadPauseMenu pauseManager;
        private GameManager gameManager;

        [Separator("Pause Panel")]
        [SerializeField] private CanvasGroup pauseCanvasGroup;
        [SerializeField] private float canvasAlphaTime = .2f;

        [Separator("Event Systems")]
        [SerializeField] private Button firstSelectedPauseBtn;
        [SerializeField] private Button[] pausePanelButtons;
        public Button LastSelectedButton {get; set;}

        // Start is called before the first frame update
        void OnEnable()
        {
            pauseManager = LoadPauseMenu.Instance;
            gameManager = GameManager.Instance;

            pauseManager.onPausedToggled += TogglePausePauseMenu;
        }

        private void OnDisable()
        {
            pauseManager.onPausedToggled -= TogglePausePauseMenu;
        }

        private void TogglePausePauseMenu()
        {
            pauseCanvasGroup.gameObject.SetActive(pauseManager.Ispaused);
            pauseCanvasGroup.DOFade(pauseManager.Ispaused ? 1f : 0f, canvasAlphaTime * Time.unscaledDeltaTime);

            firstSelectedPauseBtn.Select();
            LastSelectedButton = firstSelectedPauseBtn;
        }

        public void TogglePauseViaUI()
        {
            pauseManager.TogglePause();
            TogglePausePauseMenu();
        }

        public void SetAllButtonState()
        {
            foreach(var button in pausePanelButtons)
            {
                if(button == LastSelectedButton)
                {
                    button.Select();
                    continue;
                }
                button.GetComponent<Animator>().SetTrigger("Normal");
            }
        }

        public void MainMenuButton()
        {
            Debug.Log("Loading Main Menu");
            pauseManager.TogglePause();
            gameManager.LoadMainMenu();
        }
    }
}
