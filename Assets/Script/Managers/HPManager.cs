using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using System;
using OLJ.UI;

namespace OLJ.Manager
{
    public class HPManager : Singleton<HPManager>
    {
        private float currentHp;
        public float CurrentHp
        {
            get
            {
                return currentHp;
            }
            set
            {
                currentHp = value;
                onHpChanged?.Invoke(currentHp, maxHp);

                if(currentHp <= 0f)
                {
                    Debug.Log("Player Dead");
                }
            }
        }

        public HpRootUIManager hpRootUIManager { get; set; }

        [Header("Normal Regen System")]
        [SerializeField] private float maxHp = 100f;
        [SerializeField] private float hpToRegen = 1f;
        [SerializeField] private float hpRegenTime = .2f;

        public float MaxHp => maxHp;

        public float HpRegenTime => hpRegenTime;

        public float HpToRegen => hpToRegen;

        [Header("Broken Hp System")]
        [SerializeField] private float brokenHpThreshold;

        public float BrokenHpThreshold => brokenHpThreshold;

        //UI Event
        public Action<float, float> onHpChanged;
        public Action onPlayerDeadInstantly;


        private void Start()
        {
            currentHp = maxHp;
        }
    }
}

