using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Manager;

namespace OLJ.Field
{   
    public class StageControl : Singleton<StageControl>
    {
        [SerializeField] private Character.PlayerController player;
        [SerializeField] private Transform[] checkpoints;
        [SerializeField] private FieldSO stageSO;

        public FieldSO StageSO => stageSO;
        public OLJ.Character.PlayerController Player => player;

        public void LoadCutscene(SubtitleSO sso)
        {
            CutsceneManager.Instance.LoadSubtitle(sso);
        }
        public void TeleportPlayer(Transform target) {
            if (FieldManager.Instance) {
                FieldManager.Instance.TeleportPlayer(target);
            }
        }
        public void PlayFlowchart(Fungus.Flowchart flowchart) {
            if (CutsceneManager.Instance) {
                CutsceneManager.Instance.SetActiveFlowchart(flowchart);
            }
        }
        public void ResetPlayerPosition(int cpIndex = -1, bool stopFlowchart = true)
        {
            if(stopFlowchart)
                CutsceneManager.Instance.StopActiveFlowchart();

            if (cpIndex == -1)
                player.transform.position = checkpoints[0].transform.position;
            else
                player.transform.position = checkpoints[cpIndex].transform.position;
        }

        public void ReEnableGameObject(GameObject _go)
        {
            _go.SetActive(false);
            _go.SetActive(true);
        }
    }
}

