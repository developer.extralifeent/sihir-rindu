using DG.Tweening;
using Fungus;
using MyBox;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace OLJ.Manager
{
    public class NameInputManager : Singleton<NameInputManager>
    {
        [Separator("Panel Parent")]
        [SerializeField] private CanvasGroup parentCanvasGroup;
        [SerializeField] private MainMenuUIManager mainMenuUIManager;

        [Separator("Texts")]
        [SerializeField] private TMP_Text momNameText;
        [SerializeField] private TMP_Text childNameText;

        [Separator("Name Panel GO")]
        [SerializeField] private GameObject momNamePanel;
        [SerializeField] private GameObject childNamePanel;

        [Separator("Name Input Field")]
        [SerializeField] private TMP_InputField momNameInputField;
        [SerializeField] private TMP_InputField childNameInputField;

        [Separator("Confirmation")]
        [SerializeField] private GameObject confirmationPanel;
        [SerializeField] private Button confirmBtn;

        [Separator("Introductions")]
        [SerializeField] private Image introductionPanel;
        [SerializeField] private Flowchart[] introductionFlowchart;

        private void Start()
        {
            if(NameManager.Instance.ChildName != null && NameManager.Instance.MomName != null)
            {
                parentCanvasGroup.gameObject.SetActive(false);
                introductionPanel.gameObject.SetActive(false);
                mainMenuUIManager.enabled = true;
                return;
            }

            introductionPanel.gameObject.SetActive(true);
            introductionFlowchart[0].gameObject.SetActive(true);
        }

        public void SetMomName()
        {
            if (momNameInputField.text == string.Empty || string.IsNullOrWhiteSpace(momNameInputField.text))
            {
                NameManager.Instance.MomName = NameManager.Instance.GetRandomMomName().GetRandomName();
            }
            else
            {
                NameManager.Instance.MomName = momNameInputField.text;
            }

            momNameText.text = "Mom's Name : " + NameManager.Instance.MomName;
            childNameText.text = "Kid's Name : " + NameManager.Instance.ChildName;

            momNamePanel.SetActive(false);
            //confirmationPanel.SetActive(true);

            introductionFlowchart[2].gameObject.SetActive(true);
        }

        public void SetChildName()
        {
            momNamePanel.SetActive(true);
            momNameInputField.Select();

            childNamePanel.SetActive(false);

            if (childNameInputField.text == string.Empty || string.IsNullOrWhiteSpace(childNameInputField.text))
            {
                NameManager.Instance.ChildName = NameManager.Instance.GetRandomKidName().GetRandomName();
            }
            else
            {
                NameManager.Instance.ChildName = childNameInputField.text;
            }

            introductionFlowchart[1].gameObject.SetActive(true);
        }

        public void CancelSetData()
        {
            confirmationPanel.SetActive(false);
            childNamePanel.SetActive(true);

            SelectChildInputField();
        }

        public void ConfirmSetData()
        {
            SaveManager.Instance.SavePlayerName(NameManager.Instance.ChildName, NameManager.Instance.MomName);
            Debug.Log("Names data saved");

            StartCoroutine(ConfirmDataCoroutine());

            introductionFlowchart[3].gameObject.SetActive(true);

            IEnumerator ConfirmDataCoroutine()
            {
                LeanTween.alphaCanvas(parentCanvasGroup, 0f, 1.5f);
                yield return new WaitForSeconds(2f);
                mainMenuUIManager.enabled = true;
            } 
        }

        public void SelectChildInputField() => childNameInputField.Select();
        public void SelectConfirmButton() => confirmBtn.Select();
    }
}

