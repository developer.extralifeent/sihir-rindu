using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLJ.Manager;
using DG.Tweening;
using Spine.Unity;
using UnityEngine.InputSystem;
using System;

namespace OLJ.UI {
    public class QTEUIManager : Singleton<QTEUIManager> {
        [Header("-------Mash Button QTE UI-------")]
        [SerializeField] private Slider mashingSlider;
        [SerializeField] private List<Image> mashQteButtonIcon;

        [Header("-------Button Pressing QTE UI-------")]
        [SerializeField] private Slider buttonPressingSlider;
        [SerializeField] private List<Image> pressingQteButtonIcon;

        [Header("-------Sequence Button QTE UI-------")]
        [SerializeField] private RectTransform sequenceButtonPanel;
        [SerializeField] private SequenceButtonSections sections;
        private List<AdaptiveIcon> sequenceButtonList = new List<AdaptiveIcon>();

        [Header("-------Precision Pressing QTE UI-------")]
        [SerializeField] private RectTransform pressingSliderParent;
        [SerializeField] private List<Image> precisionQteButtonIcon;
        [SerializeField] private RectTransform precisionArea;
        [SerializeField] private RectTransform sliderHandle;

        private Coroutine mashingCoroutine;
        private Coroutine buttonPressCoroutine;

        public RectTransform PrecisionArea
        { 
            get {return precisionArea;}
        }

        public RectTransform PressingSliderParent => pressingSliderParent;
        public RectTransform SliderHandle => sliderHandle;

        [Header("-------Others-------")]
        [SerializeField] SkeletonGraphic sg;
        [SerializeField] CanvasGroup qteAnimGroup;
        public SkeletonGraphic SkeletonGraph => sg;
        private float tempQTEValue;
        delegate void QTEUIFunctions();
        private List<QTEUIFunctions> showFunction=new List<QTEUIFunctions>();
        private List<QTEUIFunctions> hideFunction=new List<QTEUIFunctions>();

        private void Start() {
            AddShowFunction();
            AddHideFunction();

            SubscribeToEvent();
        }

        #region Set Up Functions

        private void Update()
        {
            Vector3 sliderHandlePos = new(sliderHandle.localPosition.x, sliderHandle.localPosition.y, sliderHandle.localPosition.z);
            sliderHandlePos.x = QTEManager.Instance.CurrentSliderValue;
            sliderHandle.localPosition = sliderHandlePos;
        }

        private void SubscribeToEvent()
        {
            if (QTEManager.Instance)
            {
                //Show and hide QTE UI
                QTEManager.Instance.onQTEexecute.AddListener(ShowUI);

                //Update Sequence QTE UI
                QTEManager.Instance.onButtonCorrect += HideCorrectButton;
            }
        }

        private void AddShowFunction()
        {
            showFunction.Add(ShowMashButtonUI);
            showFunction.Add(ShowButtonPressingUI);
            showFunction.Add(ShowSequenceButton);
            showFunction.Add(ShowPrecisionPressingUI);
        }

        private void AddHideFunction()
        {
            hideFunction.Add(HideMashButtonUI);
            hideFunction.Add(HideButtonPressingUI);
            hideFunction.Add(HideSequenceButtonUI);
            hideFunction.Add(HidePrecisionPressingUI);
        }

        public void SetQTEAnimation(SkeletonDataAsset sk, string animName)
        {
            sg.gameObject.SetActive(true);
            sg.skeletonDataAsset = sk;
            sg.Initialize(true);
            sg.AnimationState.SetAnimation(0, animName, true);
            qteAnimGroup.DOFade(1, 0.2f);
        }
        public void HideQTEAnimation() {
            sg.gameObject.SetActive(false);
            qteAnimGroup.DOFade(0, 0.2f).OnComplete(() => {

            });           
        }
        private void ShowUI(int qteIndex) {
            HideAllQTEUI();
            QTEManager.Instance.onQTEfinished.RemoveListener(HideUI);
            QTEManager.Instance.onQTEfinished.AddListener(HideUI);
            showFunction[qteIndex].Invoke();
            Debug.Log($"Showed QTE UI {qteIndex}");
        }
        private void HideAllQTEUI() {
            // Ensure all QTE UIs are hidden before showing a new one
            for (int i = 0; i < hideFunction.Count; i++) {
                hideFunction[i].Invoke();
            }
        }
        private void HideUI(int qteIndex) {
            foreach (QTEUIFunctions qteFunc in hideFunction) {
                qteFunc.Invoke();
            }           
        }
        #endregion

        #region Mash Button QTE
        private void ShowMashButtonUI() {
            mashingSlider.gameObject.SetActive(true);
            mashingCoroutine = StartCoroutine(MashingCouroutine());
        }
        private void HideMashButtonUI() {
            mashingSlider.gameObject.SetActive(false);
            mashingSlider.value = 0;
            if (mashingCoroutine != null) {
                StopCoroutine(mashingCoroutine);
                mashingCoroutine = null;
            }
        }
        IEnumerator MashingCouroutine()
        {
            for (int i = 0; i < mashQteButtonIcon.Count; i++)
            {
                mashQteButtonIcon[i].gameObject.SetActive(QTEManager.Instance.RandomQTE == i);
            }
            while (true)
            {
                mashingSlider.maxValue = QTEManager.Instance.CountThreshold;
                if (tempQTEValue != QTEManager.Instance.Count)
                {
                    DOTween.To(() => mashingSlider.value, x => mashingSlider.value = x, QTEManager.Instance.Count, 0.1f);
                }
                tempQTEValue = QTEManager.Instance.Count;
                yield return null;
            }
        }
        #endregion

        #region Button Pressing QTE
        private void ShowButtonPressingUI() {
            buttonPressingSlider.gameObject.SetActive(true);
            buttonPressCoroutine = StartCoroutine(ButtonPressingCoroutine());
        }
        private void HideButtonPressingUI()
        {
            buttonPressingSlider.gameObject.SetActive(false);
            buttonPressingSlider.value = 0;
            if (buttonPressCoroutine != null) {
                StopCoroutine(buttonPressCoroutine);
                buttonPressCoroutine = null;
            }
        }

        private IEnumerator ButtonPressingCoroutine()
        {
            for (int i = 0; i < pressingQteButtonIcon.Count; i++)
            {
                pressingQteButtonIcon[i].gameObject.SetActive(QTEManager.Instance.RandomQTE == i);
            }
            while (true)
            {
                buttonPressingSlider.maxValue = QTEManager.Instance.PressingDurationThreshold;
                if (tempQTEValue != QTEManager.Instance.PressingAmount)
                {
                    DOTween.To(() => buttonPressingSlider.value, x => buttonPressingSlider.value = x, QTEManager.Instance.PressingAmount, 0.1f);
                }
                tempQTEValue = QTEManager.Instance.PressingAmount;
                yield return null;
            }
        }
        #endregion

        #region Sequence Pressing QTE
        private void ShowSequenceButton() {
            sequenceButtonPanel.gameObject.SetActive(true);

            sequenceButtonList.Add(sections.firstSectionBtns[QTEManager.Instance.RandomQTEHolder[0]]);
            sequenceButtonList.Add(sections.secondSectionBtns[QTEManager.Instance.RandomQTEHolder[1]]);
            sequenceButtonList.Add(sections.thirdSectionBtns[QTEManager.Instance.RandomQTEHolder[2]]);
            sequenceButtonList.Add(sections.fourthSectionBtns[QTEManager.Instance.RandomQTEHolder[3]]);

            foreach (var section in sequenceButtonList)
            {
                section.gameObject.SetActive(true);
            }
        }
        private void HideSequenceButtonUI()
        {
            sequenceButtonPanel.gameObject.SetActive(false);

            for (int i = 0; i < sequenceButtonList.Count; i++)
            {
                sequenceButtonList[i].gameObject.SetActive(false);
            }

            sequenceButtonList.Clear();
        }

        private void HideCorrectButton(int _buttonIndex)
        {
            sequenceButtonList[_buttonIndex].gameObject.SetActive(false);
        }
        #endregion

        #region Precision Pressing QTE
        private void ShowPrecisionPressingUI() {
            pressingSliderParent.gameObject.SetActive(true);
            //ressingSlider.value = QTEManager.Instance.CurrentSliderValue;

            precisionQteButtonIcon[QTEManager.Instance.RandomQTE].gameObject.SetActive(true);

            precisionArea.sizeDelta = QTEManager.Instance.PrecisionAreaXScale;

        }

        private void HidePrecisionPressingUI()
        {
            pressingSliderParent.gameObject.SetActive(false);

            precisionQteButtonIcon[QTEManager.Instance.RandomQTE].gameObject.SetActive(false);
        }
        #endregion
        private void OnDisable() {
            if (QTEManager.Instance) {
                //Show and hide QTE UI
                QTEManager.Instance.onQTEexecute.RemoveListener(ShowUI);
                QTEManager.Instance.onQTEfinished.RemoveListener(HideUI);

                //Update Sequence QTE UI
                QTEManager.Instance.onButtonCorrect -= HideCorrectButton;
            }
        }
    }
    
    [Serializable]
    public struct SequenceButtonSections
    {
        public AdaptiveIcon[] firstSectionBtns;
        public AdaptiveIcon[] secondSectionBtns;
        public AdaptiveIcon[] thirdSectionBtns;
        public AdaptiveIcon[] fourthSectionBtns;
    }
}

