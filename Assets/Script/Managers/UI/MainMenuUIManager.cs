using MyBox;
using System;
using Spine.Unity;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;
using Fungus;
using DG.Tweening.Core.Easing;
using OLJ.UI;

namespace OLJ.Manager
{
    public class MainMenuUIManager : Singleton<MainMenuUIManager>
    {
        private ControlManager controlManager;
        private SaveManager saveManager;
        private GameManager gameManager;

        [Separator("Class References")]
        [SerializeField] private MenuButtonManager menuButtonManager;
        [SerializeField] private TitleMenuController titleMenuController;

        [Separator("Panels")]
        [SerializeField] private CanvasGroup titleCanvasGroup;
        [SerializeField] private CanvasGroup bookCanvasGroup;
        [SerializeField] private CanvasGroup readBookBtnCanvasGroup;
        [SerializeField] private CanvasGroup chaptersCanvasGroup;
        [SerializeField] private CanvasGroup optionCanvasGroup;
        [SerializeField] private CanvasGroup prevNextBtnCanvasGroup;
        [SerializeField] private CanvasGroup settingPanelCanvasGroup;
        [SerializeField] private CanvasGroup creditCanvasGroup;
        public CanvasGroup CreditCanvasGroup { get { return this.creditCanvasGroup; } }

        [Separator("Transition")]
        [SerializeField] private float transitionDuration = 1.5f;

        [Separator("Book Animation")]
        [SerializeField] private SkeletonGraphic bookSkeleton;
        [SpineAnimation(dataField = "bookSkeleton"), SerializeField] private string bookOpenAnim;
        [SpineAnimation(dataField = "bookSkeleton"), SerializeField] private string bookSmallFlipAnim;
        [SpineAnimation(dataField = "bookSkeleton"), SerializeField] private string bookFlipAnim;

        [Separator("Mom and Son Animation")]
        [SerializeField] private SkeletonGraphic momSonSkeleton;
        [SpineAnimation(dataField = "momSonSkeleton"), SerializeField] private string momSonOpenBookAnim;
        [SpineAnimation(dataField = "momSonSkeleton"), SerializeField] private string momSonFlipPageAnim;
        private bool isPlayingAnim;

        [Separator("Chapter buttons")]
        [SerializeField] private Button[] chapterButtons;
        [SerializeField] private Image[] chapterImages;
        private int currentBtnIndex = 0;

        [Separator("Chapter UIs")]
        [SerializeField] private TMP_Text chapterTitleTxt;
        [SerializeField] private TMP_Text chapterSubtitleTxt;
        [SerializeField] private Image chapterImg;
        [SerializeField] private Image collectibleImg;
        [SerializeField] private Transform collectibleImgParent;
        private List<Image> collectibleImgList = new List<Image>();

        [Separator("Chapters")]
        [SerializeField] private FieldSO[] chapter1Fields;
        [SerializeField] private FieldSO[] chapter2Fields;
        [SerializeField] private FieldSO[] chapter3Fields;
        [SerializeField] private FieldSO[] chapter4Fields;
        [SerializeField] private FieldSO[] chapter5Fields;
        [SerializeField] private FieldSO[] chapter6Fields;
        [SerializeField] private FieldSO[] chapter7Fields;
        private FieldSO[] selectedChapterSO;
        private FieldSO selectedSubchapterSO;
        private int currentSelectedChapter = 1;
        private int currentSelectedSubchapter = 0;

        [Separator("Resolutions")]
        [SerializeField] private Resolutions[] resolutions;
        [SerializeField] private TMP_Text resolutionText;
        private int resolutionIndex = 0;

        [Separator("VSync")]
        [SerializeField] private bool isVSyncOn = true;
        [SerializeField] private Toggle vSyncToggleBtn;

        [Separator("Full Screen")]
        [SerializeField] private bool isFullScreen = true;
        [SerializeField] private Toggle fullScreenToggle;

        [Separator("Options")]
        [SerializeField] private Button[] optionButtons;

        [Separator("Settings")]
        [SerializeField] private Button[] settingBtns;
        private Button currentSelectedSettingBtn;
        private int currentSettingBtnIndex;
        private bool isSettingOpen = false;

        [Separator("Credits")]
        [SerializeField] Flowchart skipCreditFlowchart;
        [SerializeField] string skipCreditBlockName;

        [Separator("Others")]
        [SerializeField] private TMP_Text readBookTxt;
        [SerializeField] private CanvasGroup nameCanvasGroup;
        [SerializeField] private TMP_Text momNameTxt;
        [SerializeField] private TMP_Text sonNameTxt;

        private bool isBookAppeared = false;
        private bool isBookOpen = false;
        private bool isOptionOpen = false;
        public bool IsCreditOpen { get; set; } = false;

        private List<CanvasGroup> panelStack = new List<CanvasGroup>();

        protected override void Awake()
        {
            base.Awake();
            controlManager = ControlManager.Instance;
            saveManager = SaveManager.Instance;
            gameManager = GameManager.Instance;
        }

        private void OnEnable()
        {
            controlManager.SwitchInputAction(controlManager.CharacterInputAction.MainMenu);
            //ToggleChangeResInput(false);
        }

        // Start is called before the first frame update
        void Start()
        {
            LoadSetting();
            LoadMainMenuIndex();
            OneShotAnyKeyListener();
            SetBookName();

            //Open and read book
            //controlManager.CharacterInputAction.MainMenu.AnyBtn.performed += OnPressAnyButton;
            controlManager.CharacterInputAction.MainMenu.Submit.performed += SubmitInput;

            //Changing Chapter Selection
            controlManager.CharacterInputAction.MainMenu.NextChapter.performed += NextChapterInput;
            controlManager.CharacterInputAction.MainMenu.PrevChapter.performed += PrevChapterInput;

            //Changing Subchapter Selection
            controlManager.CharacterInputAction.MainMenu.HorizontalRight.performed += NextSubchapterInput;
            controlManager.CharacterInputAction.MainMenu.HorizontalLeft.performed += PrevSubchapterInput;

            //Close Panel
            controlManager.CharacterInputAction.MainMenu.Cancel.performed += BackInput;

            //Change Setting Button
            controlManager.CharacterInputAction.MainMenu.VerticalUp.performed += ChangeSettingnBtnUp;
            controlManager.CharacterInputAction.MainMenu.VerticalDown.performed += ChangeSettingnBtnDown;
        }

        private void OnDisable()
        {
            //Open and read book
            //controlManager.CharacterInputAction.MainMenu.AnyBtn.performed -= OnPressAnyButton;
            controlManager.CharacterInputAction.MainMenu.Submit.performed -= SubmitInput;

            //Changing Chapter Selection
            controlManager.CharacterInputAction.MainMenu.NextChapter.performed -= NextChapterInput;
            controlManager.CharacterInputAction.MainMenu.PrevChapter.performed -= PrevChapterInput;

            //Changing Subchapter Selection
            controlManager.CharacterInputAction.MainMenu.HorizontalRight.performed -= NextSubchapterInput;
            controlManager.CharacterInputAction.MainMenu.HorizontalLeft.performed -= PrevSubchapterInput;

            //Close Panel
            controlManager.CharacterInputAction.MainMenu.Cancel.performed -= BackInput;
        }

        #region InputEvents
        private IEnumerator OnPressAnyButtonCoroutine()
        {
            LeanTween.alphaCanvas(titleCanvasGroup, 0f, transitionDuration);
            yield return new WaitForSeconds(transitionDuration);
            LeanTween.alphaCanvas(bookCanvasGroup, 1f, transitionDuration);

            isBookAppeared = true;
        }

        private void OneShotAnyKeyListener()
        {
            InputSystem.onAnyButtonPress.CallOnce(ctrl =>
            {
                if (isBookAppeared) return;

                StartCoroutine(OnPressAnyButtonCoroutine());
                Debug.Log(ctrl + " Was Pressed");
            });
        }

        private void SubmitInput(InputAction.CallbackContext _context)
        {
            if (!isBookOpen && !isBookAppeared) return;

            //Open book
            if (!isBookOpen)
            {
                ToggleHorizontalDPadInput(true);

                momSonSkeleton.AnimationState.SetAnimation(0, momSonOpenBookAnim, true);
                StartCoroutine(ChangeReadButton());
                LeanTween.alphaCanvas(prevNextBtnCanvasGroup, 1f, transitionDuration);
                LeanTween.alphaCanvas(nameCanvasGroup, 0f, transitionDuration);

                StartCoroutine(ChangeChapterUI(false, false, true));

                isBookOpen = true;

                IEnumerator ChangeReadButton()
                {
                    LeanTween.alphaCanvas(readBookBtnCanvasGroup, 0f, .5f);
                    yield return new WaitForSeconds(.5f);
                    readBookTxt.text = "Read";
                    LeanTween.alphaCanvas(readBookBtnCanvasGroup, 1f, .5f);
                }
            }
            //Change Setting
            else if (isSettingOpen)
            {
                switch (currentSettingBtnIndex)
                {
                    case 0:
                        Debug.Log("INI TOMBOL RESOLUTION KOCAK");
                        break;
                    case 1:
                        ToggleVSync();
                        break;
                    case 2:
                        ToggleFullScreen();
                        break;
                }
            }
            //Start Chapter
            else if(!isOptionOpen && !isSettingOpen)
            {
                Debug.Log("StartChapter");
                titleMenuController.StartFieldScene(selectedSubchapterSO);
            }
        }

        private void NextChapterInput(InputAction.CallbackContext _context) => ChangeChapter(true);

        private void PrevChapterInput(InputAction.CallbackContext _context) => ChangeChapter(false);

        private void NextSubchapterInput(InputAction.CallbackContext _context) => DPadHorizontalInput(true);
        
        private void PrevSubchapterInput(InputAction.CallbackContext _context) => DPadHorizontalInput(false);

        private void ChangeSettingnBtnUp(InputAction.CallbackContext _context) => ChangeSettingnBtn(true);
        
        private void ChangeSettingnBtnDown(InputAction.CallbackContext _context) => ChangeSettingnBtn(false);

        private void BackInput(InputAction.CallbackContext _context)
        {
            if(!IsCreditOpen)
            {
                if (panelStack.Count <= 0) return;

                bookSkeleton.AnimationState.ClearTrack(0);
                bookSkeleton.AnimationState.SetAnimation(0, bookSmallFlipAnim, false);
                float openBookAnimDuration = bookSkeleton.Skeleton.Data.FindAnimation(bookFlipAnim).Duration;
                LeanTween.alphaCanvas(panelStack[panelStack.Count - 1], 0f, openBookAnimDuration - .25f);

                panelStack.Remove(panelStack[panelStack.Count - 1]);

                if (panelStack.Count <= 0)
                {
                    ToggleHorizontalDPadInput(true);
                    ToggleChangeChapterInput(true);
                    LeanTween.alphaCanvas(optionCanvasGroup, 1f, openBookAnimDuration - .5f);
                    LeanTween.alphaCanvas(prevNextBtnCanvasGroup, 1f, transitionDuration);

                    optionButtons[0].Select();
                    ToggleInteractableOptionBtn(true);
                }

                if (!panelStack.Contains(settingPanelCanvasGroup)) isSettingOpen = false;
            }
            else
            {
                StartCoroutine(CloseCredit());

                IEnumerator CloseCredit()
                {
                    skipCreditFlowchart.ExecuteBlock(skipCreditBlockName);

                    yield return new WaitForSeconds(1.5f);

                    IsCreditOpen = false;
                    optionButtons[4].Select();
                    ToggleInteractableOptionBtn(true);
                    ControlManager.Instance.CharacterInputAction.MainMenu.Navigate.Enable();
                }
            }
            
        }
        #endregion

        #region Testing Buttons
        [ButtonMethod]
        private void SaveMainMenuIndex()
        {
            saveManager.SaveMainMenuIndex(currentSelectedChapter, currentSelectedSubchapter, currentBtnIndex, selectedChapterSO);
            gameManager.IsPartialChapter = true;
        }

        [ButtonMethod]
        private void LoadMainMenuIndexTest()
        {
            LoadMainMenuIndex();
        }
        #endregion

        private void SetBookName()
        {
            momNameTxt.text = saveManager.PlayerData.grandpopName;
            sonNameTxt.text = saveManager.PlayerData.childName;
        }

        private void LoadMainMenuIndex()
        {
            selectedChapterSO = chapter1Fields;
            selectedSubchapterSO = selectedChapterSO[0];

            if (!gameManager.IsPartialChapter) return;

            isBookAppeared = true;
            isBookOpen = true;

            titleCanvasGroup.alpha = 0f;
            bookCanvasGroup.alpha = 1f;

            saveManager.LoadMainMenuIndexes();

            currentSelectedChapter = saveManager.MainMenuIndex.currentSelectedChpt;
            currentSelectedSubchapter = saveManager.MainMenuIndex.currentSelectedSubchpt;
            currentBtnIndex = saveManager.MainMenuIndex.buttonIndex;
            selectedChapterSO = saveManager.MainMenuIndex.selectedChapterSO;

            selectedSubchapterSO = selectedChapterSO[currentSelectedSubchapter];

            if (currentSelectedSubchapter >= selectedChapterSO.Length - 1)
                ChangeChapter(true);
            else
                ChangeSubChapter(true);

            gameManager.IsPartialChapter = false;
        }

        public void ChangeChapter(bool _isNextChapter)
        {
            if(!isBookOpen || !isBookAppeared || IsCreditOpen) return;

            StartCoroutine(SetSpineAnim());

            if(_isNextChapter)
            {
                currentBtnIndex++;
                currentSelectedChapter++;
                if (currentBtnIndex > chapterButtons.Length - 1)
                {
                    currentBtnIndex = 0;
                    currentSelectedChapter = 1;
                }
            }
            else
            {
                currentBtnIndex--;
                currentSelectedChapter--;   
                if (currentBtnIndex < 0)
                {
                    currentBtnIndex = chapterButtons.Length - 1;
                    currentSelectedChapter = chapterButtons.Length;
                }
            }

            switch (currentSelectedChapter)
            {
                case 1:
                    selectedChapterSO = chapter1Fields;
                    break;
                case 2:
                    selectedChapterSO = chapter2Fields;
                    break;
                case 3:
                    selectedChapterSO = chapter3Fields;
                    break;
                case 4:
                    selectedChapterSO = chapter4Fields;
                    break;
                case 5:
                    selectedChapterSO = chapter5Fields;
                    break;
                case 6:
                    selectedChapterSO = chapter6Fields;
                    break;
                case 7:
                    selectedChapterSO = chapter7Fields;
                    break;
            }

            currentSelectedSubchapter = 0;

            if (selectedChapterSO.Length > 0 && currentSelectedChapter != 8)
                selectedSubchapterSO = selectedChapterSO[0];

            StartCoroutine(ChangeChapterUI(true, false, false));
        }

        private void ChangeSubChapter(bool _isNext)
        {
            if (!isBookOpen || !isBookAppeared || IsCreditOpen) return;

            if (!isSettingOpen && !isOptionOpen)
            {
                if (_isNext)
                {
                    currentSelectedSubchapter++;
                    if (currentSelectedSubchapter > selectedChapterSO.Length - 1)
                        currentSelectedSubchapter = 0;
                }
                else
                {
                    currentSelectedSubchapter--;
                    if (currentSelectedSubchapter < 0)
                        currentSelectedSubchapter = selectedChapterSO.Length - 1;
                }

                if (selectedChapterSO.Length > 0)
                    selectedSubchapterSO = selectedChapterSO[currentSelectedSubchapter];

                StartCoroutine(ToggleChangeSubChapterInput());

                StartCoroutine(ChangeChapterUI(false, true, false));
            }
            else if (isSettingOpen && isOptionOpen)
            {
                ChangeResolution(_isNext);
            }

            IEnumerator ToggleChangeSubChapterInput()
            {
                ToggleHorizontalDPadInput(false);

                float openBookAnimDuration = bookSkeleton.Skeleton.Data.FindAnimation(bookSmallFlipAnim).Duration;
                yield return new WaitForSeconds(openBookAnimDuration);

                ToggleHorizontalDPadInput(true);
            }
        }

        private IEnumerator SetSpineAnim()
        {
            if(isPlayingAnim) yield break;

            StopCoroutine(SetSpineAnim());

            isPlayingAnim = true;

            momSonSkeleton.AnimationState.SetAnimation(0, momSonFlipPageAnim, true);

            float openBookAnimDuration = momSonSkeleton.Skeleton.Data.FindAnimation(momSonFlipPageAnim).Duration;
            yield return new WaitForSeconds(openBookAnimDuration - .1f); //REMOVE ( -.1f ) WHEN FINAL ASSET DONE

            momSonSkeleton.AnimationState.SetAnimation(0, momSonOpenBookAnim, true);

            isPlayingAnim = false;
        }

        private void DPadHorizontalInput(bool _isNext)
        {
           ChangeSubChapter(_isNext);
        }

        private IEnumerator ChangeChapterUI(bool _playFlipAnim = true, bool _playSmallFlipAnim = false, bool _playCoverAnim = true)
        {
            StopCoroutine(ChangeChapterUI());

            float fadeInDuration = .2f;
            float openBookAnimDuration = bookSkeleton.Skeleton.Data.FindAnimation(bookSmallFlipAnim).Duration;

            chaptersCanvasGroup.alpha = 0f;
            optionCanvasGroup.alpha = 0f;

            //Disable Input So Player Cannot Spam
            ToggleChangeChapterInput(false);
            //ToggleHorizontalDPadInput(false);

            bookSkeleton.AnimationState.ClearTrack(0);
            bookSkeleton.Skeleton.SetToSetupPose();
            if (_playCoverAnim)
                bookSkeleton.AnimationState.SetAnimation(0, bookOpenAnim, false);
            else if(_playFlipAnim)
                bookSkeleton.AnimationState.SetAnimation(0, bookFlipAnim, false);
            else if(_playSmallFlipAnim)
                bookSkeleton.AnimationState.SetAnimation(0, bookSmallFlipAnim, false);


            if(prevNextBtnCanvasGroup.alpha != 1f)
            {
                LeanTween.alphaCanvas(prevNextBtnCanvasGroup, 1, transitionDuration);
            }

            for (int i = 0; i < chapterButtons.Length; i++)
            {
                chapterImages[i].DOKill();
                chapterImages[i].color = new Color(1f, 1f, 1f, 0f);
            }

            if (currentSelectedChapter != 8)
            {
                if(panelStack.Contains(optionCanvasGroup))
                {
                    panelStack.Remove(optionCanvasGroup);
                    ToggleHorizontalDPadInput(true);
                }

                ToggleInteractableOptionBtn(false);

                //chaptersCanvasGroup.DOKill();

                yield return new WaitForSeconds(openBookAnimDuration - .5f);

                //Change Chapter Details
                chapterTitleTxt.text = selectedSubchapterSO.chapterTitle;
                chapterSubtitleTxt.text = selectedSubchapterSO.chapterSubtitle;
                chapterImg.sprite = selectedSubchapterSO.chapterImage;

                //Collectible Item List
                foreach (Transform child in collectibleImgParent.transform)
                {
                    if (child == collectibleImgParent.GetChild(0))
                        continue;

                    Destroy(child.gameObject);
                }

                collectibleImgList.Clear();

                for (int i = 0; i < selectedSubchapterSO.totalCollectible; i++)
                {
                    Image collectible = Instantiate(collectibleImg, collectibleImgParent);
                    collectible.gameObject.SetActive(true);
                    collectible.color = new Color(1f, 1f, 1f, .5f);

                    collectibleImgList.Add(collectible);
                }

                if (selectedSubchapterSO.collectibleCount > 0)
                {
                    for (int i = 0; i < selectedSubchapterSO.collectibleCount; i++)
                    {
                        collectibleImgList[i].color = Color.white;
                    }
                }
                
                LeanTween.alphaCanvas(chaptersCanvasGroup, 1f, fadeInDuration);

                if (readBookBtnCanvasGroup.alpha != 1)
                    LeanTween.alphaCanvas(readBookBtnCanvasGroup, 1f, transitionDuration);

                if(isOptionOpen) isOptionOpen = false;

                yield return new WaitForSeconds(fadeInDuration + .1f);

                //Enable Input So Player Can Change Chapter
                ToggleChangeChapterInput(true);
            }
            else
            {
                LeanTween.alphaCanvas(readBookBtnCanvasGroup, 0f, transitionDuration);

                yield return new WaitForSeconds(openBookAnimDuration - .5f);

                OpenOption(fadeInDuration);

                yield return new WaitForSeconds(fadeInDuration + .1f);

                //Enable Input So Player Can Change Chapter
                ToggleChangeChapterInput(true);

                ToggleHorizontalDPadInput(true);
            }
            FadeChapterImage(openBookAnimDuration);
        }

        private void OpenOption(float _fadeInDuration)
        {
            isOptionOpen = true;

            LeanTween.alphaCanvas(optionCanvasGroup, 1f, _fadeInDuration);

            optionButtons[0].Select();
            ToggleInteractableOptionBtn(true);

            //Disable Input For Changing Subhapter
            //ToggleChangeSubChapterInput(false);
        }

        private void FadeChapterImage(float _fadeDuration)
        {
            for (int i = 0; i < chapterButtons.Length; i++)
            {
               chapterImages[i].DOFade(i == currentBtnIndex ? 1 : 0, _fadeDuration);
            }
        }

        #region SETTINGS
        //Load Setting Data From Save Manager
        private void LoadSetting()
        {
            //Load data from SaveManager
            SaveManager.Instance.LoadPlayerSettings();

            //Set all variables to loaded data
            resolutionIndex = SaveManager.Instance.SettingData.resolutionIndex;
            isVSyncOn = SaveManager.Instance.SettingData.vSync;
            isFullScreen = SaveManager.Instance.SettingData.fullScreen;

            //Assign loaded data to corresponding variable

            //VSync
            QualitySettings.vSyncCount = isVSyncOn ? 1 : 0;
            vSyncToggleBtn.isOn = isVSyncOn;

            //FullScreen and resolution
            fullScreenToggle.isOn = isFullScreen;
            SetResolution();
        }

        private void ChangeResolution(bool _isNext)
        {
            if(currentSelectedSettingBtn != settingBtns[0]) return;

            if (_isNext)
                resolutionIndex = resolutionIndex < resolutions.Length - 1 ? resolutionIndex += 1 : 0;
            else
                resolutionIndex = resolutionIndex > 0 ? resolutionIndex -= 1 : resolutions.Length - 1;

            SetResolution();
        }

        private void SetResolution()
        {
            resolutionText.text = resolutions[resolutionIndex].ResoultionName;
            Screen.SetResolution(resolutions[resolutionIndex].width, resolutions[resolutionIndex].height, isFullScreen);
            SaveManager.Instance.SavePlayerSetting(resolutionIndex, isVSyncOn, isFullScreen);
        }

        public void ToggleVSync()
        {
            isVSyncOn = !isVSyncOn;
            QualitySettings.vSyncCount = isVSyncOn ? 1 : 0;
            vSyncToggleBtn.isOn = isVSyncOn;
        }

        public void ToggleFullScreen()
        {
            isFullScreen = !isFullScreen;
            SetResolution();
            fullScreenToggle.isOn = isFullScreen;
        }

        private void ChangeSettingnBtn(bool _isUp)
        {
            if(!isSettingOpen) return;

            if (_isUp)
            {
                currentSettingBtnIndex--;

                if(currentSettingBtnIndex < 0)
                    currentSettingBtnIndex = settingBtns.Length - 1;
            }
            else
            {
                currentSettingBtnIndex++;

                if (currentSettingBtnIndex > settingBtns.Length - 1)
                    currentSettingBtnIndex = 0;
            }

            currentSelectedSettingBtn = settingBtns[currentSettingBtnIndex];

            for (int i = 0; i < settingBtns.Length; i++)
            {
                if(i ==  currentSettingBtnIndex)
                    settingBtns[i].transform.GetChild(0).GetComponent<Image>().DOFade(.5f, .1f);
                else
                    settingBtns[i].transform.GetChild(0).GetComponent<Image>().DOFade(0f, .1f);
            }
        }

        public IEnumerator OpenSetting()
        {
            isSettingOpen = true;
            currentSelectedSettingBtn = settingBtns[0];
            currentSettingBtnIndex = 0;

            panelStack.Add(settingPanelCanvasGroup);

            bookSkeleton.AnimationState.ClearTrack(0);
            bookSkeleton.AnimationState.SetAnimation(0, bookFlipAnim, false);
            float openBookAnimDuration = bookSkeleton.Skeleton.Data.FindAnimation(bookFlipAnim).Duration;

            ToggleHorizontalDPadInput(true);
            ToggleChangeChapterInput(false);

            LeanTween.alphaCanvas(prevNextBtnCanvasGroup, 0f, transitionDuration);
            LeanTween.alphaCanvas(optionCanvasGroup, 0f, .2f);
            yield return new WaitForSeconds(openBookAnimDuration - .5f);
            LeanTween.alphaCanvas(settingPanelCanvasGroup, 1f, .2f);

            //Set selected button to resolution Btn
            settingBtns[0].transform.GetChild(0).GetComponent<Image>().DOFade(.5f, .1f);
            currentSelectedSettingBtn = settingBtns[0];

            ToggleInteractableOptionBtn(false);
        }
        #endregion

        public void ToggleInteractableOptionBtn(bool _isInteractable)
        {
            for (int i = 0; i < optionButtons.Length; i++)
            {
                optionButtons[i].interactable = _isInteractable;
            }
        }

        public void AddPanelStack(CanvasGroup _newPanel) => panelStack.Add(_newPanel);

        public void RemovePanelStack(CanvasGroup _oldPanel) => panelStack.Remove(_oldPanel);

        private void ToggleAllInput(bool _isOpening)
        {
            if(_isOpening)
                controlManager.CharacterInputAction.MainMenu.Enable();
            else
                controlManager.CharacterInputAction.MainMenu.Disable();
        }

        private void ToggleHorizontalDPadInput(bool _isOpening)
        {
            if(_isOpening)
            {
                //Enable Input For Changing Chapter
                controlManager.CharacterInputAction.MainMenu.HorizontalRight.Enable();
                controlManager.CharacterInputAction.MainMenu.HorizontalLeft.Enable();
            }
            else
            {
                //Disable Input For Changing Chapter
                controlManager.CharacterInputAction.MainMenu.HorizontalRight.Disable();
                controlManager.CharacterInputAction.MainMenu.HorizontalLeft.Disable();
            }
        }

        private void ToggleChangeChapterInput(bool _isOpening)
        {
            if (_isOpening)
            {
                //Enable Input For Changing Chapter
                controlManager.CharacterInputAction.MainMenu.NextChapter.Enable();
                controlManager.CharacterInputAction.MainMenu.PrevChapter.Enable();
            }
            else
            {
                //Disable Input For Changing Chapter
                controlManager.CharacterInputAction.MainMenu.NextChapter.Disable();
                controlManager.CharacterInputAction.MainMenu.PrevChapter.Disable();
            }
        }
    }
}