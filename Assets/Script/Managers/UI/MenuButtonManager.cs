using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OLJ.Manager
{
    public class MenuButtonManager : MonoBehaviour
    {
        [Separator("URLs")]
        [SerializeField] private string discordLink = "https://discord.gg/f5acjUuNKE";
        [SerializeField] private string instagramLink = "https://www.instagram.com/extralifeent";
        [SerializeField] private string twitterLink = "https://x.com/home";
        [SerializeField] private string itchIoLink = "https://elentertainment.itch.io";

        public void OpenDiscordBtn() => Application.OpenURL(discordLink);
        public void OpenInstagramBtn() => Application.OpenURL(instagramLink);
        public void OpenTwitterBtn() => Application.OpenURL(twitterLink);
        public void OpenItchIO() => Application.OpenURL(itchIoLink);
        public void OpenSettings() => StartCoroutine(MainMenuUIManager.Instance.OpenSetting());
        public void ToggleVSync() => MainMenuUIManager.Instance.ToggleVSync();
        public void ToggleFullScreen() => MainMenuUIManager.Instance.ToggleFullScreen();
        public void OpenCredit()
        {
            MainMenuUIManager.Instance.IsCreditOpen = true;
            MainMenuUIManager.Instance.ToggleInteractableOptionBtn(false);
            ControlManager.Instance.CharacterInputAction.MainMenu.Navigate.Disable();
        }
        public void SetGrandpopName() => NameInputManager.Instance.SetMomName();
        public void SetChildName() => NameInputManager.Instance.SetChildName();
        public void ConfirmNameData() => NameInputManager.Instance.ConfirmSetData();
        public void CancelNameData() => NameInputManager.Instance.CancelSetData();

    }
}

