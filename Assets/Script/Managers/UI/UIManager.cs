using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Fungus;
using DG.Tweening;
using OLJ.Manager;
using TMPro;
using Spine;
using OLJ.Gameplay;

namespace OLJ.UI {
   
    public class UIManager : Singleton<UIManager> {
        public enum SayDialogueType { field, bust }
        public enum BustDirection { right, left }

        [Header("----Say Dialogue parameters----")]
        [SerializeField] SayDialog fieldDialogue;
        [SerializeField] SayDialog bustDialogue;

        [Header("----Illustration Parameters----")]
        [SerializeField] Image illustrationBackgroundImg;
        [SerializeField] Image illustrationPage;
        [SerializeField] CutsceneBorder cutsceneBorder;

        [Header("----Bust Parameters----")]
        [SerializeField] CanvasGroup bustParent;
        [SerializeField] Image rightBust;
        [SerializeField] RectTransform brPos;
        [SerializeField] Image leftBust;
        [SerializeField] RectTransform blPos;
        [SerializeField] List<OLJCharacterSO> characterList;
        OLJCharacterSO currentCharacter;
        private Vector3 rightBustOriginalPosition;
        private Vector3 leftBustOriginalPosition;

        [Header("----Transition----")]
        [SerializeField] CanvasGroup transitionParent;

        [Header("----Notification Parameter----")]
        [SerializeField] CanvasGroup notifParent;
        [SerializeField] TextMeshProUGUI headerText;
        [SerializeField] TextMeshProUGUI notifText;

        [Header("----Diary Count----")]
        [SerializeField] private Image diaryImage;
        [SerializeField] private TMP_Text diaryCountText;
        [SerializeField] private float diaryIconHoldTime;
        [SerializeField] private float diaryIconFadeTime;

        [Header("----Camera Panning----")]
        [SerializeField] private Image cameraPanningImg;

        [Header("----Video Clip And Spine parameter----")]
        [SerializeField] RawImage videoImage;

        [Header("----Hp Vignette----")]
        [SerializeField] private Image hpVignette;

        public List<OLJCharacterSO> CharacterList => characterList;
        public class UIEvents : UnityEvent { }        
        public UIEvents onOpenDiary=new UIEvents();
        public UIEvents onCloseDiary=new UIEvents();
        private bool isOpenDiary;

        public bool IsOpenDiary
        { 
            get 
            {
                return isOpenDiary; 
            } 
            set
            { 
                isOpenDiary = value; 
            } 
        }

        public CameraPanning CamPan {get; set;}

        public Image IllustrationPage
        {
            get { return illustrationPage; }
            set { illustrationPage = value; }
        }

        private void Start() {
            if (ControlManager.Instance) {
                ControlManager.Instance.CharacterInputAction.Player.Diary.Enable();
                ControlManager.Instance.CharacterInputAction.Player.Diary.performed += OpenDiary;
               
            }

            if(CollectionManager.Instance)
            {
                CollectionManager.Instance.onDiaryAdded += DisplayDiaryCount;
            }

            if(HPManager.Instance)
            {
                HPManager.Instance.onHpChanged += UpdateHpVignette;
            }

            #region Cam Panning (Obsolette)
            //if(CamPan)
            //{
            //    CamPan.onPanningActive += () =>
            //    {
            //        cameraPanningImg.DOKill();
            //        cameraPanningImg.DOFade(1f, .3f);
            //    };
            //    CamPan.onPanningDeactive += () =>
            //    {
            //        cameraPanningImg.DOKill();
            //        cameraPanningImg.DOFade(0f, .3f);
            //    };
            //}
            #endregion

            rightBustOriginalPosition = rightBust.rectTransform.position;
            leftBustOriginalPosition = leftBust.rectTransform.position;
        }

        private void OnDisable()
        {
            if (CollectionManager.Instance)
            {
                CollectionManager.Instance.onDiaryAdded -= DisplayDiaryCount;
            }

            if (HPManager.Instance)
            {
                HPManager.Instance.onHpChanged -= UpdateHpVignette;
            }

            if (CamPan)
            {
                CamPan.onPanningActive -= () =>
                {
                    cameraPanningImg.DOKill();
                    cameraPanningImg.DOFade(1f, .3f);
                };
                CamPan.onPanningDeactive -= () =>
                {
                    cameraPanningImg.DOKill();
                    cameraPanningImg.DOFade(0f, .3f);
                };
            }
        }

        public void ToggleCamPanIcon(bool _isPanning)
        {
            if(_isPanning)
            {
                cameraPanningImg.DOKill();
                cameraPanningImg.DOFade(1f, .3f);
            }
            else
            {
                cameraPanningImg.DOKill();
                cameraPanningImg.DOFade(0f, .3f);
            }
        }

        private void OpenDiary(InputAction.CallbackContext context) {
            isOpenDiary = !isOpenDiary;
            if (isOpenDiary) {
                onOpenDiary?.Invoke();
            } else {
                onCloseDiary?.Invoke();
            }
        }
        private void DisplayDiaryCount()
        {
            diaryCountText.text = CollectionManager.Instance.DiaryData.Count.ToString();
            diaryImage.DOFade(1f, diaryIconFadeTime);
            diaryCountText.DOFade(1f, diaryIconFadeTime);
            StartCoroutine(WaitDiaryIcon());
        }

        private IEnumerator WaitDiaryIcon()
        {
            yield return new WaitForSeconds(diaryIconHoldTime);
            diaryImage.DOFade(0f, diaryIconFadeTime);
            diaryCountText.DOFade(0f, diaryIconFadeTime);
        }

        public void SetSayDialogue(SayDialogueType type) {
            if (type == SayDialogueType.bust) {
                SayDialog.ActiveSayDialog = bustDialogue;
            } else {
                SayDialog.ActiveSayDialog = fieldDialogue;
            }
        }
        public SayDialog GetCurrentDialogue() {
            return SayDialog.ActiveSayDialog;
        }
        public void ShowIllustration(Sprite img) {
            ControlManager.Instance.CurrentBody.DisableControl();
            ControlManager.Instance.CharacterInputAction.UI.Enable();

            illustrationBackgroundImg.gameObject.SetActive(true);

            CanvasGroup canvasB = illustrationBackgroundImg.GetComponent<CanvasGroup>();
            canvasB.DOFade(1, 1f);

            CanvasGroup canvasP = illustrationPage.GetComponent<CanvasGroup>();
            canvasP.DOFade(0, 0.5f).OnComplete(() => {
                illustrationPage.sprite = img;
                canvasP.DOFade(1, 1f);
            });
           
        }
      
        public void HideIllustration() {
            //ControlManager.Instance.CurrentBody.EnableControl();
            ControlManager.Instance.CharacterInputAction.UI.Disable();
            CanvasGroup canvasGroup = illustrationBackgroundImg.GetComponent<CanvasGroup>();
            canvasGroup.DOFade(0, 1f).OnComplete(() => illustrationBackgroundImg.gameObject.SetActive(false));
           
        }
        public void ShowBustImage(string charName, string charExpression,BustDirection direct) {            
            foreach (OLJCharacterSO so in characterList) {
                if (so.charName == charName) {
                    currentCharacter = so;
                }
            }
            if (currentCharacter) {
                foreach (Sprite charaSprite in currentCharacter.sprite) {
                    if (charaSprite.name == charExpression) {
                        if (direct == BustDirection.right) {
                            BustEnter(rightBust, charaSprite, brPos);
                            if (leftBust.sprite != null) {
                                leftBust.DOColor(Color.grey, 1f); ;
                            }
                        } else {
                            BustEnter(leftBust, charaSprite, blPos);
                            if (rightBust.sprite != null) {
                                rightBust.DOColor(Color.grey, 1f); ;


                            }


                        } 
                    }
                }
            } else {
                Debug.Log($"No Character Named {charName} found");
            }
           
        }
        void BustEnter(Image bust,Sprite charImage,RectTransform movePos) {
            bustParent.DOFade(1, 1f);
            bust.gameObject.SetActive(true);
            bust.rectTransform.DOMove(movePos.position, 1f);
            bust.sprite = charImage;
            bust.DOColor(Color.white, 0.5f);            
        }
        public void HideBustImage() {
            rightBust.rectTransform.DOMove(rightBustOriginalPosition, 1f);
            leftBust.rectTransform.DOMove(leftBustOriginalPosition, 1f);
            bustParent.DOFade(0, 1f);
            currentCharacter = null;
            
        }
        public void PlayVideo(UnityEngine.Video.VideoClip clip,float volume,float playbackTime,bool muted) {
            videoImage.GetComponent<CanvasGroup>().DOFade(1, 1);
            UnityEngine.Video.VideoPlayer videoClip = videoImage.GetComponent<UnityEngine.Video.VideoPlayer>();
            videoClip.clip = clip;
            videoClip.SetDirectAudioVolume(0, volume);
            videoClip.SetDirectAudioMute(0, muted);
            videoClip.playbackSpeed = playbackTime;
        }
        public void StopVideo() {
            videoImage.GetComponent<CanvasGroup>().DOFade(0, 1);
            UnityEngine.Video.VideoPlayer videoClip = videoImage.GetComponent<UnityEngine.Video.VideoPlayer>();
            videoClip.clip = null;
        }
        public void ShowNotification(string header,string notif) {
            ControlManager.Instance.CharacterInputAction.UI.Cancel.Enable();
            ControlManager.Instance.CharacterInputAction.UI.Cancel.performed += HideNotification;
            notifParent.DOFade(1, 1f);
            headerText.text = header;
            notifText.text = notif;
        }
        public void HideNotification(InputAction.CallbackContext context) {
            notifParent.DOFade(0, 1f);
            ControlManager.Instance.CharacterInputAction.UI.Cancel.Disable();
        }
        public void ShowFlash(float fadeInTime, float holdTime, float fadeOutTime) {
            transitionParent.DOFade(1, fadeInTime).OnComplete(() => {
                StartCoroutine(WaitFlash(holdTime, fadeOutTime));
            });
        }
        IEnumerator WaitFlash(float holdTime,float fadeOutTime) {
            yield return new WaitForSeconds(holdTime);
            transitionParent.DOFade(0, fadeOutTime);
        }
        public void ShowTransition() {
            GameManager.Instance.OpenTransitionSequence();
        }
        public void HideTransition() {
            GameManager.Instance.CloseTransitionSequence();
        }
        public void PlayTransition(System.Action funct) {
            GameManager.Instance.PlayTransition(funct);
        }
        public void ShowCutsceneBorder() {
            cutsceneBorder.PlayBorder();
        }
        public void HideCutsceneBorder() {
            cutsceneBorder.HideBorder();
        }
        private void UpdateHpVignette(float _currentHp, float _maxHp)
        {
            hpVignette.color = new Color(
                hpVignette.color.r, hpVignette.color.g, hpVignette.color.b, 1 - (_currentHp / _maxHp));
        }
        public void HideAll() {
            HideIllustration();
            HideBustImage();
            StopVideo();
        }
    }
  
}


