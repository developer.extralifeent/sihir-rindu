using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using OLJ.Manager;
using TMPro;

namespace OLJ.UI {
    public class DiaryManager : Singleton<DiaryManager> {
        [SerializeField] RectTransform diaryParentTransform;
        [SerializeField] Image diaryImage;
        [SerializeField] TextMeshProUGUI diaryDesc;
        public List<DiarySO> data=new List<DiarySO>();
        int dataIndex = 0;

        private void Start() {
            UIManager.Instance.onOpenDiary.AddListener(OpenDiary);
            UIManager.Instance.onCloseDiary.AddListener(HideDiary);
        }

        public void OpenDiary() {
            GetCollectionData();
            diaryParentTransform.GetComponent<CanvasGroup>().alpha = 1;
            Debug.Log("Open Diary");
        } 
        
        public void HideDiary() {
            //OLJ.Manager.ControlManager.Instance.DisableUI();
            ControlManager.Instance.SwitchInputAction(ControlManager.Instance.CharacterInputAction.Player);
            diaryParentTransform.GetComponent<CanvasGroup>().alpha = 0;
            Debug.Log("Close Diary");
        }

        public void OpenPage(int page) {
            RefreshPage(page);
        }

        void GetCollectionData() {
            //OLJ.Manager.ControlManager.Instance.EnableUI();
            ControlManager.Instance.SwitchInputAction(ControlManager.Instance.CharacterInputAction.UI);
            ControlManager.Instance.CharacterInputAction.UI.Navigation.performed += ChangePage;
            if (CollectionManager.Instance.DiaryData.Count!=0) {
                data = CollectionManager.Instance.DiaryData;
                RefreshPage(dataIndex);
            }
        }

        void ChangePage(InputAction.CallbackContext context) {
            if (data.Count > 0) {
                dataIndex += (int)context.ReadValue<Vector2>().x;
                if (dataIndex > data.Count - 1) {
                    dataIndex = 0;
                }
                if (dataIndex < 0) {
                    dataIndex = data.Count - 1;
                }
                RefreshPage(dataIndex);
            }          
        }

        void RefreshPage(int index) {
            diaryDesc.text = data[index].description;
            diaryImage.sprite = data[index].imageContain;
        }
       
    }
}


