using MyBox;
using OLJ.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : Singleton<CursorManager>
{
    [Separator("Cursor sprites")]
    [SerializeField] private Texture2D normalCursorTexture;
    [SerializeField] private Texture2D hoverCursorTexture;
    [SerializeField] private Texture2D clickCursorTexture;

    [Separator("Disabling Cursor Parameters")]
    [SerializeField] private float disableCursorTime = 2f;

    private bool isCursorIdling;
    private bool isCoroutineStarted;
    public bool isCursorHovering { get; set; }

    private void Update()
    {
        Vector2 mouseDir = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        isCursorIdling = mouseDir.magnitude == 0f;

        if (isCursorIdling)
        {
            StartCoroutine(DisableCursorCoroutine());
        }
        else
        {
            StopAllCoroutines();

            isCoroutineStarted = false;

            if(!Cursor.visible)
                Cursor.visible = true;
                

            if(isCursorHovering)
                ChangeHoverCursor();
            else
                ChangeNormalCursor();
        }
    }

    private IEnumerator DisableCursorCoroutine()
    {
        if (isCoroutineStarted) yield break;

        isCoroutineStarted = true;
        yield return new WaitForSeconds(disableCursorTime);

        if (isCursorIdling)
            Cursor.visible = false;
    }

    public void ChangeNormalCursor()
    {
        Cursor.SetCursor(normalCursorTexture, Vector2.zero, CursorMode.Auto);
    }

    public void ChangeHoverCursor()
    {
        Cursor.SetCursor(hoverCursorTexture, Vector2.zero, CursorMode.Auto);
    }

    public void ChangeClickCursor()
    {
        Cursor.SetCursor(clickCursorTexture, Vector2.zero, CursorMode.Auto);
    }
}
