using OLJ.PlayerSaveData;
using MyBox;
using System.IO;
using UnityEngine;

namespace OLJ.Manager
{
    public class SaveManager : OLJ.Singleton<SaveManager>
    {
        [Separator("Testing")]
        [SerializeField] private bool useSaveData;

        protected override void Awake()
        {
            base.Awake();

            CreateDirectory();
        }

        private void CreateDirectory()
        {
            // Check if the folder exists
            if (!Directory.Exists(Application.persistentDataPath + "/OLJSave"))
            {
                // Create the folder
                Directory.CreateDirectory(Application.persistentDataPath + "/OLJSave");
                Debug.Log($"Folder created at: {Application.persistentDataPath + "/OLJSave"}");
            }
            else
            {
                Debug.Log($"Folder already exists at: {Application.persistentDataPath + "/OLJSave"}");
            }
        }

        #region PlayerData
        [Separator("Player Data")]
        [SerializeField] private PlayerData playerData;
        public bool IsNameSet {  get; private set; }
        public PlayerData PlayerData => playerData;

        private const string playerDataFileName = "/PlayerData.json";
        
        public void SavePlayerName(string _playerName, string _grandpopName)
        {
            if(!useSaveData) return;

            PlayerData.childName = _playerName;
            playerData.grandpopName = _grandpopName;

            string jsonPlayerName = JsonUtility.ToJson(playerData);
            File.WriteAllText(Application.persistentDataPath + "/OLJSave" + playerDataFileName, jsonPlayerName);
        }

        public void LoadPlayerName()
        {
            IsNameSet = File.Exists(Application.persistentDataPath + "/OLJSave" + playerDataFileName);
            if (IsNameSet)
            {
                string jsonSettingData = File.ReadAllText(Application.persistentDataPath + "/OLJSave" + playerDataFileName);
                PlayerData jsonLoadedData = JsonUtility.FromJson<PlayerData>(jsonSettingData);

                playerData.childName = jsonLoadedData.childName;
                playerData.grandpopName = jsonLoadedData.grandpopName;
                Debug.Log("Player name loaded");
            }
            else
            {
                playerData.childName = playerData.grandpopName = null;
                Debug.Log("Player name is null");
            }
        }
        #endregion

        #region Player Settings
        [Separator("Player Setting")]
        [SerializeField] private PlayerSettingData settingData;
        public PlayerSettingData SettingData { get { return settingData; } }

        private const string playerSettingFileName = "/PlayerSettings.json";

        public void SavePlayerSetting(int _resolutionIndex, bool _vSyncActive, bool _fullScreenActive)
        {
            if (!useSaveData) return;

            settingData.resolutionIndex = _resolutionIndex;
            settingData.vSync = _vSyncActive;
            settingData.fullScreen = _fullScreenActive;

            string jsonSettingData = JsonUtility.ToJson(settingData);
            File.WriteAllText(Application.persistentDataPath + "/OLJSave" + playerSettingFileName, jsonSettingData);
        }

        public void LoadPlayerSettings()
        {
            if(File.Exists(Application.persistentDataPath + "/OLJSave" + playerSettingFileName))
            {
                string jsonSettingData = File.ReadAllText(Application.persistentDataPath + "/OLJSave" + playerSettingFileName);
                PlayerSettingData jsonLoadedData = JsonUtility.FromJson<PlayerSettingData>(jsonSettingData);

                settingData.resolutionIndex = jsonLoadedData.resolutionIndex;
                settingData.vSync = jsonLoadedData.vSync;
                settingData.fullScreen = jsonLoadedData.fullScreen;
            }
            else
            {
                settingData.resolutionIndex = 0;
                settingData.vSync = true;
                settingData.fullScreen = true;
            }
        }
        #endregion

        #region Chapter selection
        [Separator("Main Menu Index")]
        [SerializeField] private MainMenuIndexes mainMenuIndex;
        public MainMenuIndexes MainMenuIndex => mainMenuIndex;

        private const string menuIndexFileName = "/menuIndex.json";

        public void SaveMainMenuIndex(int _currentSelectedChp, int _currentSelectedSubchp, int _buttonIndex, FieldSO[] _selectedChapterSos)
        {
            if (!useSaveData) return;

            mainMenuIndex.currentSelectedChpt = _currentSelectedChp;
            mainMenuIndex.currentSelectedSubchpt = _currentSelectedSubchp;
            mainMenuIndex.buttonIndex = _buttonIndex;
            mainMenuIndex.selectedChapterSO = _selectedChapterSos;

            string jsonSettingData = JsonUtility.ToJson(mainMenuIndex);
            File.WriteAllText(Application.persistentDataPath + "/OLJSave" + menuIndexFileName, jsonSettingData);
        }

        public void LoadMainMenuIndexes()
        {
            if (File.Exists(Application.persistentDataPath + "/OLJSave" + menuIndexFileName))
            {
                string jsonSettingData = File.ReadAllText(Application.persistentDataPath + "/OLJSave" + menuIndexFileName);
                MainMenuIndexes jsonLoadedData = JsonUtility.FromJson<MainMenuIndexes>(jsonSettingData);

                mainMenuIndex.currentSelectedChpt = jsonLoadedData.currentSelectedChpt;
                mainMenuIndex.currentSelectedSubchpt = jsonLoadedData.currentSelectedSubchpt;
                mainMenuIndex.buttonIndex = jsonLoadedData.buttonIndex;
                mainMenuIndex.selectedChapterSO = jsonLoadedData.selectedChapterSO;

            }
            else
            {
                mainMenuIndex.currentSelectedChpt = 0;
                mainMenuIndex.currentSelectedSubchpt = 0;
                mainMenuIndex.buttonIndex = 0;
            }
        }
        #endregion
    }
}