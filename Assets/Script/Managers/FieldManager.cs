using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Field;
using OLJ.Character;
using UnityEngine.Events;

namespace OLJ.Manager {
    public class FieldManager : Singleton<FieldManager>
    {
        [SerializeField] Transform levelArena;
        [SerializeField] GameObject activeLevelArena;
        private PlayerController currentPlayer;

        public class FieldEvent : UnityEvent { }
        public FieldEvent onFieldLoad = new FieldEvent();
        public FieldEvent onFieldUnload = new FieldEvent();
        private void Start()
        {
            if (OLJ.Manager.GameManager.Instance) {
                FieldSetup(OLJ.Manager.GameManager.Instance.FieldSO);
            }

        }
        private void FieldSetup(FieldSO field) {
            onFieldLoad?.Invoke();
            if (activeLevelArena == null) {
                if (field != null) {
                    activeLevelArena = Instantiate(field.fieldPrefab);
                    SetActiveArena();
                } else {
                    Debug.LogError("No FieldSO found!");
                }

            } else {
                activeLevelArena = Instantiate(activeLevelArena);
                SetActiveArena();
            }
        }
        private void SetActiveArena() {
            if (activeLevelArena != null) {
                Debug.Log($"Load Arena: {activeLevelArena.name}" );
                if (activeLevelArena.GetComponent<StageControl>() != null) {                    
                    currentPlayer = activeLevelArena.GetComponent<StageControl>().Player;
                    ControlManager.Instance.CurrentBody = currentPlayer;                    
                    activeLevelArena.transform.SetParent(levelArena);
                } else {
                    Debug.Log("No Stage Controller Detected");
                }
            } else {
                Debug.LogError("No Arena found!");
            }
        }
        public void TeleportPlayer(Transform target) {
            ControlManager.Instance.CurrentBody.transform.position = target.position;
        }
        public void LoadFieldPrefab(FieldSO field) {
            onFieldUnload?.Invoke();
            ResetField(field);           
        }
       
        private void ResetField(FieldSO field) {
            ControlManager.Instance.CurrentBody = null;
            UnloadField(field);
        }
        void UnloadField(FieldSO field) {
            Destroy(activeLevelArena.gameObject);
            activeLevelArena = null;
            GameManager.Instance.LoadField(field);
            FieldSetup(GameManager.Instance.FieldSO);
            Debug.Log("Added Field: " + field.name);
        }
       
    }

}

