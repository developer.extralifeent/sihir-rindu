using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using OLJ.Character;
using MyBox;
using System.Data.Common;

namespace OLJ.Manager
{
    public class GameManager : Singleton<GameManager>
    {
        [Header("Scene Manager")]
        [SerializeField] string titleMenu;
        [SerializeField] string generalGameplay;
        [SerializeField] string testingScene;
        [SerializeField] string loadingScene;

        [Header("Loading Parameter")]
        [SerializeField] Animator transitionAnim;
        [SerializeField] float minLoadError = 2f;
        [ReadOnly] public List<FieldSO> allFieldSO = new List<FieldSO>();

        [Header("Dev Parameter")]
        [SerializeField] bool devMode;

        public bool DevMode => devMode;
        private float loadingProggress;
        private FieldSO currentFieldSO;
        private List<Scene> loadedScenes;
        public string TitleMenu => titleMenu;
        public string GeneralGameplay => generalGameplay;
        public string TestingScene => testingScene;
        public float Proggress => loadingProggress;
        public float MinimalLoadError => minLoadError;
        public FieldSO FieldSO => currentFieldSO;
        
        [field : SerializeField] public bool IsPartialChapter { get; set; } = false;

        //Events
        public delegate void OnLoadingStart();
        public delegate void OnLoadingProgress();
        public delegate void OnloadingEnd();
        public delegate void OnGameOver();
        public OnLoadingStart onLoadingStart;
        public OnLoadingProgress onLoadingProgress;
        public OnloadingEnd onLoadingEnd;
        public OnGameOver onGameOver;
        protected override void Awake()
        {
            base.Awake();
            loadedScenes = new List<Scene>();
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                loadedScenes.Add(SceneManager.GetSceneAt(i));
            }
        }
        private void Start()
        {
            if (loadedScenes.Count <= 1)
            {
                LoadScene(titleMenu);
            }
            else
            {
                SceneManager.SetActiveScene(loadedScenes[1]);
            }

            
        }
        [ButtonMethod]
        private string RefreshFieldSO() {
            allFieldSO.Clear();
            FieldSO[] allDataBase = Resources.LoadAll<FieldSO>("");
            // Iterate through each asset path
            foreach (FieldSO asset in allDataBase) {
                if (!allFieldSO.Contains(asset)) {

                    allFieldSO.Add(asset);
                }
                // Do whatever you want with the ScriptableObject
                Debug.Log("Found ScriptableObject at path: " + asset.name);
            }
            return "Refreshed Field";
        }
        [ButtonMethod]
        private string DeleteAllFieldSO() {
            allFieldSO.Clear();
            return "Reset Field";
        }

        private void SaveData(int fieldID) {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/playerData.dat";
            FileStream stream = new FileStream(path, FileMode.Create);
            formatter.Serialize(stream, fieldID);
            stream.Close();
            Debug.Log($"Data Saved: {fieldID}");
        }
       private int LoadData() {
            string path = Application.persistentDataPath + "/playerData.dat";
            if (File.Exists(path)) {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);
                int data = (int)formatter.Deserialize(stream);
                Debug.Log($"Load Saved Data: {data}");
                stream.Close();
                return data;
            } else {
                Debug.LogError("Save file not found");
                return -1;
            }
            
        }
        public void LoadScene(string title)
        {
            if (!SceneManager.GetSceneByName(title).isLoaded && !loadedScenes.Contains(SceneManager.GetSceneByName(title))) 
            {
                StartCoroutine(StartTransition(title));
            }
           
        }
        public void SavingField(int id) {
            SaveData(id);
        }
        public void LoadSavedField() {
            foreach (FieldSO fso in allFieldSO) {
                if (fso.id == LoadData()) {
                    currentFieldSO = fso;
                    Debug.Log($"Load Arena: id {currentFieldSO.fieldPrefab.name}");
                }
            }
            if (currentFieldSO != null) {

                LoadField(currentFieldSO);
            } else {
                Debug.Log("No Saved Data Loaded");
            }
        }
        public void LoadGamePlay()
        {
            UnloadScene(titleMenu);
            LoadScene(generalGameplay);
        }

        public void LoadMainMenu()
        {
            PlayTransition(() =>
            {
                UnloadScene(generalGameplay);
                SceneManager.LoadSceneAsync(titleMenu, LoadSceneMode.Additive);
            });
        }

        public void LoadTestingScene()
        {
            UnloadScene(titleMenu);
            LoadScene(testingScene);
        }
        public void UnloadScene(string title)
        {
            if (SceneManager.GetSceneByName(title).isLoaded) {

                SceneManager.UnloadSceneAsync(title);
            }
        }
        public void LoadField(FieldSO fSO)
        {
            currentFieldSO = fSO;
            //SaveData(currentFieldSO.id);
            UnloadScene(titleMenu);
            LoadScene(generalGameplay);
        }
        public void PlayTransition(System.Action funct) {
            StartCoroutine(PlayTransitionEnum(funct));
        }
       
        //For single call Only
        public void OpenTransitionSequence() {
            Debug.Log("Do Transition: ");
            transitionAnim.Play("Transition Open");
        }
        //For single call only
        public void CloseTransitionSequence() {
            transitionAnim.Play("Transition Close");
        }
        public void RestartLevel()
        {
            Time.timeScale = 1;
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }
        //Play transition and invoke function
        IEnumerator PlayTransitionEnum(System.Action funct) {
            CloseTransitionSequence();
            yield return new WaitForSeconds(transitionAnim.GetCurrentAnimatorStateInfo(0).length);
            funct.Invoke();
            OpenTransitionSequence();
        }
        //Play transition and load scene
        IEnumerator StartTransition(string scene) {

            Debug.Log("Transition Close");
            CloseTransitionSequence();
            yield return new WaitForSeconds(transitionAnim.GetCurrentAnimatorStateInfo(0).length);
            StartCoroutine(StartLoadingAsync(scene));
        }
        IEnumerator StartLoadingAsync(string title)
        {
            onLoadingStart?.Invoke();

            Debug.Log("loading Start");
            SceneManager.LoadSceneAsync(loadingScene, LoadSceneMode.Additive);
            AsyncOperation loadOperation=SceneManager.LoadSceneAsync(title, LoadSceneMode.Additive);
            while (!loadOperation.isDone)
            {
                loadingProggress = loadOperation.progress;
                onLoadingProgress?.Invoke();
                Debug.Log("loading progress: " + loadingProggress);
                yield return null;
            }
            if (loadOperation.isDone)
            {
                onLoadingEnd?.Invoke();
                Debug.Log("loading End");
                StartCoroutine(WaitErrorDuration());
                UnloadScene(loadingScene);
            }
        }
        IEnumerator WaitErrorDuration() {
            yield return new WaitForSeconds(minLoadError);
            OpenTransitionSequence();
        }
    }
}
