using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using DG.Tweening;
using System;
using System.ComponentModel;
using MyBox;
using Unity.Mathematics;
using OLJ.UI;
using OLJ.Character;
using static UnityEngine.Rendering.DebugUI;

namespace OLJ.Manager {
    public class QTEManager : Singleton<QTEManager> {
        [SerializeField] private QTE_Type qteType;

        #region MashButton Variables
        [Header("Mash Button Variables")]
        [SerializeField] int countThreshold = 10;
        [SerializeField] float duration = 2f;
        [SerializeField] float resetTime = 1f;

        private bool isCounting = false;
        private float countdownTimer = 0f;
        private float count;
        Tween mashCountTween;
        //Mash Button Public Variables
        public int CountThreshold {
            get { return countThreshold; }
            set { countThreshold = value; }
        }
        public float Duration {
            get { return duration; }
            set { duration = value; }
        }
        public float ResetTime {
            get { return resetTime; }
            set { resetTime = value; }
        }
        public float Count => count;
        #endregion

        #region ButtonPressing Variables
        [Header("Button Pressing Variables")]
        [SerializeField] private float pressingDurationThreshold;
        [SerializeField] private float pressingValueMultiplier;

        private bool isPressing;
        private float pressingAmount;
        private Tween pressingCountTween;

        //Button pressing public variables
        public float PressingDurationThreshold
        {
            get { return pressingDurationThreshold; }
            set { pressingDurationThreshold = value; }
        }

        public float PressingValueMultiplier
        {
            get { return pressingValueMultiplier; }
            set { pressingValueMultiplier = value; }
        }

        public float PressingAmount
        {
            get { return pressingAmount; }
            set { pressingAmount = value; }
        }
        #endregion

        #region Button Sequence Variables
        [Header("Button Pressing Variables")]
        [MyBox.ReadOnly] public List<int> RandomQTEHolder = new List<int>();

        private List<InputAction> buttonToPress = new List<InputAction>();
        public List<InputAction> ButtonToPress
        {
            get { return buttonToPress; }
            set { buttonToPress = value; }
        }

        private int buttonIndex = 0;
        public Action<int> onButtonCorrect;
        #endregion

        #region Precision Pressing Variables
        [Header("Precision Pressing Variables")]
        [SerializeField] private bool isPrecisionAreaRandomized;
        [SerializeField, ConditionalField(nameof(isPrecisionAreaRandomized), false)] private float minXScalePrecisionArea;
        [SerializeField, ConditionalField(nameof(isPrecisionAreaRandomized), false)] private float maxXScalePrecisionArea;
        [SerializeField, ConditionalField(nameof(isPrecisionAreaRandomized), true)] private float fixedXScalePrecisionArea;
        [SerializeField] private float slidingTime;
        private float randomizedPrecisionAreaPos;
        private float customTime;
        //[SerializeField, Range(0f, 1f)] private float thresholdPress;
        //[SerializeField] private float maxThresholdToAdd;
        private float currentSliderValue;
        public float CurrentSliderValue { get { return currentSliderValue; } }

        public bool IsPrecisionAreaRandomized
        {
            get { return isPrecisionAreaRandomized; }
            set { isPrecisionAreaRandomized = value; }
        }
        public float MinXScalePrecisionArea
        {
            get { return minXScalePrecisionArea; }
            set { minXScalePrecisionArea = value; }
        }
        public float MaxXScalePrecisionArea
        {
            get { return maxXScalePrecisionArea; }
            set { maxXScalePrecisionArea = value; }
        }
        public float FixedXScalePrecisionArea
        {
            get { return fixedXScalePrecisionArea; }
            set { fixedXScalePrecisionArea = value; }
        }
        public float SlidingTime
        {
            get { return slidingTime; }
            set { slidingTime = value; }
        }

        public Vector3 PrecisionAreaXScale { get; private set; }

        #endregion

        private Animator anim;

        int randomQTE;
        List<InputAction> qteInputAction;
        #region
        private Action<InputAction.CallbackContext> mashButtonHandler;
        private Action<InputAction.CallbackContext> buttonPressingHandler;
        private Action<InputAction.CallbackContext> sequenceButtonHandler;
        private Action<InputAction.CallbackContext> precisionPressingHandler;

        #endregion
        public InputAction CurrentInput => currentInput;
        public int RandomQTE => randomQTE;

        InputAction currentInput;

        public class QTEEvents : UnityEvent<int> { }
        public QTEEvents onQTEexecute = new QTEEvents();
        public QTEEvents onQTEfinished = new QTEEvents();

        //public UnityEvent<int> onQTEexecute;
        //public UnityEvent<int> onQTEfinished;

        List<Action<InputAction.CallbackContext>> qteList = new List<Action<InputAction.CallbackContext>>();

        public bool IsQteStarted { get; set; }

       
        private void Start() {
            qteInputAction = ControlManager.Instance.QTEInput;
            mashButtonHandler = MashButton;
            buttonPressingHandler = ButtonPressing;
            sequenceButtonHandler = SequenceButton;
            precisionPressingHandler = PrecisionPressing;
            qteList.Add(mashButtonHandler);
            qteList.Add(buttonPressingHandler);
            qteList.Add(sequenceButtonHandler);
            qteList.Add(precisionPressingHandler);

            StartAllQTECoroutine();
        }

        private void Update()
        {
            HandleButtonPressingValue();
            PingPongSliderValueUpdate();
        }

        public void StartQTE(int qteIndex) {
            ControlManager.Instance.EnabledQTE();
            CallQTEFunction(qteIndex);
            onQTEexecute?.Invoke(qteIndex);
            IsQteStarted = true;
        }
        public void EndQTE(int qteIndex) {
            ControlManager.Instance.DisableQTE();
            DeCallQTEFunction(qteIndex);
            onQTEfinished?.Invoke(qteIndex);

            Debug.Log($"QTEUI called: EndQTE {qteIndex}");
            qteType = QTE_Type.None;

            IsQteStarted = false;
        }
        void CallQTEFunction(int index)
        {
            if (index == 0 || index == 1) {

                if(index == 0) qteType = QTE_Type.ButtonMashing;
                else qteType = QTE_Type.ButtonHolding;

                randomQTE = UnityEngine.Random.Range(0, qteInputAction.Count);

                Action<InputAction.CallbackContext> handler = (index == 0) ? mashButtonHandler : buttonPressingHandler;

                 for (int i = 0; i < qteInputAction.Count; i++) {
           
                     qteInputAction[i].performed -= handler;
                     qteInputAction[i].canceled -= handler;

           
                     qteInputAction[i].performed += handler;
                     qteInputAction[i].canceled += handler;

                    if (i == randomQTE) {

                        Debug.Log($"Call QTE Function: input name {qteInputAction[i].name}");
                        qteInputAction[i].Enable();
                    } else {
                        qteInputAction[i].Disable();
                    }
                }
            }
            else if(index == 2)
            {
                // Sequence button pressing
                qteType = QTE_Type.ButtonSequence;

                for (int i = 0; i < qteInputAction.Count; i++)
                {
                    randomQTE = UnityEngine.Random.Range(0, qteInputAction.Count);
                    RandomQTEHolder.Add(randomQTE);

                    buttonToPress.Add(qteInputAction[randomQTE]);
                    buttonToPress[i].performed += context => qteList[index](context);
                    buttonToPress[i].Disable();
                }
                buttonToPress[buttonIndex].Enable();
            }
            else if (index == 3)
            {
                qteType = QTE_Type.ButtonPrecision;

                //Precision pressing
                float defaultScale = 45f;
                if (isPrecisionAreaRandomized)
                {
                    if(minXScalePrecisionArea == 0 || MaxXScalePrecisionArea == 0)
                    {
                        Debug.LogError("Precision area is randomized, but min or max X scale of the area is still set to 0");
                    }

                    float randomXScale = UnityEngine.Random.Range(minXScalePrecisionArea, maxXScalePrecisionArea);
                    PrecisionAreaXScale = new Vector3(randomXScale, defaultScale, 0f);
                }
                else
                {
                    PrecisionAreaXScale = new Vector3(fixedXScalePrecisionArea, defaultScale, 0f);
                }

                float pressingSliderXHalfSize = QTEUIManager.Instance.PressingSliderParent.sizeDelta.x / 2f;
                float precissionAreaXHalfSize = QTEUIManager.Instance.PrecisionArea.sizeDelta.x / 2f;

                randomizedPrecisionAreaPos = UnityEngine.Random.Range(-pressingSliderXHalfSize + precissionAreaXHalfSize,
                                                                            pressingSliderXHalfSize - precissionAreaXHalfSize);

                QTEUIManager.Instance.PrecisionArea.transform.localPosition = new Vector3(randomizedPrecisionAreaPos, 0f, 0f);

                foreach (var button in qteInputAction)
                {
                    button.Disable();
                }

                randomQTE = UnityEngine.Random.Range(0, qteInputAction.Count);
                qteInputAction[randomQTE].performed += context => qteList[index](context);
                qteInputAction[randomQTE].Enable();
            }
            else
            {
                Debug.LogWarning("No function assigned for this index");
            }
        }
        void DeCallQTEFunction(int index) {
            Debug.Log($"DeCall Function: {index}");

            if (qteInputAction.Count > 0) {
                Action<InputAction.CallbackContext> handler;
                switch (index) {
                    case 0: handler = mashButtonHandler; break;
                    case 1: handler = buttonPressingHandler; break;
                    case 2: handler = sequenceButtonHandler; break;
                    case 3: handler = precisionPressingHandler; break;
                    default: Debug.LogWarning("No handler found for this index"); return;
                }

                // Unsubscribe all input actions
                foreach (InputAction ip in qteInputAction) {
                    ip.performed -= handler;
                    ip.canceled -= handler;
                    ip.Disable(); // Disable the input actions immediately
                }

                // Call the Reset functions to stop ongoing tweens, coroutines, etc.
                ResetAllQTEValue();

                // Trigger the finished event after everything is cleaned up
                StartCoroutine(DelayedQTEFinished(index));
            } else {
                Debug.LogWarning("No function assigned for this index");
            }

            IsQteStarted = false; // Ensure the QTE status is reset
        }

        private void StartAllQTECoroutine()
        {
            StartCoroutine(MashButtonEnum());
            //StartCoroutine(PingPongSliderValue());
            //StartCoroutine(HandleButtonPressingValue());
            //StartButtonPressingCoroutine, etc
        }

        private void ResetAllQTEValue()
        {
            // Stop active tweens
            mashCountTween?.Kill();
            pressingCountTween?.Kill();

            // Stop coroutines if necessary
            StopAllCoroutines(); // Stops all coroutines in this script

            // Clear out any sequence data
            ResetMashValue();
            ResetPressingValue();
            ClearSequence();

            Debug.Log("All QTE values reset");
        }

        private IEnumerator DelayedQTEFinished(int index) {
            yield return new WaitForSeconds(0.1f);
            onQTEfinished?.Invoke(index);
        }

        public void StartAnimQTE(string _animStateName)
        {
            int _animStateHash = Animator.StringToHash(_animStateName);

            if (anim == null)
                anim = FindObjectOfType<PlayerController>().GetComponentInChildren<Animator>();

            if (_animStateHash != 0)
            {
                anim.Play(_animStateHash);
            }
        }

        public void EndAnimQTE()
        {
            int _animStateHash = Animator.StringToHash("idle");
            anim.Play(_animStateHash);
        }

        #region MashButton
        void MashButton(InputAction.CallbackContext context) {
            if (qteType != QTE_Type.ButtonMashing) return;

            if (context.performed) {
                IncreaseMashValue();
            }
        }
        private void IncreaseMashValue() {
            count++;
            Debug.Log($"Increase Mash Value: {count}");
            if (count >= countThreshold) {
                EndQTE(0);
                count = 0;
                ResetMashValue();
            } else {
                mashCountTween.Kill();
                isCounting = true;
                countdownTimer = duration;
            }
        }

        private void ResetMashValue() {
            if (qteType != QTE_Type.ButtonMashing) return;

            mashCountTween = DOTween.To(() => count, x => count = x, 0, 2);
            isCounting = false;
            countdownTimer = resetTime;
        }

        IEnumerator MashButtonEnum() {
            while (true) {
                if (isCounting) {
                    countdownTimer -= Time.deltaTime;
                    if (countdownTimer <= 0f) {
                        ResetMashValue();
                    }
                }
                yield return null;
            }
        }
       
        #endregion

        #region ButtonPressing
        void ButtonPressing(InputAction.CallbackContext context) {

            if (qteType != QTE_Type.ButtonHolding) return;

            if (context.performed) isPressing = true;
            if (context.canceled) isPressing = false;
            //else isPressing = !isPressing;
            Debug.Log(context.phase);
        }

        private void HandleButtonPressingValue()
        {
            if (isPressing)
                IncreasePressingValue();
            else
                ResetPressingValue();
        }

        private void IncreasePressingValue()
        {
            pressingAmount += Time.deltaTime * pressingValueMultiplier;
            if (pressingAmount >= pressingDurationThreshold)
            {
                isPressing = false;
                EndQTE(1);
                PressingAmount = 0;
                ResetPressingValue();
            }
            else
            {
                pressingCountTween.Kill();
            }
        }

        private void ResetPressingValue()
        {
            if (qteType != QTE_Type.ButtonHolding) return;

            isPressing = false;
            pressingCountTween = DOTween.To(() => pressingAmount, x => pressingAmount = x, 0, Time.deltaTime * .005f);

            //if (pressingAmount <= 0) return;
            //pressingAmount -= Time.deltaTime;
        }
        #endregion

        #region Sequence Button
        private void SequenceButton(InputAction.CallbackContext context) {

            if (qteType != QTE_Type.ButtonSequence) return;

            if (buttonToPress[buttonIndex].phase == InputActionPhase.Performed)
            {
                Debug.Log("Button pressed");
                if (buttonIndex >= buttonToPress.Count - 1)
                    EndQTE(2);
                else
                    NextButton();
            }
        }

        private void NextButton()
        {
            buttonToPress[buttonIndex].Disable();

            //Disable Pressed Button Visual
            onButtonCorrect?.Invoke(buttonIndex);

            buttonIndex++;
            buttonToPress[buttonIndex].Enable();
        }

        private void ClearSequence()
        {
            //Fire an event for visual purpose

            for (int i = 0; i < buttonToPress.Count; i++)
            {
                Debug.Log("Unsubscribing" + buttonToPress[i] + ".performed from context");
                buttonToPress[i].performed -= context => qteList[2](context);
                //qteInputAction[i].performed -= context => qteList[2](context);
            }

            buttonIndex = 0;
            buttonToPress.Clear();
            RandomQTEHolder.Clear();
        }
        #endregion

        #region Precision Pressing
        void PrecisionPressing(InputAction.CallbackContext context) {
            if (context.performed)
            {
                CheckPrecisionArea();
                Debug.Log("Precision Area : Checking Precision Area");
            }
        }

        public void CheckPrecisionArea()
        {
            if(IsOnPrecisionArea())
            {
                Debug.Log("Precision Area Detected");
                EndQTE(3);
            }
            else
            {
                //Fire event for UI feedback
                Debug.Log("You missed the precision area");
            }
        }

        private IEnumerator PingPongSliderValue()
        {
            float maxValue = QTEUIManager.Instance.PressingSliderParent.sizeDelta.x / 2f;
            float minValue = -(QTEUIManager.Instance.PressingSliderParent.sizeDelta.x) / 2f;

            float customTime = 0f;
            while (true)
            {
                customTime += Time.unscaledDeltaTime * slidingTime;
                currentSliderValue = Mathf.PingPong(customTime, maxValue - minValue) + minValue;
                yield return null;
            }
        }

        private void PingPongSliderValueUpdate()
        {
            if (QTEUIManager.Instance)
            {
                float maxValue = QTEUIManager.Instance.PressingSliderParent.sizeDelta.x / 2f;
                float minValue = -(QTEUIManager.Instance.PressingSliderParent.sizeDelta.x) / 2f;

                //customTime += Time.unscaledDeltaTime * slidingTime;
                currentSliderValue = Mathf.PingPong(Time.unscaledTime * slidingTime, maxValue - minValue) + minValue;
            }
        }

        private bool IsOnPrecisionArea()
        {
            float sliderHandlePos = QTEUIManager.Instance.SliderHandle.localPosition.x;

            float precisionAreaHalfSize = QTEUIManager.Instance.PrecisionArea.sizeDelta.x / 2f;

            return sliderHandlePos >= randomizedPrecisionAreaPos - precisionAreaHalfSize &&
                    sliderHandlePos <= randomizedPrecisionAreaPos + precisionAreaHalfSize;
        }
        #endregion
    }

    public enum QTE_Type
    {
        None,
        ButtonMashing,
        ButtonHolding,
        ButtonSequence,
        ButtonPrecision
    }
}