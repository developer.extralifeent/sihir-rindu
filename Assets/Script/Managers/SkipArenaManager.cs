using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.UI;
using TMPro;
using OLJ.Manager;

namespace OLJ.DevCheat {
    public class SkipArenaManager : MonoBehaviour {
      
        private int arenaId;

        public void ReadInputField() {
            arenaId = int.Parse(DevCheatUIManager.Instance.IdInputField.text);
            Debug.Log("Read input Field: " + arenaId);
        }
        public void SubmitId() {
           foreach(FieldSO fso in GameManager.Instance.allFieldSO) {
                if (FieldManager.Instance && UIManager.Instance && fso.id == arenaId) {
                    UIManager.Instance.PlayTransition(() => {
                        FieldManager.Instance.LoadFieldPrefab(fso);
                    });
                }
            }
           
        }
    }
}

