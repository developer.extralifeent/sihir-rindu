using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

namespace OLJ.Manager {
    public class CutsceneManager : Singleton<CutsceneManager> {
        [SerializeField] Transform subtitleHolder;
        [SerializeField] float skipButtonDuration = 3;
        Flowchart activeFlowchart;
        bool isSkip;
        float skipTime;
        public float SkipTime => skipTime;
        public enum Language {
            English,
            Indonesia
        }
        public Language language;
        private SubtitleSO currentSubtitle;
        private void Start() {
            ControlManager.Instance.CharacterInputAction.UI.SkipCutscene.Enable();
            ControlManager.Instance.CharacterInputAction.UI.SkipCutscene.performed += SkipCutscenePressed;
            ControlManager.Instance.CharacterInputAction.UI.SkipCutscene.canceled += SkipCutsceneRelease;
        }
        private void Update() {
            if (isSkip) {
                skipTime += Time.deltaTime;
            } else {
                skipTime = 0;
            }
            if (skipTime >= skipButtonDuration) {
                SkipCutscene();
            }
        }
        public void LoadSubtitle(SubtitleSO sso) {
            currentSubtitle = sso;
            foreach (SubtitleController sc in currentSubtitle.subController) {
                if (sc.language == language) {
                    Transform subtitle = Instantiate(sc.transform);
                    subtitle.SetParent(subtitleHolder);
                }
            }
        }
        public void SetActiveFlowchart(Flowchart flowchart) {
            StartCoroutine(PlayFlowchartRoutine(flowchart));

        }
        private void SkipCutscene() {
            if (activeFlowchart) {              
                Debug.Log("Cutscene: Stop Cutscene");
                activeFlowchart.StopAllBlocks();
                activeFlowchart.gameObject.SetActive(false);
            }
            ControlManager.Instance.CharacterInputAction.UI.SkipCutscene.Disable();
            OLJ.UI.UIManager.Instance.HideAll();
            ControlManager.Instance.CharacterInputAction.Player.Enable();
            isSkip = false;
        }
        private void SkipCutscenePressed(UnityEngine.InputSystem.InputAction.CallbackContext context) {
            isSkip = true;
        }
        private void SkipCutsceneRelease(UnityEngine.InputSystem.InputAction.CallbackContext context) {
            isSkip = false;
        }
        IEnumerator PlayFlowchartRoutine(Flowchart flowchart) {
            yield return new WaitForEndOfFrame();

            if (activeFlowchart) {
                activeFlowchart.StopAllBlocks();
                activeFlowchart.gameObject.SetActive(false);
            }

            activeFlowchart = flowchart;
            flowchart.gameObject.SetActive(true);

        }

        public void StopActiveFlowchart() => activeFlowchart.StopAllBlocks();
    }
}
