using OLJ;
using OLJ.PlayerSaveData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Manager
{
    public class NameManager : Singleton<NameManager>
    {
        public string MomName { get; set; }
        public string ChildName { get; set; }

        [SerializeField] private RandomNameContainer randomKidNameContainer;
        [SerializeField] private RandomNameContainer randomMomNameContainer;

        private void Start()
        {
            SaveManager.Instance.LoadPlayerName();
            if (SaveManager.Instance.IsNameSet)
            {
                MomName = SaveManager.Instance.PlayerData.grandpopName;
                ChildName = SaveManager.Instance.PlayerData.childName;
            }
        }

        public RandomNameContainer GetRandomKidName () => randomKidNameContainer;
        public RandomNameContainer GetRandomMomName () => randomMomNameContainer;
    }

    [System.Serializable]
    public class RandomNameContainer
    {
        [field : SerializeField] public string[] Name { get; set; }

        public string GetRandomName() => Name[Random.Range(0, Name.Length - 1)];
    }
}

