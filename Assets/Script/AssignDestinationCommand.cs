using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.Character;
using UnityEngine.AI;

[CommandInfo("AI",
                "Set AI Destination",
                "Set Destination of navmesh agent")]
[AddComponentMenu("")]
public class AssignDestinationCommand : Command
{
    [SerializeField] NavMeshAgent agent;
    [SerializeField] Transform Target;
    [SerializeField] bool WaitFinished;

    public override void OnEnter()
    {
        agent.SetDestination(Target.position);
        agent.transform.Find("PlayerDetection").gameObject.SetActive(false);
        if (WaitFinished)
        {
            if (agent.stoppingDistance<=0)
            {
                Continue();
                agent.transform.Find("PlayerDetection").gameObject.SetActive(true);
            }
        }
        else
        {
            Continue();
            if (agent.stoppingDistance <= 0)
            {               
                agent.transform.Find("PlayerDetection").gameObject.SetActive(true);
            }
        }
        
    }
    public override string GetSummary()
    {
        if (Target != null && agent != null)
        {
            string s = "Move: " + agent.name + " To: " + Target.name;
            return s;
        }
        else
        {
            string s = "No Variable Yet";
            return s;
        }
        
    }
}
