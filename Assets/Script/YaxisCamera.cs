using UnityEngine;
using Cinemachine;
using OLJ.Character;

/// <summary>
/// An add-on module for Cinemachine Virtual Camera that locks the camera's Z co-ordinate
/// </summary>
[ExecuteInEditMode]
[SaveDuringPlay]
[AddComponentMenu("")] // Hide in menu
public class YaxisCamera : CinemachineExtension {
    [Tooltip("Lock the camera's Z position to this value")]
    public float m_YPosition = 10;
    private PlayerController m_PlayerController;

    protected override void PostPipelineStageCallback(
        CinemachineVirtualCameraBase vcam,
        CinemachineCore.Stage stage, ref CameraState state, float deltaTime) {

        m_PlayerController = GetComponentInParent<PlayerController>();

        if (stage == CinemachineCore.Stage.Body) {
            var pos = state.RawPosition;
            pos.y = m_PlayerController.transform.position.y + m_YPosition;
            state.RawPosition = pos;
        }
    }
}