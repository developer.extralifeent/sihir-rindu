using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowchartManager : MonoBehaviour
{
    [SerializeField] GameObject firstFlowchart;
    [SerializeField] GameObject secondFlowchart;
    [SerializeField] GameObject Wall;
    private void Start() {
        if(PlayerPrefs.GetInt("Position Index") != 0) {
            firstFlowchart.SetActive(false);
        }
        if(PlayerPrefs.GetInt("Position Index") < 6)
        {
            secondFlowchart.SetActive(true);
        }
        else
        {
            secondFlowchart.SetActive(false);
        }
        if (PlayerPrefs.GetInt("PuzleReq") >= 1)
        {
            Wall.SetActive(false);
        }
    }
}
