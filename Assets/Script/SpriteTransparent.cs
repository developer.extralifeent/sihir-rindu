using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class SpriteTransparent : MonoBehaviour
{
    [SerializeField] LayerMask LayertoTrans;
    GameObject ObjectTotranparent;
    Vector3 targetDirection;

    private void Start() {
    }
    private void FixedUpdate() {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, targetDirection);
        if (Physics.Raycast(ray, out hit, Vector3.Distance(transform.position,targetDirection), LayertoTrans)) {
            ObjectTotranparent = hit.transform.gameObject;
            ObjectTotranparent.GetComponent<SkeletonAnimation>().skeleton.A = 0.5f;
            Debug.DrawRay(transform.position, targetDirection * hit.distance, Color.yellow);
           // Debug.Log("Trensparent Item: Hit Transparentable");
        } else {
            if (ObjectTotranparent == null) {
               // Debug.Log("Transparent item: No item Yet");
            } else {
                ObjectTotranparent.GetComponent<SkeletonAnimation>().skeleton.A = 1f;
            }          
            Debug.DrawRay(transform.position, targetDirection * 1000, Color.white);
            //Debug.Log("Transparent Item: Not Found");
        }

    }
    void SetTargetControlled(Transform MonsterCont) {
        targetDirection = (MonsterCont.position - transform.position).normalized;
    }
    void SetTargetReleased() {
    }
}
