using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Character;
public class PlayerDetection : MonoBehaviour
{
    public MonsterController MonsterBehaviour;
    bool HitPlayer;
    [SerializeField] float Distance = 5f;
    [SerializeField] LayerMask PlayerType;
    private void Update() {
        HitPlayer = Physics.CheckSphere(gameObject.transform.position, Distance, PlayerType);
        if (HitPlayer)
        {
            MonsterBehaviour.MonsterState = MonsterController.Behaviour.Attack;
        }
        else
        {
            if (MonsterBehaviour.ControlledByPlayer == false) { 
                MonsterBehaviour.MonsterState = MonsterController.Behaviour.Patrol;
            }
           
        }
    }   
   
}
