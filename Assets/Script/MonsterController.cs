using UnityEngine.Events;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Cinemachine;
using System;
using Random = UnityEngine.Random;
namespace OLJ.Character {

    public class MonsterController : MonoBehaviour {
        public enum Behaviour {
            Idle,
            Patrol,
            Attack,
            Controlled,
            Taunted
        }
        public Behaviour MonsterState;
        public enum EfectedByTaunt {
            Yes,
            No

        }
        public EfectedByTaunt EasilyTaunt = EfectedByTaunt.No;

        public float DigestTime;
        [SerializeField] float MonsterSpeed = 5f;
        [SerializeField] Transform CameraTarget;
        [SerializeField] Rigidbody Rb;
        [SerializeField] public CinemachineVirtualCamera CameraPrority;
        [SerializeField] public GameObject[] InstantiatedOnDead;
        public Animator Anime;


        //Events
        public event Action OnDestroyed;
        public event Action OnPatrol;
        public event Action OnAttack;
        public event Action OnControlled;
        public event Action<bool> OnDisabled;
        public UnityEvent OnEating;

        //PatrolArea
        [SerializeField] float IdlingDuration;
        public Transform Patrolpos1, Patrolpos2;
        private Vector3 PatrolPos;

        //Navigation
        public NavMeshAgent Nav;
        public bool ControlledByPlayer = false;
        Vector3 MoveValue;


        // Taunted Behaviour
        bool IsEasilyTaunted;
        private void Start() {

            Nav = GetComponent<NavMeshAgent>();

            PickRandomPosition();

            switch (EasilyTaunt) {
                case EfectedByTaunt.Yes:
                    IsEasilyTaunted = true;
                    break;
                case EfectedByTaunt.No:
                    IsEasilyTaunted = false;
                    break;
            }

        }
        private void Update() {
            switch (MonsterState) {
                case Behaviour.Idle:
                    Idling();
                    break;
                case Behaviour.Patrol:
                    Patrol();
                    break;
                case Behaviour.Attack:
                    Attacking();
                    break;
                case Behaviour.Controlled:
                    Controlled();
                    break;
                case Behaviour.Taunted:
                    break;

            }
            if (DigestTime <= 0 && ControlledByPlayer) {
                //GameManager.instance.RestartLevel(gameObject.transform);
                MonsterState = Behaviour.Patrol;
                DigestTime = 0;
            }
        }

        protected virtual void Attacking() {
            OnAttack?.Invoke();
            Nav.enabled = true;
            Nav.isStopped = false;
            PlayAnimOnNotControlled();
        }
        protected virtual void Idling() {

        }
        protected virtual void Patrol() {
            RandomPatrol();
        }
        protected virtual void Controlled() {
            if (ControlledByPlayer) {
                OnControlled?.Invoke();
                InputControll();
                CameraPrority.m_Priority = 12;
                Nav.enabled = false;
                Rb.isKinematic = false;
            }


        }
        public virtual void dead() {

            for (int i = 0; i < InstantiatedOnDead.Length; i++) {

                GameObject inst = Instantiate(InstantiatedOnDead[i], transform.position, Quaternion.identity);
                Destroy(inst, 9);
            }
            OnDestroyed?.Invoke();
            Debug.Log("Monster Destroyed");
            Destroy(gameObject);

        }
        private void OnTriggerEnter(Collider other) {
            if (other.GetComponent<PlayerController>() != null) {
                OnEating?.Invoke();
                Anime.Play("eat");
                ControlledByPlayer = true;
                MonsterState = Behaviour.Controlled;
            }

        }
        void RandomPatrol() {
            OnPatrol?.Invoke();
            if (Nav.remainingDistance <= 0) {
                Nav.isStopped = true;
                StartCoroutine(PickRandomPosition());
            }
            PlayAnimOnNotControlled();
        }

        IEnumerator PickRandomPosition() {
            //Debug.Log("Randoming Destination");
            float x = Random.Range(Patrolpos1.position.x, Patrolpos2.position.x);
            float z = Random.Range(Patrolpos1.position.z, Patrolpos2.position.z);
            PatrolPos = new Vector3(x, 0, z);
            yield return new WaitForSeconds(IdlingDuration);
            Nav.destination = PatrolPos;
            Nav.isStopped = false;

        }
        void InputControll() {

            MoveValue.x = Input.GetAxisRaw("Horizontal");
            MoveValue.z = Input.GetAxisRaw("Vertical");
            Rb.velocity = MoveValue.normalized * MonsterSpeed;
            PlayAnimOnControlled();
        }
        void PlayAnimOnControlled() {
            if (MoveValue != Vector3.zero) {
                Anime.SetFloat("Horizontal", DirCalculation(MoveValue.x));
                Anime.SetFloat("Vertical", DirCalculation(MoveValue.z));
            }
            Anime.SetFloat("Speed", MoveValue.magnitude);

        }
        private float DirCalculation(float value) {
            if (value > 0) {
                return 1;
            } else if (value < 0) {
                return -1;
            } else {
                return 0;
            }
        }
        void PlayAnimOnNotControlled() {
            if (Mathf.Abs(Mathf.Floor(Nav.desiredVelocity.x)) <= 0.5f * Nav.speed) {
                Anime.SetFloat("Horizontal", 0);
            } else {
                Anime.SetFloat("Horizontal", Mathf.Sign(Nav.desiredVelocity.x));

            }
            if (Mathf.Abs(Mathf.Floor(Nav.desiredVelocity.z)) <= 0.5f * Nav.speed) {
                Anime.SetFloat("Vertical", 0);
            } else {
                Anime.SetFloat("Vertical", Mathf.Sign(Nav.desiredVelocity.z));
            }

            Anime.SetFloat("Speed", Nav.desiredVelocity.magnitude);
        }

        public virtual void TauntEffect(Transform MonsterPosition) {
            if (IsEasilyTaunted) {
                LeanTween.move(gameObject, MonsterPosition.position, 4);

            } else {

                Idling();
            }

        }
        public void DisbleThis(bool State) {
            OnDisabled?.Invoke(State);
            Nav.isStopped = !State;
            Rb.velocity = Vector3.zero;
            Anime.SetFloat("Speed", 0);
            this.enabled = State;
            Debug.Log("MonsterController Script From: " + gameObject.name + " is " + State);
            Debug.Log("NavState " + gameObject.name + ": " + Nav.isStopped);

        }
    }
}
