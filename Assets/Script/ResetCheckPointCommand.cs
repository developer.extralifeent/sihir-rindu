using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Save and Load",
                "Reset Player Prefs",
                " Reset Player Prefs.")]
[AddComponentMenu("")]
public class ResetCheckPointCommand : Command
{
    public override void OnEnter()
    {
        PlayerPrefs.DeleteAll();
        Continue();
    }
}
