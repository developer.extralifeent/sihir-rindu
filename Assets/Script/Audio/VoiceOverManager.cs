using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceOverManager : Singleton<VoiceOverManager>
{
    [SerializeField] private AudioSource source;
    public AudioSource Source => source;

    public void PlayVO(AudioClip _voClip)
    {
        if (source.clip == _voClip) return;

        source.Stop();
        source.clip = _voClip;
        source.Play();
    }

    public void StopVO()
    {
        source.Stop();
        source.clip = null;
    }
}
