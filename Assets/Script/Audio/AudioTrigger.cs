using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{
    public string PlayClip, StopClip;
    private void Start()
    {
        AudioManager.instance.Play(PlayClip);
        AudioManager.instance.Stop(StopClip);
    }
}
