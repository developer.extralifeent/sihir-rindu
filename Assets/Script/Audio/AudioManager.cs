using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public Sound[] Audio;
    //public Sound[] FootSteps;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        foreach (Sound s in Audio)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.soundAudio;
            s.source.volume = s.Volume;
            s.source.pitch = s.Pitch;
            s.source.loop = s.loop;
        }
        //foreach (Sound f in FootSteps)
        //{
        //    f.source = gameObject.AddComponent<AudioSource>();
        //    f.source.clip = f.soundAudio;
        //    f.source.volume = f.Volume;
        //    f.source.pitch = f.Pitch;
        //    f.source.loop = f.loop;
        //}
    }

    public void Play(string name)
    {
        Sound s = Array.Find(Audio, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found !");
            return;
        }
        s.source.Play();

        //Sound f = Array.Find(FootSteps, sound => sound.name == name);
        //if (s == null)
        //{
        //    Debug.LogWarning("Footstep sound: " + name + " not found !");
        //    return;
        //}
        //f.source.Play();
    }
    public void Stop(string name)
    {
        Sound s = Array.Find(Audio, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found !");
            return;
        }
        s.source.Stop();

        //Sound f = Array.Find(FootSteps, sound => sound.name == name);
        //if (s == null)
        //{
        //    Debug.LogWarning("Footstep Sound: " + name + " not found !");
        //    return;
        //}
        //f.source.Stop();
    }
}