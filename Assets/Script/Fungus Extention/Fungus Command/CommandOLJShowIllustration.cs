using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLJ.UI;
using Fungus;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                "OLJShowIllustration",
                "Show Illustration")]
    [AddComponentMenu("")]
    public class CommandOLJShowIllustration : Command {
        [SerializeField] Sprite illust;
        public override void OnEnter() {
            base.OnEnter();
            UIManager.Instance.ShowIllustration(illust);
            Continue();
        }
        public override string GetSummary()
        {
            string ill ="illust: "+ illust.name;
            return ill;
        }
    }
    
}
