using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.UI;
using Fungus;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                 "OLJ Set Bust",
                 "Set Character Bust")]
    [AddComponentMenu("")]
    public class CommandOLJSetBust : Command {
        [SerializeField] string charName = "Character Name";
        [SerializeField] string charExpression = "Character Expression";
        [SerializeField] UIManager.BustDirection face;
       
        public override void OnEnter() {
            if (UIManager.Instance && UIManager.Instance.CharacterList.Count > 0) {
                UIManager.Instance.ShowBustImage(charName, charExpression, face);
               
            }
            Continue();
        }
        public override string GetSummary() {
            string sum = charName + " " + charExpression+" " + face;
            return sum;
        }
    }
}

