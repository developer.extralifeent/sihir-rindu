using Fungus;
using OLJ.Manager;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                  "Play Book Spine Anim",
                  "Play Book Spine Animation")]
    public class CommandMainMenuBook : Command
    {
        private MainMenuUIManager mainMenuUIManager;

        private void Start()
        {
            mainMenuUIManager = MainMenuUIManager.Instance;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            mainMenuUIManager.ChangeChapter(true);
        }
    }
}
