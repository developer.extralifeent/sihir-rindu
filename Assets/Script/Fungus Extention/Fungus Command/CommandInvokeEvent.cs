using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
               "Invoke Event",
               "Invoke Event From Flowchart")]
    [AddComponentMenu("")]
    public class CommandInvokeEvent : Command
    {
        [SerializeField] UnityEvent onCommandExecute;

        public override void OnEnter()
        {
            base.OnEnter();
            onCommandExecute.Invoke();
            Continue();
        }
    }
}
