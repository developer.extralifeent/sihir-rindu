using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.Character;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                 "Move Character",
                 "Move character and play it's movement aniamtion")]
    [AddComponentMenu("")]
    public class CommandMoveCharacter : Command {

        enum Orientation { Front, Back, Left, Right, None};

        [SerializeField] CharacterAnimation character;
        [SerializeField] Orientation finalOrientation = Orientation.None;
        [SerializeField] List<Transform> positions;
        [SerializeField] float customMoveSpeed = -1;
        [SerializeField] bool waitFinished = true;
        private int positionIndex;

        public override void OnEnter() {
            character.GetComponent<CharacterBody>().DisableControl();
            if (waitFinished) {
                StartCoroutine(MoveToPositions());
            } else {
                StartCoroutine(MoveToPositions());
                Continue();
            }
        }
        public override string GetSummary() {
            string sum = "";
            if (character) {
                if(positions.Count > 0)
                {
                    sum = $"Move to: {positions[0].name} Final orientation: {finalOrientation} ";
                }
            }
            return sum;

        }
        IEnumerator MoveToPositions() {

            float defaultSpeed = character.CharSpeed;

            if (customMoveSpeed != -1)
                character.CharSpeed = customMoveSpeed;

            while (positionIndex < positions.Count) {
                
                Transform targetPosition = positions[positionIndex];
                Vector3 lastPosition = (targetPosition.position - character.transform.position).normalized;
                while (Vector3.Distance(character.transform.position, targetPosition.position) > 0.5f) {
                    character.MoveCharacter(targetPosition);
                    yield return null;
                }
                character.SetAnimDirection(lastPosition, 0);
                positionIndex++;
                yield return new WaitForSeconds(0);
            }

            if (customMoveSpeed != -1)
                character.CharSpeed = defaultSpeed;

            if (finalOrientation != Orientation.None)
            {
                switch (finalOrientation)
                {
                    case Orientation.Back: character.SetAnimDirection(Vector3.forward, 0); break;
                    case Orientation.Front: character.SetAnimDirection(Vector3.back, 0); break;
                    case Orientation.Left: character.SetAnimDirection(Vector3.left, 0); break;
                    case Orientation.Right: character.SetAnimDirection(Vector3.right, 0); break;
                }
            }

            Continue();
            Debug.Log("Move Done");
        }
    }
}

