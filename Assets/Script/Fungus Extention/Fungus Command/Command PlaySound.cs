using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Audio",
                "Play BGM",
                "Play Audio From Audio Manager")]
[AddComponentMenu("")]
public class CommandPlaySound : Command
{
    [SerializeField] string AudioName;
    [SerializeField] bool IsPlaying;
    public override void OnEnter()
    {
        if (IsPlaying) { 
            AudioManager.instance.Play(AudioName);
        }
        else
        {
            AudioManager.instance.Stop(AudioName);
        }
    }
    public override string GetSummary()
    {
        if (IsPlaying)
        {
            string s = "Play " + AudioName;
            return s;
        }
        else
        {
            string s = "Stop " + AudioName;
            return s;
        }
       
    }
}
