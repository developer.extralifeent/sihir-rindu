using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ",
                    "Show Notification",
                    "Show Notification Card")]
    [AddComponentMenu("")]
    public class CommandShowNotification : Command {
        [SerializeField] string header;
        [TextArea(0, 10)]
        [SerializeField] string notifText;
        public override void OnEnter() {
            if (OLJ.UI.UIManager.Instance) {
                OLJ.UI.UIManager.Instance.ShowNotification(header, notifText);
            }
            Continue();
        }
    }
}

