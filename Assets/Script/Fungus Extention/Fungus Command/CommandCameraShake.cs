using Cinemachine;
using Fungus;
using MyBox;
using OLJ.Character;
using OLJ.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                   "OLJ Camera Shake",
                   "Shake Camera On Certain Events")]
    public class CommandCameraShake : Command, ISerializationCallbackReceiver
    {
        private bool isSerialized = false;

        [Separator("Class References")]
        [SerializeField] private CharacterBody player;
        [SerializeField] private CinemachineVirtualCamera virtualCam;
        [SerializeField] private CinemachineBasicMultiChannelPerlin cinemachinePerlin;

        [Separator("Camera Shake Value", true)]
        [SerializeField] private float shakeDuration = .2f;
        [SerializeField] private float magnitude = 5f;
        private float currentTime = 0f;

        [Separator("Others")]
        [SerializeField] private bool waitTillFinished;
        [SerializeField] private bool isDamaging = false;
        [SerializeField, ConditionalField(nameof(isDamaging), false)] private float damageToTake;

        private void Update()
        {
            ShakeCamCountDown();
        }

        public override void OnEnter()
        {
            base.OnEnter();
            ShakeCam();
        }

        private void ShakeCam()
        {
            cinemachinePerlin = virtualCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

            cinemachinePerlin.m_AmplitudeGain = magnitude;
            currentTime = shakeDuration;

            if (isDamaging)
                player.GetComponentInChildren<PlayerHp>().TakeDamage(damageToTake);

            if (!waitTillFinished)
                Continue();
        }

        private void ShakeCamCountDown()
        {
            if (currentTime <= 0f) return;
            currentTime -= Time.deltaTime;

            if(currentTime <= 0f)
            {
                currentTime = 0f;
                cinemachinePerlin.m_AmplitudeGain = Mathf.Lerp(magnitude, 0f, 1 - (currentTime / shakeDuration));

                if(waitTillFinished)
                    Continue();
            }
        }

        public void OnAfterDeserialize()
        {
            isSerialized = true;
        }

        public void OnBeforeSerialize()
        {
            if(!isSerialized)
            {
                player = ControlManager.Instance.CurrentBody;

                if(player != null)
                {
                    virtualCam = player.GetComponentInChildren<CinemachineVirtualCamera>();
                }
                
                cinemachinePerlin = virtualCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            }
        }
    }
}
