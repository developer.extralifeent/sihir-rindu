using Fungus;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Cutscene",
                   "Play Spine Animation",
                   "Play Spine Animation")]
    public class CommandPlaySpineAnime : Command 
    {
        [SerializeField] private SkeletonAnimation skeletonAnim;
        [SpineAnimation(dataField: "skeletonAnim"), SerializeField] private string animName;
        [SerializeField] private bool loopAnimation;
        [SerializeField] private bool waitTillFinished;

        public override void OnEnter()
        {
            base.OnEnter();
            StartCoroutine(PlaySpineAnimCoroutine());
            
        }

        private IEnumerator PlaySpineAnimCoroutine()
        {
            //skeletonAnim.AnimationState.ClearTrack(0);
            skeletonAnim.AnimationState.SetAnimation(0, animName, loopAnimation);

            float animDuration = skeletonAnim.skeleton.Data.FindAnimation(animName).Duration;
            if(waitTillFinished)
                yield return new WaitForSeconds(animDuration);

            Continue();

        }

        public override string GetSummary() {
            string sum = "";
            if (skeletonAnim != null) {
                sum = $"Anim name: {animName}";
            }
            return sum;
        }
    }
}

