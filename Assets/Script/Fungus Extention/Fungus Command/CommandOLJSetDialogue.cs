using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.UI;
using Fungus;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                 "OLJSetDialogue",
                 "Set SayDialogue For Olj")]
    [AddComponentMenu("")]
    public class CommandOLJSetDialogue : Command {
        [SerializeField] UIManager.SayDialogueType sayDialogueType;
        [SerializeField] bool hideDialogue;
        public override void OnEnter() {
            base.OnEnter();
            UIManager.Instance.SetSayDialogue(sayDialogueType);
            if (hideDialogue) {
                UIManager.Instance.GetCurrentDialogue().GetComponent<CanvasGroup>().alpha = 0;
            }
            Continue();
        }
        public override string GetSummary() {
            string sum = "Say Dialogue Type: " + sayDialogueType;
            return sum;
        }
    }

}

