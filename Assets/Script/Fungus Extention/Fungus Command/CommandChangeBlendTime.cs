using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using Cinemachine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Cutscene",
                "OLJSetCameraBlendTime",
                "Set CameraBlendTime")]
    [AddComponentMenu("")]
    public class CommandChangeBlendTime : Command
    {
        [SerializeField] float blendTimeDuration = 2f;
        public override void OnEnter()
        {
            Camera.main.GetComponent<CinemachineBrain>().m_DefaultBlend.m_Time = blendTimeDuration;
            Debug.Log(Camera.main.GetComponent<CinemachineBrain>().m_DefaultBlend.m_Time);
            Continue();
        }
    }
}

