using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Manager;
using Fungus;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ",
                 "Stop Player",
                 "StopPlayer")]
    [AddComponentMenu("")]
    public class CommandStopPlayer : Command {
        [SerializeField] bool receiveInput;
        public override void OnEnter() {
            base.OnEnter();
            if (ControlManager.Instance != null) {
                if (receiveInput) {

                    ControlManager.Instance.EnableControl();
                } else {
                    ControlManager.Instance.DisableControl();
                }
            }
            Continue();
        }
    }

}

