using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using Spine;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                   "Play Animation",
                   "Play character animation")]
    [AddComponentMenu("")]
    public class CommandPlayAnimation : Command {
        [SerializeField] GameObject objectToAnimate;
        [SerializeField] string animationName="";
        [SerializeField] bool waitFinished;
        [Space]
        [SerializeField] bool freezeAnimationAfterFinished = false; //ini buat ngefreeze frame terakhir animasi, jadi ga pindah ke idle. nanti kalo udah tau caranya, tolong dibikin supaya ngeloop animasi nya (tanpa ke idle)
        [SerializeField] bool loopAnimation = false;

        float duration;

        Animator animee;
        private void Start()
        {
            if (objectToAnimate.GetComponent<OLJ.Character.CharacterAnimation>())
                animee = objectToAnimate.GetComponent<OLJ.Character.CharacterAnimation>().Anime;
            else if (objectToAnimate.GetComponentInChildren<Animator>())
                animee = objectToAnimate.GetComponentInChildren<Animator>();
            else
                animee = GetComponent<Animator>();
            AnimationClip animClip = GetAnimationClip(animationName, animee);

            if (animClip)
            {
                duration = animClip.length;
                animClip.wrapMode = loopAnimation ? WrapMode.Loop : WrapMode.Once;
            }
                
        }

        private AnimationClip GetAnimationClip(string name,Animator anim)
        {
            // Get all animation clips from the animator
            AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;

            // Search for the animation clip by name
            foreach (AnimationClip clip in clips)
            {
                if (clip.name == name)
                {
                    return clip;
                }
            }

            // Return null if the clip is not found
            return null;
        }
        public override void OnEnter() {
            base.OnEnter();
            animee.Play(animationName);

            if (waitFinished && animee) {
                StartCoroutine(WaitFinishedAnimation(duration));
            } else {
                StartCoroutine(FreezeAnimationCoroutine(duration));
            }
        }

        IEnumerator FreezeAnimationCoroutine(float length)
        {
            Continue();
            if (!freezeAnimationAfterFinished)
            {
                yield return new WaitForSeconds(length);
                if(objectToAnimate.GetComponent<OLJ.Character.CharacterAnimation>()) animee.Play("Idle");
            }
        }

        IEnumerator WaitFinishedAnimation(float length) {
            yield return new WaitForSeconds(length);

            if (!freezeAnimationAfterFinished && objectToAnimate.GetComponent<OLJ.Character.CharacterAnimation>()) animee.Play("Idle");
            Continue();
        }
        public override string GetSummary() {
            string sum;
            if (objectToAnimate) {
                if (objectToAnimate.GetComponent<OLJ.Character.CharacterAnimation>()) {
                    sum = $"object has character animation";
                } else if (objectToAnimate.GetComponentInChildren<Animator>()) {
                    sum = $"object has animator on children";
                } else {
                    sum = $"object has animator";
                }
            } else {
                sum = "No animation object";
            }
            return sum + $" Animation name: {animationName}";
        }
    }
}

