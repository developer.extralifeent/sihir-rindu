using Fungus;
using MyBox;
using OLJ.Character;
using OLJ.Manager;
using OLJ.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                "QTE Button Pressing",
                "Add Button Pressing QTE")]
    public class CommandButtonPressing : Command
    {
        [Header("QTE Value")]
        [SerializeField] int pressingDurationThreshold = 5;
        [SerializeField] float pressingValueMultiplier = 1.5f;
        QTEManager qteM;

        [Header("Spine")]
        [SerializeField] private bool useSpine = true;
        [SerializeField, ConditionalField(nameof(useSpine), false)] private SpineAnimationData spa;

        [Header("Animation")]
        [SerializeField, ConditionalField(nameof(useSpine), true)] private string animStateName;

        private void Start()
        {
            useSpine = true;

            qteM = QTEManager.Instance;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            if (useSpine)
            {
                if (QTEUIManager.Instance && spa.animName.Length > 0 && spa.sk != null && spa.animName != null)
                {
                    QTEUIManager.Instance.SetQTEAnimation(spa.sk, spa.animName);
                }
            }
            else
            {
                qteM.StartAnimQTE(animStateName);
            }


            qteM.PressingDurationThreshold = pressingDurationThreshold;
            qteM.PressingValueMultiplier = pressingValueMultiplier;

            qteM.onQTEfinished?.AddListener(NextLine);
            QTEManager.Instance.StartQTE(1);
            Debug.Log("QTE Enter: Button Pressing");

        }

        void NextLine(int val)
        {
            Continue();
        }

        public override void OnExit()
        {
            base.OnExit();
            if (QTEUIManager.Instance)
            {
                QTEUIManager.Instance.HideQTEAnimation();
            }
            QTEManager.Instance.onQTEfinished?.RemoveListener(NextLine);

            if (!useSpine)
                QTEManager.Instance.EndAnimQTE();
        }
    }
}
