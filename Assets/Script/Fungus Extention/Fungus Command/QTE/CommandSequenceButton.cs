using Fungus;
using OLJ.Manager;
using OLJ.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                "QTE Sequence Button",
                "Add Sequence Button QTE")]
    public class CommandSequenceButton : Command
    {
        [Header("Spine")]
        [SerializeField] private bool useSpine;
        [SerializeField] SpineAnimationData spa;

        [Header("Animation")]
        [SerializeField] private string animStateName;

        private QTEManager qteM;

        private void Start()
        {
            qteM = QTEManager.Instance;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            Debug.Log("QTE Enter: Sequencing");
            if (useSpine)
            {
                if (QTEUIManager.Instance && spa.animName.Length > 0 && spa.sk != null && spa.animName != null)
                {
                    QTEUIManager.Instance.SetQTEAnimation(spa.sk, spa.animName);
                }
            }
            else
            {
                qteM.StartAnimQTE(animStateName);
            }


            if (qteM)
            {
                //qteM.PressingDurationThreshold = pressingDurationThreshold;
                //qteM.PressingValueMultiplier = pressingValueMultiplier;

                qteM.onQTEfinished?.AddListener(NextLine);
                qteM.StartQTE(2);
            }

        }

        void NextLine(int val)
        {
            Continue();
        }

        public override void OnExit()
        {
            base.OnExit();
            if (QTEUIManager.Instance)
            {
                QTEUIManager.Instance.HideQTEAnimation();
            }
            qteM.onQTEfinished?.RemoveListener(NextLine);

            if (!useSpine)
                qteM.EndAnimQTE();
        }
    }
}
