using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using Spine.Unity;
using MyBox;
using OLJ.UI;
using OLJ.Manager;
using OLJ.Utility;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ",
                    "QTE Mashing Button",
                    "Add Mashing Button QTE")]
    [AddComponentMenu("")]
    public class CommandMashButton : Command {

        [Header("QTE Value")]
        [SerializeField] int mashCountThreshold = 20;
        [SerializeField] float mashDuration = 0.2f;
        [SerializeField] float mashResetTime = 1f;

        [Header("Spine")]
        [SerializeField] private bool useSpine;
        [SerializeField] SpineAnimationData spa;

        [Header("Animation")]
        [SerializeField] private string animStateName;

        QTEManager qteM;
        private void Start() {
            qteM = QTEManager.Instance;
        }

        public override void OnEnter() {
            base.OnEnter();
            Debug.Log("QTE Enter: Mash Button");
            if (useSpine)
            {
                if (QTEUIManager.Instance && spa.animName.Length > 0 && spa.sk != null && spa.animName != null)
                {
                    QTEUIManager.Instance.SetQTEAnimation(spa.sk, spa.animName);
                }
            }
            else
            {
                qteM.StartAnimQTE(animStateName);
            }

            if (qteM) {
                qteM.CountThreshold = mashCountThreshold;
                qteM.Duration = mashDuration;
                qteM.ResetTime = mashResetTime;
               
                qteM.onQTEfinished?.AddListener(NextLine);                
                qteM.StartQTE(0);               
            }
           
        }

        void NextLine(int val) {           
            Continue();
        }

        public override void OnExit() {
            base.OnExit();
            if (QTEUIManager.Instance) {
                QTEUIManager.Instance.HideQTEAnimation();
            }
            qteM.onQTEfinished?.RemoveListener(NextLine);

            if (!useSpine)
                qteM.EndAnimQTE();
        }
       
       
    }
}

