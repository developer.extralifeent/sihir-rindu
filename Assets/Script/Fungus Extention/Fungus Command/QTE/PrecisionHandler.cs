using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Manager
{
    public class PrecisionHandler : Singleton<PrecisionHandler>
    {
        private bool isOnPrecisionArea;

        public bool IsOnPrecisionArea { get { return isOnPrecisionArea; } }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("PrecisionArea"))
            {
                isOnPrecisionArea = true;
                Debug.Log("Precision Area : isOnPrecisionArea " + isOnPrecisionArea);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("PrecisionArea"))
            {
                isOnPrecisionArea = false;
                Debug.Log("Precision Area : isOnPrecisionArea " + isOnPrecisionArea);
            }
        }
    }
}
