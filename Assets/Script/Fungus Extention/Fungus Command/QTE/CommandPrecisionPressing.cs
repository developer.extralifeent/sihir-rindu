using Fungus;
using MyBox;
using OLJ.Manager;
using OLJ.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                "QTE Precision Pressing",
                "Add Precision Pressing QTE")]
    public class CommandPrecisionPressing : Command
    {
        [Header("Randomized Precision Area Value")]
        [SerializeField] private bool randomizePrecisionAreaSize;

        [SerializeField, ConditionalField(nameof(randomizePrecisionAreaSize), false)] private float minXScalePrecisionArea;
        [SerializeField, ConditionalField(nameof(randomizePrecisionAreaSize), false)] private float maxXScalePrecisionArea;
        [SerializeField, ConditionalField(nameof(randomizePrecisionAreaSize), true)] private float fixedXScalePrecisionArea;

        [Header("Sliding speed")]
        [SerializeField] private float slidingSpeed = 750f;

        [Header("Animation")]
        [SerializeField] private bool useSpine = true;
        [SerializeField, ConditionalField(nameof(useSpine), false)] SpineAnimationData spa;
        [SerializeField, ConditionalField(nameof(useSpine), true)] private string animStateName;

        private QTEManager qteM;

        private void Awake()
        {
            qteM = QTEManager.Instance;
        }


        public override void OnEnter()
        {
            base.OnEnter();
            Debug.Log("QTE Enter: Precision Pressing");
            if (useSpine)
            {
                if (QTEUIManager.Instance && spa.animName.Length > 0 && spa.sk != null && spa.animName != null)
                {
                    QTEUIManager.Instance.SetQTEAnimation(spa.sk, spa.animName);
                }
            }
            else
            {
                qteM.StartAnimQTE(animStateName);
            }

            if (qteM != null)
            {
                qteM.IsPrecisionAreaRandomized = randomizePrecisionAreaSize;
                qteM.MinXScalePrecisionArea = minXScalePrecisionArea;
                qteM.MaxXScalePrecisionArea = maxXScalePrecisionArea;
                qteM.FixedXScalePrecisionArea = fixedXScalePrecisionArea;

                if (slidingSpeed == 0)
                {
                    //Debug.LogWarning("Sliding speed in precision pressing flowchart is stil set to 0, using default value from QTE Manager");
                    slidingSpeed = qteM.SlidingTime;
                }
                else
                {
                    qteM.SlidingTime = slidingSpeed;
                }

                qteM.StartQTE(3);
                qteM.onQTEfinished.AddListener(NextLine);
                Debug.Log("QTE Precision : Added QTE listener");
            }
        }

        private void NextLine(int val)
        {
            Continue();
        }

        public override void OnExit()
        {
            base.OnExit();
            if (QTEUIManager.Instance)
            {
                QTEUIManager.Instance.HideQTEAnimation();
            }

            qteM.onQTEfinished?.RemoveListener(NextLine);
            Debug.Log("QTE Precision : Removed QTE listener");

            if (!useSpine)
                QTEManager.Instance.EndAnimQTE();
        }
    }
}