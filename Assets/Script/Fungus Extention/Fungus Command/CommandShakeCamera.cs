using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using Cinemachine;

namespace OLJ.FungusExtention {
    [AddComponentMenu("")]
    public class CommandShakeCamera : Command {
        [SerializeField] float duration;
        [SerializeField] bool waitFinished = true;
        CinemachineImpulseSource impulseSrc;
       
        public override void OnEnter() {
            impulseSrc = Camera.main.GetComponent<CinemachineImpulseSource>();
            if (impulseSrc) {
                impulseSrc.m_ImpulseDefinition.m_TimeEnvelope.m_SustainTime = duration;
                impulseSrc.GenerateImpulse();
            }            
            if (waitFinished) {
                StartCoroutine(WaitImpulse());
            } else {
                Continue();
            }
        }
        IEnumerator WaitImpulse() {
            yield return new WaitForSeconds(duration);
            Continue();
        }
    }
}

