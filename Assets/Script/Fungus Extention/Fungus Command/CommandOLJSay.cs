using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.UI;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
               "OLJSay",
               "Set Dialogue for character")]
    [AddComponentMenu("")]
    public class CommandOLJSay : Say {
        [Header("----Set Dialogue Parameter----")]
        [SerializeField] UIManager.SayDialogueType sayDialogueType;
        [SerializeField] bool hideDialogue;


        [Header("----Set Bust----")]
        [SerializeField] int characterIndex;
        [SerializeField] int expressionIndex;
        [SerializeField] UIManager.BustDirection face;
        private void Start() {
           
        }
        public override void OnEnter() {
                    
            base.OnEnter();
        }
    }
}

