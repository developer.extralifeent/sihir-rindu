using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                "OLJFlash",
                "Set Flash")]
    [AddComponentMenu("")]
    public class CommandFlash : Command {
        [SerializeField] float fadeInDuration;
        [SerializeField] float holdDuration;
        [SerializeField] float fadeOutDuration;
        public override void OnEnter() {
            if (OLJ.UI.UIManager.Instance) {
                OLJ.UI.UIManager.Instance.ShowFlash(fadeInDuration,holdDuration,fadeOutDuration);
            }
            Continue();
        }
    }
}

