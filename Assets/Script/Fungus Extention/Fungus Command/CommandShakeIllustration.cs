using Fungus;
using OLJ.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/illustration",
                   "OLJ Illustration Shake",
                   "Shake Illustration UI")]
    public class CommandShakeIllustration : Command
    {
        private UIManager uiManager;

        [SerializeField] private float magnitude = .15f;
        [SerializeField] private float shakeDuration = .5f;
        private float currentTime = 0f;

        private Vector3 illustStartPos;

        private void Awake()
        {
            uiManager = UIManager.Instance;
        }

        private void Start()
        {
            illustStartPos = uiManager.IllustrationPage.transform.localPosition;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            StartCoroutine(ShakeIllustration());
            Continue();
        }

        private IEnumerator ShakeIllustration()
        {
            currentTime = shakeDuration;

            while(currentTime > 0f)
            {
                float xPos = Random.Range(-1f, 1f) * magnitude;
                float yPos = Random.Range(-1f, 1f) * magnitude;

                uiManager.IllustrationPage.transform.localPosition = new Vector3(xPos, yPos, uiManager.IllustrationPage.transform.localPosition.z);

                currentTime -= Time.deltaTime;
                yield return null;
            }

            uiManager.IllustrationPage.transform.localPosition = illustStartPos;
        }
    }
}
