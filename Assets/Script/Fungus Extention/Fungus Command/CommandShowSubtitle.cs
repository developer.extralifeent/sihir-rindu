using DG.Tweening;
using Fungus;
using System.Collections;
using TMPro;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
               "Show World Space Subtitle",
               "Set subtitle in world space")]
    public class CommandShowSubtitle : Command
    {
        [Header("Dialogs")]
        [SerializeField] private TMP_Text textObject;
        [SerializeField] private Canvas subtitleCanvas;
        [SerializeField, TextArea] private string dialog;
        [SerializeField] private AudioClip voiceOverClip;
        [SerializeField] private float delayAfterVoice = 2f;
        [SerializeField] private bool waitForVO = true;
        private Vector3 textStartPos;
        private Vector3 textObjectStartOutPos;

        [Header("Typing Anim (The smaller the numbers, the faster the text)")]
        [SerializeField] private bool typewriterText = true;
        [SerializeField, MyBox.ConditionalField(nameof(typewriterText), false)] private float charDelayTime = .05f;

        [Header("Text Anim")]
        [SerializeField] private bool animateAllText;
        [SerializeField, MyBox.ConditionalField(nameof(animateAllText), false)] private TEXT_ANIM_TYPE animType;
        [SerializeField, MyBox.ConditionalField(nameof(animateAllText), false)] private AnimProperty animProperty;

        [Header("Text In Transition")]
        [SerializeField] private bool useInTransitionAnim;
        [SerializeField, MyBox.ConditionalField(nameof(useInTransitionAnim), false)] private float textOffsetIn;
        [SerializeField, MyBox.ConditionalField(nameof(useInTransitionAnim), false)] private TEXT_TRANSITION_ANIM transitionInAnimType;
        [SerializeField, MyBox.ConditionalField(nameof(useInTransitionAnim), false)] private TransitionInProperty transitionInProperty;

        [Header("Text Out Transition")]
        [SerializeField] private bool useOutTransitionAnim = true;
        [SerializeField, MyBox.ConditionalField(nameof(useOutTransitionAnim), false)] private float textOffsetOut;
        [SerializeField, MyBox.ConditionalField(nameof(useOutTransitionAnim), false)] private TEXT_TRANSITION_ANIM transitionOutAnimType;
        [SerializeField, MyBox.ConditionalField(nameof(useOutTransitionAnim), false)] private TransitionOutProperty transitionOutProperty;

        private VoiceOverManager voManager;


        private void Awake()
        {
            voManager = VoiceOverManager.Instance;

        }
      
        private void Start()
        {
            SetUpTextAnimation();
            //canvasStartPos = subtitleCanvas.transform.localPosition;
            textStartPos = textObject.transform.localPosition;
            Debug.Log("Text Start Pos: " + textStartPos);
        }

        public override void OnEnter()
        {
            base.OnEnter();

            textObject.transform.localPosition = textStartPos;
            voManager.PlayVO(voiceOverClip);

            StartCoroutine(CycleText());
        }

        public override void OnExit()
        {
            base.OnExit();
            textObject.transform.DOKill(true);
            textObject.transform.localPosition = textStartPos;

            StopAllCoroutines();
        }
        public override string GetSummary() {
            string sum ="";
            if (voiceOverClip) {
                sum = $"{voiceOverClip.name}: {dialog}";
            }
            return sum;
            
        }
        private IEnumerator CycleText()
        {
            textObject.DOFade(1f, transitionOutProperty.fadeOutDuration);
            PlayTextTransition(true);

            if (typewriterText)
            {
                foreach (char character in dialog.ToCharArray())
                {
                    if (animateAllText)
                        textObject.text += $"<link={animType}>{character}</link>";
                    else
                        textObject.text += character;

                    yield return new WaitForSeconds(charDelayTime);
                }
            }
            else
            {
                if (animateAllText)
                    textObject.text = $"<link={animType}>{dialog}</link>";
                else
                    textObject.text = dialog;
            }

            if(waitForVO)
                yield return new WaitUntil(() => !voManager.Source.isPlaying);
            else
                yield return new WaitForSeconds(delayAfterVoice);

            if (!useOutTransitionAnim)
            {
                Continue();
                yield break;
            }

            PlayTextTransition(false);
            textObject.DOFade(0f, transitionOutProperty.fadeOutDuration);
            yield return new WaitForSeconds(transitionOutProperty.fadeOutDuration);
            textObject.text = string.Empty;
            Continue();
        }

        private void PlayTextTransition(bool _isTransitionIn)
        {
            if(_isTransitionIn)
            {
                if (!useInTransitionAnim) return;

                switch (transitionInAnimType)
                {
                    case TEXT_TRANSITION_ANIM.move:
                        MoveText(_isTransitionIn);
                        Debug.Log("Transition Move");
                        break;
                    case TEXT_TRANSITION_ANIM.scale:
                        ScaleText(_isTransitionIn);
                        Debug.Log("Transition Scale");
                        break;
                }
            }
            else
            {
                if(!useOutTransitionAnim) return;

                switch (transitionOutAnimType)
                {
                    case TEXT_TRANSITION_ANIM.move:
                        MoveText(_isTransitionIn);
                        break;
                    case TEXT_TRANSITION_ANIM.scale:
                        ScaleText(_isTransitionIn);
                        Debug.Log("Transition Scale");
                        break;
                }
            }
        }

        private void MoveText(bool _isMovingIn)
        {
            if (_isMovingIn)
            {
                if (transitionInProperty.useDoShakeIn)
                    transitionInProperty.SetShakeIn(textObject.transform);

                switch (transitionInProperty.textMoveInDir)
                {
                    case TEXT_MOVE_DIR.left:
                        textObject.transform.localPosition = textStartPos + Vector3.left * textOffsetIn;
                        break;
                    case TEXT_MOVE_DIR.right:
                        textObject.transform.localPosition = textStartPos + Vector3.right * textOffsetIn;
                        break;
                    case TEXT_MOVE_DIR.up:
                        textObject.transform.localPosition = textStartPos + Vector3.up * textOffsetIn;
                        break;
                    case TEXT_MOVE_DIR.down:
                        textObject.transform.localPosition = textStartPos + Vector3.down * textOffsetIn;
                        break;
                }
                textObject.transform.DOLocalMove(textStartPos, transitionInProperty.textMoveInDuration);
            }
            else
            {
                if (transitionOutProperty.useDoShakeOut)
                    transitionOutProperty.SetShakeOut(textObject.transform);

                switch (transitionOutProperty.textMoveOutDir)
                {
                    case TEXT_MOVE_DIR.left:
                        textObjectStartOutPos = textStartPos + Vector3.left * textOffsetOut;
                        break;
                    case TEXT_MOVE_DIR.right:
                        textObjectStartOutPos = textStartPos + Vector3.right * textOffsetOut;
                        break;
                    case TEXT_MOVE_DIR.up:
                        textObjectStartOutPos = textStartPos + Vector3.up * textOffsetOut;
                        break;
                    case TEXT_MOVE_DIR.down:
                        textObjectStartOutPos = textStartPos + Vector3.down * textOffsetOut;
                        break;
                }

                textObject.transform.DOLocalMove(textObjectStartOutPos, transitionOutProperty.textMoveOutDuration);
            }
        }

        private void ScaleText(bool _isScalingUp)
        {
            if (_isScalingUp)
            {
                textObject.transform.localScale = Vector3.zero;
                textObject.transform.DOScale(1f, transitionInProperty.scalingInDuration);
            }
            else
            {
                textObject.transform.localScale = Vector3.one;
                textObject.transform.DOScale(0f, transitionOutProperty.scalingOutDuration);
            }
        }

        private void SetUpTextAnimation()
        {
            //force clearing and adding our own effects here
            //TMProLinkAnimLookup.RemoveAll();

            TMProLinkAnimLookup.AddHelper("pivot", new Fungus.TMProLinkAnimEffects.PivotEffect()
            {
                mode = Fungus.TMProLinkAnimEffects.TMPLinkAnimatorMode.PerCharacter,
                speed = animProperty.pivotSpeed,
                degScale = animProperty.degScale
            });
            TMProLinkAnimLookup.AddHelper("pulse", new Fungus.TMProLinkAnimEffects.PulseEffect()
            {
                mode = Fungus.TMProLinkAnimEffects.TMPLinkAnimatorMode.PerCharacter,
                speed = animProperty.pulseSpeed,
                HSVIntensityScale = animProperty.HSVIntensityScale,
                hueScale = animProperty.hueScale,
                saturationScale = animProperty.saturationScale,
                scale = animProperty.scale
            });
            TMProLinkAnimLookup.AddHelper("shake", new Fungus.TMProLinkAnimEffects.ShakeEffect()
            {
                mode = Fungus.TMProLinkAnimEffects.TMPLinkAnimatorMode.PerCharacter,
                offsetScale = animProperty.shakeOffsetScale,
                rotScale = animProperty.rotScale
            });
            TMProLinkAnimLookup.AddHelper("wave", new Fungus.TMProLinkAnimEffects.WaveEffect()
            {
                mode = Fungus.TMProLinkAnimEffects.TMPLinkAnimatorMode.PerCharacter,
                speed = animProperty.waveSpeed,
                indexStep = animProperty.indexStep,
                scale = animProperty.waveScale
            });
            TMProLinkAnimLookup.AddHelper("wiggle", new Fungus.TMProLinkAnimEffects.WiggleEffect()
            {
                mode = Fungus.TMProLinkAnimEffects.TMPLinkAnimatorMode.PerSection,
                offsetScale = animProperty.wiggleOffsetScale
            });
            
        }

       
    }

    public enum TEXT_ANIM_TYPE
    {
        pivot,
        pulse,
        shake,
        wave,
        wiggle
    }

    public enum TEXT_TRANSITION_ANIM
    {
        move,
        scale
    }

    public enum TEXT_MOVE_DIR
    {
        left,
        right,
        up,
        down
    }

    public enum DOSHAKE_TYPE
    {
        position,
        rotation,
        scale
    }

    [System.Serializable]
    public class AnimProperty
    {
        [Header("Pivot anim")]
        public float pivotSpeed = 10f;
        public float degScale = 15f;


        [Header("Pulse anim")]
        public float pulseSpeed = 3f;
        public float HSVIntensityScale = .15f;
        public float hueScale = 0f;
        public float saturationScale = 0f;
        public Vector3 scale = new Vector3(.05f, .05f, 0f);

        [Header("Shake anim")]
        public Vector2 shakeOffsetScale = Vector2.one * .5f;
        public float rotScale = 5f;

        [Header("Wave anim")]
        public float waveSpeed = .5f;
        public float indexStep = .3f;
        public float waveScale = 2f;

        [Header("Wiggle anim")]
        public Vector2 wiggleOffsetScale = Vector2.one * 5f;

    }

    [System.Serializable]
    public class TransitionInProperty
    {
        [Header("Shaking effect")]
        public bool useDoShakeIn;
        [MyBox.ConditionalField(nameof(useDoShakeIn), false)] public DOSHAKE_TYPE doshakeInType;
        [MyBox.ConditionalField(nameof(useDoShakeIn), false)] public float shakeInDuration = 2f;
        [MyBox.ConditionalField(nameof(useDoShakeIn), false)] public float shakeInStrength = 2.5f;

        public void SetShakeIn(Transform _textObject)
        {
            switch(doshakeInType)
            {
                case DOSHAKE_TYPE.position:
                    _textObject.DOShakePosition(shakeInDuration, Vector3.one * shakeInStrength);
                    break;
                case DOSHAKE_TYPE.rotation:
                    _textObject.DOShakeRotation(shakeInDuration, Vector3.one * shakeInStrength);
                    break;
                case DOSHAKE_TYPE.scale:
                    _textObject.DOShakeScale(shakeInDuration, Vector3.one * shakeInStrength);
                    break;
            }
        }

        [Header("Text Move Direction (Only set if Transition Anim Type is set to Move)")]
        public TEXT_MOVE_DIR textMoveInDir;
        public float textMoveInDuration = 2f;

        [Header("Text Scaling (Only set if Transition Anim Type is set to Scale)")]
        public float scalingInDuration = 1f;

        [Header("Fading In")]
        public float fadeInDuration = .8f;
    }

    [System.Serializable]
    public class TransitionOutProperty
    {
        [Header("Shaking effect")]
        public bool useDoShakeOut;
        [MyBox.ConditionalField(nameof(useDoShakeOut), false)] public DOSHAKE_TYPE doshakeOutType;
        [MyBox.ConditionalField(nameof(useDoShakeOut), false)] public float shakeOutDuration = 2f;
        [MyBox.ConditionalField(nameof(useDoShakeOut), false)] public float shakeOutStrength = 2.5f;

        public void SetShakeOut(Transform _subtitleCanvas)
        {
            switch (doshakeOutType)
            {
                case DOSHAKE_TYPE.position:
                    _subtitleCanvas.DOShakePosition(shakeOutDuration, Vector3.one * shakeOutStrength);
                    break;
                case DOSHAKE_TYPE.rotation:
                    _subtitleCanvas.DOShakeRotation(shakeOutDuration, Vector3.one * shakeOutStrength);
                    break;
                case DOSHAKE_TYPE.scale:
                    _subtitleCanvas.DOShakeScale(shakeOutDuration, Vector3.one * shakeOutStrength);
                    break;
            }
        }

        [Header("Text Move Direction (Only set if Transition Anim Type is set to Move)")]
        public TEXT_MOVE_DIR textMoveOutDir;
        public float textMoveOutDuration = 2f;

        [Header("Text Scaling (Only set if Transition Anim Type is set to Scale)")]
        public float scalingOutDuration = 1f;

        [Header("Fading Out")]
        public float fadeOutDuration = .8f;
    }
}
