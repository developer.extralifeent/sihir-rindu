using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.UI;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ",
            "Cutscene Border",
            "Show/Hide Cutscene Border")]
    [AddComponentMenu("")]
    public class CommandShowCutsceneBorder : Command {
        public enum ShowType {Show,Hide}
        [SerializeField] ShowType showType;
        public override void OnEnter() {
            if (UIManager.Instance) {
                if (showType == ShowType.Show) {
                    UIManager.Instance.ShowCutsceneBorder();
                } else {
                    UIManager.Instance.HideCutsceneBorder();
                }
            }
            Continue();
        }
    }
}

