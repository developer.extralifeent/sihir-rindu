using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Spine;
using Fungus;
using MyBox;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                 "OLJPlayVideo",
                 "PlayVideo from video or Spine animation.")]
    [AddComponentMenu("")]
    public class CommandOLJPlayVideo : Command {
        [SerializeField] VideoClip clip;
        [Range(0, 1)]
        [SerializeField] float volume = 1;
        [Range(0, 5)]
        [SerializeField] float playbackTime = 1;
        [SerializeField] bool mute;
        [SerializeField] bool waitFinished = true;
        public override void OnEnter() {
            base.OnEnter();
            if (OLJ.UI.UIManager.Instance) {
                OLJ.UI.UIManager.Instance.PlayVideo(clip, volume, playbackTime, mute);
            }
            if (waitFinished) {
                StartCoroutine(WaitClip());
            } else {

                Continue();
            }
            
        }
        IEnumerator WaitClip() {
            yield return new WaitForSeconds((float)clip.length);
            OLJ.UI.UIManager.Instance.StopVideo();
            Continue();
        }
    }
}

