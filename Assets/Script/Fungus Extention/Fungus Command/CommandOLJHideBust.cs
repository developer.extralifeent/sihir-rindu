using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.UI;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                 "Hide Bust",
                 "Hide Character Bust")]
    [AddComponentMenu("")]
    public class CommandOLJHideBust : Command {
        public override void OnEnter() {
            base.OnEnter();
            if (UIManager.Instance) {
                UIManager.Instance.HideBustImage();
            }
            Continue();
        }
    }
}

