using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using Cinemachine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                  "OLJLoadScene",
                  "Load Scene")]
    [AddComponentMenu("")]
    public class CommandLoadScene : Command
    {
        [SerializeField] string sceneName;

        public override void OnEnter()
        {
            if (OLJ.Manager.GameManager.Instance)
            {
                OLJ.Manager.GameManager.Instance.LoadScene(sceneName);
                if(sceneName == "MainMenu")
                    OLJ.Manager.GameManager.Instance.UnloadScene("General Gameplay");
            }
            else
            {
                Debug.LogError("No Game Manager Instance Found");
            }
        }
    }
}
