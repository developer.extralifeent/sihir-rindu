using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                "Sucka-Sucka Spawner",
                "Spawn Sucka-Sucka")]
    public class CommandSpawnSuckaSucka : Command
    {
        private SpawnerManager spawnerManager;

        private void Awake()
        {
            spawnerManager = SpawnerManager.Instance;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            spawnerManager.SpawnSuckaSucka();
            Continue();
        }
    }
}
