using Fungus;
using OLJ.UI;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Cutscene",
                   "Play UI Spine Animation",
                   "Play UI Spine Animation")]
    public class CommandShowUISpineAnim : Command
    {
        [SerializeField] private SpineAnimationData skeletonAnim;
        [SerializeField] private float spineAnimDuration = .8f;
        [SerializeField] private bool waitForAnimation = true;

        public override void OnEnter()
        {
            base.OnEnter();
            StartCoroutine(SetSpineUIAnim());
        }

        private IEnumerator SetSpineUIAnim()
        {
            QTEUIManager.Instance.SetQTEAnimation(skeletonAnim.sk, skeletonAnim.animName);

            if (waitForAnimation)
            {
                yield return new WaitForSeconds(spineAnimDuration);
                QTEUIManager.Instance.HideQTEAnimation();
                Continue();
            }
            else
            {
                StartCoroutine(HideAfterDelay());
                Continue();
            }
        }

        private IEnumerator HideAfterDelay()
        {
            yield return new WaitForSeconds(spineAnimDuration);
            QTEUIManager.Instance.HideQTEAnimation();
        }
    }
}
