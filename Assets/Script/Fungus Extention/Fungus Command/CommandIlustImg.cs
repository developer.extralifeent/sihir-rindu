using DG.Tweening;
using Fungus;
using UnityEngine;
using UnityEngine.UI;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/illustration",
                "Show or Hide Image Illustration",
                "Show Illustration On Screen")]
    public class CommandIlustImg : Command
    {
        [SerializeField] private bool showImage = true;

        [SerializeField] private Image canvasImage;
        [SerializeField] private Sprite ilustImage;

        [SerializeField] private float imgFadeDuration = 1.5f;

        public override void OnEnter()
        {
            base.OnEnter();
            canvasImage.sprite = ilustImage;

            FadeIllustImg();

            Continue();
        }

        private void FadeIllustImg()
        {
            canvasImage.color = new Color(canvasImage.color.r, canvasImage.color.g, canvasImage.color.b, showImage ? 0f : 1f);

            if(showImage)
                canvasImage.DOFade(1f, imgFadeDuration);
            else
                canvasImage.DOFade(0f, imgFadeDuration);
        }
    }
}   
