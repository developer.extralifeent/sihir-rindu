using Fungus;
using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ/Cutscene",
                   "Change Spine Anim Skin",
                   "Change Spine Anim Skin")]
    public class CommandChangeSpineAnimSkin : Command
    {
        [SerializeField] private SkeletonAnimation skeletonAnim;
        [SerializeField, SpineSkin(dataField = "skeletonAnim")] private string skinName;

        public override void OnEnter()
        {
            base.OnEnter();

            Skeleton skeleton = skeletonAnim.Skeleton;

            skeleton.SetSkin(skinName);
            skeleton.SetToSetupPose();
            skeletonAnim.AnimationState.Apply(skeleton);

            Continue();
        }
    }
}
