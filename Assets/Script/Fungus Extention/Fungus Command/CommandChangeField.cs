using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.Manager;
using OLJ.UI;
using OLJ.Field;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ",
                   "Load Field",
                   "Load New Field")]
    [AddComponentMenu("")]
    public class CommandChangeField : Command {
        [SerializeField] FieldSO field;
        public override void OnEnter() {
            base.OnEnter();
            if (FieldManager.Instance&&UIManager.Instance) {
                UIManager.Instance.PlayTransition(() => {
                    if (StageControl.Instance.StageSO.isFinalPart)
                    {
                        GameManager.Instance.IsPartialChapter = true;
                        GameManager.Instance.LoadMainMenu();
                    }
                    else
                        FieldManager.Instance.LoadFieldPrefab(field);
                });
            }
            Continue();
        }
        public override string GetSummary() {

            string soName;
            string prefabName;
            if (field) {
                soName = field.name;
                if (field.fieldPrefab) {
                    prefabName = field.fieldPrefab.name;
                } else {
                    prefabName = "No Prefab Assign";
                }
            } else {
                return "No FieldSO";
            }
            return "SO: " + soName + " Name: " + prefabName;
        }
    }
}

