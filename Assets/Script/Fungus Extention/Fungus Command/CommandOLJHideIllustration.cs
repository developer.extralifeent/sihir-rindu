using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using OLJ.UI;

namespace OLJ.FungusExtention {
    [CommandInfo("OLJ/Cutscene",
                "OLJHideIllustration",
                "Hide Illustration")]
    [AddComponentMenu("")]
    public class CommandOLJHideIllustration : Command {
        public override void OnEnter() {
            base.OnEnter();
            UIManager.Instance.HideIllustration();
            Continue();
        }
    }
}
   
