using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Manager;
using Fungus;

namespace OLJ.FungusExtention
{
    [CommandInfo("OLJ",
                 "Translate Position",
                 "TranslatePosition")]
    [AddComponentMenu("")]
    public class CommandOLJTranslatePosition : Command
    {
        [SerializeField] GameObject objectToMove;
        [SerializeField] Transform targetPosition;
        public override void OnEnter()
        {
            base.OnEnter();
            if (objectToMove)
                objectToMove.transform.position = targetPosition.position;
            Continue();
        }
    }

}

