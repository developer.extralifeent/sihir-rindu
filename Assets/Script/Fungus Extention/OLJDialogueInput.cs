using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Fungus;
using OLJ.Manager;

namespace OLJ.FungusExtention {
    public class OLJDialogueInput : DialogInput {
        CharacterInput inputAction;
        bool isSubscribeToInputAction = false;
        private void Start() {
            if (ControlManager.Instance) {
                inputAction = ControlManager.Instance.CharacterInputAction;
            }
        }
        public override void SetInput() {

            if (!isSubscribeToInputAction) {
                inputAction.UI.Enable();
                inputAction.UI.Submit.performed += NextLineExecution;
                isSubscribeToInputAction = true;
            }
        }
        void NextLineExecution(InputAction.CallbackContext context) {
            GetComponent<SayDialog>().Stop();
            SetClickAnywhereClickedFlag();
        }
      
    }
}

