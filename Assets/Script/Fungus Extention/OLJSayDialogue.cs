using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

namespace OLJ.FungusExtention {
    public class OLJSayDialogue : SayDialog {
        public enum PortraitFacing { 
        left,
        right
        }
        [SerializeField] PortraitFacing portraitFace;
        public override void SetCharacterImage(Sprite image) {
          base.SetCharacterImage(image);
        }
    }
}

