using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualCameraControll : MonoBehaviour
{
    public GameObject Camera;
    void Update()
    {
        if (Input.GetKeyDown (KeyCode.J))
        {
            Camera.SetActive(false);
        }
        if (Input.GetKeyDown (KeyCode.K))
        {
            Camera.SetActive(true);
        }
    }
}
