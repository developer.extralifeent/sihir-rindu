using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Character;

public class AreaControl : MonoBehaviour
{
    //Respawn Area
    [SerializeField] MonsterController UsedMonster;
    [SerializeField] MonsterController RespawnedEnemy;
    [SerializeField] Transform MonsterRespawn;
    [SerializeField] float TimeRespawn;
    // Position To Patrol
    [SerializeField] Transform Position1, Position2;
    // Teleport Portal
    [SerializeField] GameObject Portal;
    [SerializeField] Vector3 offset;
    private void Start() {        
        UsedMonster.OnDestroyed += StartRespawningMonster;       
    }
    public void StartRespawningMonster() {
      
        Debug.Log("Start Respawning");
        StartCoroutine(RespawnMonster());
    }
   
    private void OnDisable() {
        UsedMonster.OnDestroyed -= StartRespawningMonster;        
    }
    IEnumerator RespawnMonster() {       
        yield return new WaitForSeconds(TimeRespawn);
        GameObject pt = Instantiate(Portal, MonsterRespawn.position + offset, Quaternion.identity);       
        Debug.Log("Respawn Monster finished");
        if (UsedMonster == null)
        {
            MonsterController monster = Instantiate(RespawnedEnemy, MonsterRespawn.position, Quaternion.identity);
            monster.GetComponent<MonsterController>().Patrolpos1 = Position1;
            monster.GetComponent<MonsterController>().Patrolpos2 = Position2;
            UsedMonster = monster;
            UsedMonster.OnDestroyed += StartRespawningMonster;
            pt.GetComponentInChildren<Animator>().Play("vfx-portal-end");
            Destroy(pt, 0.5f);
        }


    }
}
