using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Character;
using MyBox;
using DG.Tweening;
using System;

public class SpikeController : MonoBehaviour {
    [SerializeField] float delayTime = 3f;
    [SerializeField] float force = 2f;
    [SerializeField] TRAP_TYPE trapType;
    [SerializeField] private float damage = 10f;
    [SerializeField, ConditionalField(nameof(trapType), false, TRAP_TYPE.Vine)] private string vineSlapAnimName = "VineSlapAnim";
    [SerializeField, ConditionalField(nameof(trapType), false, TRAP_TYPE.Vine)] private SpriteRenderer slapIndicator;
    [SerializeField, ConditionalField(nameof(trapType), false, TRAP_TYPE.Vine)] private Collider hitBoxCollider;
    private int slapAnimHash;

    [SerializeField] TRAP_ORIENTATION trapOrientation;

    public Action onPlayerHit;

    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<CharacterBody>()) {
            CharacterBody cb = other.GetComponent<CharacterBody>();
            CheckTrapOrientation(cb);
            
            PlayerHp playerHp = other.GetComponent<PlayerHp>();
            playerHp.TakeDamage(damage);
            //Debug.Log("Knockback Value: " + knockBackDistance);
        }
    }

    private void CheckTrapOrientation(CharacterBody cb)
    {
        Vector3 knockBackDirection;
        switch (trapOrientation)
        {
            case TRAP_ORIENTATION.Horizontal:
                if (cb.LastInput.x != 0)
                {
                    knockBackDirection = Vector3.forward;
                    cb.Knockback(knockBackDirection, force);
                }
                else if (cb.LastInput.z != 0)
                {
                    knockBackDirection = -cb.LastInput;
                    cb.Knockback(knockBackDirection, force);
                }
                break;

            case TRAP_ORIENTATION.Vertical:
                if (cb.LastInput.z != 0)
                {
                    knockBackDirection = -Vector3.right;
                    cb.Knockback(knockBackDirection, force);
                }
                else if (cb.LastInput.x != 0)
                {
                    knockBackDirection = -cb.LastInput;
                    cb.Knockback(knockBackDirection, force);
                }
                break;
        }
    }

    public void Start()
    {
        StartCoroutine(ActivateTrapCoroutine());
        slapAnimHash = Animator.StringToHash(vineSlapAnimName);
    }

    IEnumerator ActivateTrapCoroutine()
    {
        switch (trapType)
        {
            case TRAP_TYPE.Spike:
                bool nyala = true;
                while (true)
                {
                    yield return new WaitForSeconds(delayTime);
                    GetComponent<BoxCollider>().enabled = nyala;
                    transform.Find("Visual").gameObject.SetActive(nyala);
                    nyala = !nyala;
                }
            case TRAP_TYPE.Vine:
                Animator anim = this.GetComponentInChildren<Animator>();
                while (true)
                {
                    yield return new WaitForSeconds(delayTime / 2);
                    slapIndicator.DOFade(1f, .5f);

                    yield return new WaitForSeconds(delayTime / 2);
                    anim.Play(slapAnimHash);

                    yield return new WaitForSeconds(.7f);
                    hitBoxCollider.enabled = true;
                    slapIndicator.DOFade(0f, .5f);

                    yield return new WaitForSeconds(.3f);
                    hitBoxCollider.enabled = false;
                }
        }
    }
}

public enum TRAP_TYPE
{
    Spike,
    Vine
}

public enum TRAP_ORIENTATION
{
    Horizontal,
    Vertical
}
