using OLJ.Character;
using OLJ.Gameplay.Interactable;
using System.Collections;
using UnityEngine;

namespace OLJ.Gameplay
{
    public class MovingPlatformController : MonoBehaviour
    {
        [Header("Points")]
        [SerializeField] private Transform[] points;

        [Header("Values")]
        [SerializeField] private float movingSpeed;
        [SerializeField] private float minDistance;
        private bool isPlayerInside;

        [Header("Moving delay")]
        [SerializeField] private float movingDelay;

        [Header("Interacts")]
        [SerializeField] private JumpInteract[] jumpInInteracts;
        [SerializeField] private JumpInteract[] jumpOutInteracts;

        private int pointIndex = 0;

        private void Start()
        {
            StartCoroutine(MovePlatformCoroutine());
        }

        private IEnumerator MovePlatformCoroutine()
        {
            while (true)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, points[pointIndex].position, movingSpeed * Time.deltaTime);
                yield return null;

                if (isPlayerInside)
                {
                    ToggleJumpInInteract(false);
                    ToggleJumpOutInteract(true);
                }
                else
                {
                    ToggleJumpInInteract(true);
                    ToggleJumpOutInteract(false);
                }

                if (Vector3.Distance(this.transform.position, points[pointIndex].position) <= minDistance)
                {
                    pointIndex++;
                    if (pointIndex >= points.Length)
                        pointIndex = 0;

                    yield return new WaitForSeconds(movingDelay);
                }
            }
        }

        private void ToggleJumpInInteract(bool _isActive)
        {
            foreach (var point in jumpInInteracts)
            {
                point.gameObject.SetActive(_isActive);
            }
        }

        private void ToggleJumpOutInteract(bool _isActive)
        {
            foreach (var point in jumpOutInteracts)
            {
                point.gameObject.SetActive(_isActive);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.TryGetComponent(out PlayerController _charController))
            {
                _charController.transform.SetParent(this.transform);
                isPlayerInside = true;
                Debug.Log("Player Enter Platform");
            }
        }


        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(out PlayerController _charController))
            {
                _charController.transform.SetParent(null);
                isPlayerInside = false;
                Debug.Log("Player Exit Platform");
            }
        }
    }
}
