using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Gameplay.Interactable {
    public class ControlBehaviour : MonoBehaviour {
        public void Enter() {
            this.gameObject.SetActive(false);
        }
        public void Exit() {
            this.gameObject.SetActive(true);
        }
    }
}

