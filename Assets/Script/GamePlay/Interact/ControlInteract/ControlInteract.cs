using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using OLJ.Character;
using OLJ.Manager;

namespace OLJ.Gameplay.Interactable {
    public class ControlInteract : InteractableBehaviour {
        public class ControlInteractEvent : UnityEvent<float,float> { }
        public ControlInteractEvent onPressedButton = new ControlInteractEvent();
        public ControlInteractEvent onReleaseButton = new ControlInteractEvent();
        [SerializeField] float holdDuration;
        float tempDuration;
        bool isHolding;
        CharacterBody currentController;
        private void Update() {
            if (isHolding) {
                Debug.Log("Hold Duration: " + tempDuration);
                tempDuration += Time.deltaTime;
                onPressedButton?.Invoke(tempDuration, holdDuration);
                if (tempDuration >= holdDuration) {
                    HoldResult(currentController);
                }
            } else {
                tempDuration = 0;
                onReleaseButton?.Invoke(tempDuration, holdDuration);
            }
        }
        //public override void ShowIcon(CharacterBody bodyType) {
        //    if (DirectionCalculation(bodyType)) {
        //        base.ShowIcon(bodyType);
        //    }
        //}
        protected override void StartInteractAction(CharacterBody bodyType) {
            if (bodyType.GetComponent<ControlBehaviour>()) {
                this.gameObject.layer = bodyType.gameObject.layer;
                SetUp(bodyType);
                InputConfig(bodyType);
                base.StartInteractAction(bodyType);
                //if (DirectionCalculation(bodyType)) {
                //    SetUp(bodyType);
                //    InputConfig(bodyType);
                //    base.InteractStartAction(bodyType);                    
                //}               
            }
        }      
        public void HoldResult(CharacterBody bodyType) {
            InteractEndAction(bodyType);
            bodyType.GetComponent<ControlBehaviour>().Exit();
            bodyType.transform.position = this.transform.position;
            ControlManager.Instance.CurrentBody = bodyType;
            ControlManager.Instance.CharacterInputAction.Player.Additional.Disable();
            GetComponent<CharacterBody>().Dead();
            Debug.Log("Get Out");
        }
        void InputConfig(CharacterBody bodyType) {
            /*ControlManager.Instance.CharacterInputAction.Player.Additional.Enable();
            ControlManager.Instance.CharacterInputAction.Player.Additional.started += HoldingStart;
            ControlManager.Instance.CharacterInputAction.Player.Additional.canceled += HoldingStop;*/
        }
        void SetUp(CharacterBody bodyType) {
            currentController = bodyType;
            bodyType.GetComponent<ControlBehaviour>().Enter();
            CharacterBody thisBody = GetComponent<CharacterBody>();
            ControlManager.Instance.CurrentBody = thisBody;
            GetComponent<AIController>().MonsterState = AIController.Behaviour.Controlled;
        }        
        void HoldingStart(InputAction.CallbackContext context) {
            isHolding = true;
        }
        void HoldingStop(InputAction.CallbackContext context) {           
            isHolding = false;
        }
        //bool DirectionCalculation(CharacterBody bodyType) {
        //    Vector3 otherBodyDirection = bodyType.GetComponent<CharacterAnimation>().AnimeDirection;
        //    Vector3 thisBodyDirection = gameObject.GetComponent<CharacterAnimation>().AnimeDirection;
        //    Vector3 resultCalc = otherBodyDirection + thisBodyDirection;
        //    if (Mathf.Abs(resultCalc.x) > 1 || Mathf.Abs(resultCalc.z) > 1) {
        //        Debug.Log("Can Enter");
        //        return true;
        //    } else {
        //        Debug.Log("No Enter");
        //        return false;
        //    }
        //}
    }
}



