using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Gameplay.Interactable {
    public class BushInteract : InteractableBehaviour {
        [SerializeField] Animator anim;
        [SerializeField] string animationState;
        protected override void StartInteractAction(CharacterBody bodyType) {
            base.StartInteractAction(bodyType);
            if (anim) {
                anim.Play(animationState);
            } else {
                Debug.LogWarning("Bush interact: No animation has been assigned");
            }
        }
        public override void InteractEndAction(CharacterBody bodyType) {
            base.InteractEndAction(bodyType);
        }
        private void OnTriggerEnter(Collider other) {
            if (other.GetComponent<PlayerController>()) {
                StartInteractAction(other.GetComponent<PlayerController>());
            }
        }

        public void DisableBushInteraction()
        {
            GetComponent<BoxCollider>().enabled = false;
        }
    }
}

