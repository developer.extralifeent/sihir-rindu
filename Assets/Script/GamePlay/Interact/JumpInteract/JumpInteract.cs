using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Character;
using DG.Tweening;
using MyBox;

namespace OLJ.Gameplay.Interactable {
    public class JumpInteract : InteractableBehaviour {

        [SerializeField] private INTERACT_TYPE interactType;

        public INTERACT_TYPE InteractType => interactType;

        [SerializeField] Transform pointA;
        [SerializeField] Transform pointB;
        [SerializeField, ConditionalField(nameof(interactType), false, INTERACT_TYPE.highPlatform)] private Transform pointC;
        [SerializeField, ConditionalField(nameof(interactType), false, INTERACT_TYPE.highPlatform)] private float secondJumpDuration;

        [SerializeField] Transform indicator;
        [SerializeField] float indicatorUp;
        Transform endPos;

        private void Start()
        {
            StartCoroutine(IndicatorBillboard());
        }

        protected override void StartInteractAction(CharacterBody bodyType) {
            base.StartInteractAction(bodyType);
            bodyType.DisableControl();
            bodyType.GetComponent<CharacterAnimation>().SetAnimDirection(bodyType.LastInput, 0);
            if (Vector3.Distance(bodyType.transform.position, pointA.position) > Vector3.Distance(bodyType.transform.position, pointB.position)) {
                endPos = pointA;
                //indicator.position = pointA.position + Vector3.up * 2;
            } else {
                endPos = pointB;
            }
            if (bodyType.GetComponent<JumpBehaviour>())
            {
                JumpBehaviour pc = bodyType.GetComponent<JumpBehaviour>();
                CheckJumpInteractType(pc, bodyType);
            }
        }

        private void CheckJumpInteractType(JumpBehaviour _jumpBehaviour, CharacterBody _body)
        {
            switch(interactType)
            {
                case INTERACT_TYPE.jump:
                    _jumpBehaviour.Jump(endPos.position, interactDuration, _body.LastInput, false);
                    break;
                case INTERACT_TYPE.highPlatform:
                    _jumpBehaviour.ClimbHighPlatform(endPos.position, pointC.position,interactDuration, secondJumpDuration,_body.LastInput);
                    break;
                case INTERACT_TYPE.midPlatform:
                    _jumpBehaviour.Jump(endPos.position, interactDuration, _body.LastInput, false);
                    break;
                case INTERACT_TYPE.vaultPlatform:
                    _jumpBehaviour.Jump(endPos.position, interactDuration, _body.LastInput, true);
                    break;
            }
        }

        public override void ShowIcon(CharacterBody bodyType) {
            indicator.gameObject.SetActive(true);
            if (Vector3.Distance(bodyType.transform.position, pointA.position) > Vector3.Distance(bodyType.transform.position, pointB.position)) {
                endPos = pointA;                
            } else {
                endPos = pointB;
            }
            //indicator.position = endPos.position + Vector3.up * indicatorUp;
            //indicator.DOMove(indicator.transform.position + Vector3.up * 1, 1).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        }
        public override void HideIcon(CharacterBody bodyType) {
            indicator.gameObject.SetActive(false);
        }
        public override void InteractEndAction(CharacterBody bodyType) {
            base.InteractEndAction(bodyType);
            bodyType.EnableControl();
        }
        
        private IEnumerator IndicatorBillboard()
        {
            while (true)
            {
                indicator.LookAt(Camera.main.transform.position);
                yield return null;
            }
        }
    }

    public enum INTERACT_TYPE
    {
        jump,
        highPlatform,
        midPlatform,
        vaultPlatform
    }
}

