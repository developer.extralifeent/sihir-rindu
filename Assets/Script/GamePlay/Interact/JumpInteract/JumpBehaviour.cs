using System.Collections; 
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using OLJ.Character;
using MyBox;
using System;

namespace OLJ.Gameplay.Interactable {
    public class JumpBehaviour : MonoBehaviour {
        [Separator("Class References")]
        [SerializeField] private PlayerController player;
        private CapsuleCollider playerCol;
        private Animator animator;
        private Animator maskingAnimator;

        [Separator("----Jumping Behaviour----")]
        [SerializeField] float jumpHeight = 5f;
        [SerializeField] Ease jumpType;

        [Separator("----Climbing Behaviour----")]
        //[SerializeField] private float climbHeight = 10f;
        [SerializeField] private Ease climbType = Ease.OutBack;
        [SerializeField] private Ease secondClimbType = Ease.Linear;

        [Separator("----Ground Checker----")]
        [SerializeField] private Transform groundCheckTransform;
        [SerializeField] private float groundCheckRadius;

        private void Start()
        {
            playerCol = GetComponent<CapsuleCollider>();
        }

        #region Jump
        public void Jump(Vector3 endPosition, float _jumpDuration, Vector3 direction, bool _isVaulting) {
            //jumpTween = transform.DOJump(endPosition, jumpHeight, 1, jumpDuration);
            //GetComponent<Collider>().enabled = false;
            //jumpDuration = _jumpDuration;

            CheckLilyPad();

            transform.DOMove(endPosition, _jumpDuration).OnComplete(() => OnJumpComplete(_jumpDuration, _isVaulting));
            Debug.Log("Player Jump");
            GetComponent<CharacterBody>().DisableControl();
            animator = player.Anime;
            animator.transform.DOMoveY(animator.transform.position.y + jumpHeight, _jumpDuration / 2f).SetEase(jumpType).SetLoops(2, LoopType.Yoyo).OnComplete(() => OnJumpComplete(_jumpDuration, _isVaulting));

            maskingAnimator = player.MaskingAnim;
            if(maskingAnimator)
                maskingAnimator.transform.DOMoveY(animator.transform.position.y + jumpHeight, _jumpDuration / 2f).SetEase(jumpType).SetLoops(2, LoopType.Yoyo).OnComplete(() => OnJumpComplete(_jumpDuration, _isVaulting));

            //jumpTween.SetEase(jumpType).OnComplete(OnJumpComplete);
            SetAnimVisual(direction);

            if (!_isVaulting)
            {
                animator.SetTrigger("JumpStart");
                maskingAnimator.SetTrigger("JumpStart");
            }
            else
                Debug.Log("Player Vaulting");
        }
        void OnJumpComplete(float _jumpDuration, bool _isVaulting)
        {
            Debug.Log("Jump complete");
            animator.transform.DOLocalMoveY(-2f, _jumpDuration / 2f);
            maskingAnimator.transform.DOLocalMoveY(-2f, _jumpDuration / 2f);

            CheckLilyPad();

            if (!_isVaulting)
            {
                player.Anime.SetTrigger("JumpEnd");
                maskingAnimator.SetTrigger("JumpEnd");
            }
            else
                Debug.Log("Player Vaulting");
            
            this.GetComponent<CharacterBody>().EnableControl();
            this.GetComponent<Collider>().enabled = true;
            //animator.transform.DOMoveY(animator.transform.position.y - jumpHeight, jumpDuration / 2f).OnComplete(() => { GetComponent<Collider>().enabled = true; });
        }

        private void CheckLilyPad()
        {
            Collider[] colliders = Physics.OverlapSphere(groundCheckTransform.position, groundCheckRadius);

            foreach(Collider col in colliders)
            {
                if(col.TryGetComponent(out InstantiateObject _instantiatedObj))
                {
                    _instantiatedObj.OnLilyPadStepped(this.transform);
                    Debug.Log("Lily Pad Stepped");
                }
                else
                {
                    Debug.Log("No Lily Pad Detected");
                }
            }
        }
        #endregion

        #region Climbing High Platform
        public void ClimbHighPlatform(Vector3 point1, Vector3 point2, float _firstClimbDuration, float _secondClimbDuration,Vector3 direction)
        {
            //jumpTween = transform.DOJump(endPosition, jumpHeight, 1, jumpDuration);
            playerCol.enabled = false;
            //jumpDuration = _jumpDuration;
            Vector3 midPos = new Vector3(point1.x, point1.y - playerCol.center.y, point1.z);
            transform.DOMove(midPos, _firstClimbDuration).OnComplete(() => StartCoroutine(FirstClimbCoroutine(point2, _secondClimbDuration, direction)));
            Debug.Log("Player First Climb");
            GetComponent<CharacterBody>().DisableControl();
            animator = player.Anime;
            animator.transform.DOMoveY(playerCol.bounds.min.y, _firstClimbDuration).SetEase(climbType).OnComplete(() => StartCoroutine(FirstClimbCoroutine(point2, _secondClimbDuration, direction)));
            //jumpTween.SetEase(jumpType).OnComplete(OnJumpComplete);
            SetAnimVisual(direction);
            //player.Anime.SetTrigger("ClimbHighStart");
        }

        public void OnFirstClimbComplete(Vector3 point2, float _secondClimbDuration, Vector3 _direction)
        {
            Vector3 finalPos = new Vector3(point2.x, point2.y + playerCol.height / 2f, point2.z); 
            transform.DOMove(finalPos, _secondClimbDuration).OnComplete(OnClimbComplete);
            animator = player.Anime;
            //player.Anime.SetTrigger("SecondClimbHigh");
            SetAnimVisual(_direction);

            animator.transform.DOLocalMoveY(-2f, _secondClimbDuration).SetEase(secondClimbType).OnComplete(OnClimbComplete);

            Debug.Log("Player Second Climb");
        }

        private IEnumerator FirstClimbCoroutine(Vector3 point2, float _secondClimbDuration, Vector3 _direction)
        {
            yield return new WaitForSeconds(1f);
            OnFirstClimbComplete(point2, _secondClimbDuration, _direction);
        }
        #endregion

        #region Climbing mid platform
        public void ClimbMidPlatform(Vector3 point1, Vector3 point2, float _firstClimbDuration, float _secondClimbDuration, Vector3 direction)
        {
            //jumpTween = transform.DOJump(endPosition, jumpHeight, 1, jumpDuration);
            playerCol.enabled = false;
            //jumpDuration = _jumpDuration;
            Vector3 midPos = new Vector3(point1.x, point1.y - playerCol.center.y, point1.z);
            transform.DOMove(midPos, _firstClimbDuration).OnComplete(() => StartCoroutine(FirstMidClimbCoroutine(point2, _secondClimbDuration, direction)));
            Debug.Log("Player First Climb");
            GetComponent<CharacterBody>().DisableControl();
            animator = player.Anime;
            animator.transform.DOMoveY(playerCol.bounds.min.y, _firstClimbDuration).SetEase(climbType).OnComplete(() => StartCoroutine(FirstMidClimbCoroutine(point2, _secondClimbDuration, direction)));
            //jumpTween.SetEase(jumpType).OnComplete(OnJumpComplete);
            SetAnimVisual(direction);
            //player.Anime.SetTrigger("ClimbHighStart");
        }

        public void OnFirsMidtClimbComplete(Vector3 point2, float _secondClimbDuration, Vector3 _direction)
        {
            Vector3 finalPos = new Vector3(point2.x, point2.y + playerCol.height / 2f, point2.z);
            transform.DOMove(finalPos, _secondClimbDuration).OnComplete(OnClimbComplete);
            animator = player.Anime;
            //player.Anime.SetTrigger("SecondClimbHigh");
            SetAnimVisual(_direction);

            animator.transform.DOLocalMoveY(-2f, _secondClimbDuration).SetEase(secondClimbType).OnComplete(OnClimbComplete);

            Debug.Log("Player Second Climb");
        }

        private IEnumerator FirstMidClimbCoroutine(Vector3 point2, float _secondClimbDuration, Vector3 _direction)
        {
            yield return new WaitForSeconds(1f);
            OnFirsMidtClimbComplete(point2, _secondClimbDuration, _direction);
        }
        #endregion

        private void OnClimbComplete()
        {
            Debug.Log("Player Last Climb");
            //player.Anime.SetTrigger("ClimbHighEnd");

            //Vector3 finalStopPos = new Vector3(this.transform.position.x, this.transform.position.y - 2.5f, this.transform.position.z);
            //player.Anime.transform.DOMoveY(this.transform.position, .1f);
            this.GetComponent<CharacterBody>().EnableControl();
            playerCol.enabled = true;
        }
        void SetAnimVisual(Vector3 direction) {
            CharacterAnimation charAnim = transform.GetComponent<CharacterAnimation>();

            Animator charMainAnim = charAnim.Anime;
            charMainAnim.SetFloat("Vertical", direction.z);
            charMainAnim.SetFloat("Horizontal", direction.x);
            charMainAnim.SetFloat("Speed", 0);

            if (this.transform.GetComponent<CharacterAnimation>().MaskingAnim)
            {
                Animator charMaskingAnim = charAnim.MaskingAnim;
                charMaskingAnim.SetFloat("Vertical", direction.z);
                charMaskingAnim.SetFloat("Horizontal", direction.x);
                charMaskingAnim.SetFloat("Speed", 0);
            }
        }
    }
}

