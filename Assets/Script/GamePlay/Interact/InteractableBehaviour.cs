using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using OLJ.Character;
using OLJ.UI;
using System;
using UnityEngine.UI;
using DG.Tweening;
using MyBox;

namespace OLJ.Gameplay.Interactable {
    public class InteractableBehaviour : MonoBehaviour, Iinteractable {
        [SerializeField] protected float interactDuration;
        //[SerializeField] protected int interactIndex;

        public class InteractIcon : UnityEvent { }
        public InteractIcon onShowIcon = new InteractIcon();
        public InteractIcon onHideIcon = new InteractIcon();
        [System.Serializable] public class InteractEvent : UnityEvent { }    
        public InteractEvent onInteractStart;
        public InteractEvent onInteractEnd;

        [Separator("Indicator")]
        [SerializeField] protected Image[] eyesIndicator;
        [SerializeField] protected float eyesIndicatorfadeDuration = .5f;


        public Image[] EyesIndicator => eyesIndicator;

        public Action<CharacterBody> onIconShowing;
        public Action<CharacterBody> onIconHiding;

        public virtual void ShowIcon(CharacterBody bodyType) {
            //onShowIcon?.Invoke();
            onIconShowing?.Invoke(bodyType);
        }
        public virtual void HideIcon(CharacterBody bodyType) {
            //onHideIcon?.Invoke();
            onIconHiding?.Invoke(bodyType);
        }
        public void InteractStart(CharacterBody bodyType) {
            StartInteractAction(bodyType);
            Debug.Log("Interact: Start");
        }        
        protected virtual void StartInteractAction(CharacterBody bodyType) {
            bodyType.IsInteracting = true;
            HideIcon(bodyType);
            if (interactDuration >= 0) {
                StartCoroutine(InteractDurationCour(bodyType));
                Debug.Log("Interact: Duration Start Counting");
            }
            onInteractStart?.Invoke();
        }
        public virtual void InteractEndAction(CharacterBody bodyType) {
            bodyType.IsInteracting = false;
            onInteractEnd?.Invoke();
            Debug.Log("Interact: End");
        }
        
        IEnumerator InteractDurationCour(CharacterBody bodyType) {
            yield return new WaitForSeconds(interactDuration);
            Debug.Log("Interact: Duration End Counting");
            InteractEndAction(bodyType);
        }

        public void CheckPlayerDistance(CharacterBody _bodyType, float _detectionRadius, float _indicatorValue)
        {
            float distance = Vector3.Distance(this.transform.position, _bodyType.transform.position);
            if (distance <= _detectionRadius && !_bodyType.IsItemDetected)
            {
                ToggleEyeIndicator(1f);
            }
            else
            {
                ToggleEyeIndicator(0f);
            }
        }

        private void ToggleEyeIndicator(float _indicatorFadeValue)
        {
            for (int i = 0; i < EyesIndicator.Length; i++)
            {
                EyesIndicator[i].DOFade(_indicatorFadeValue, eyesIndicatorfadeDuration);
            }
        }
    }
    public interface Iinteractable {
        public void InteractStart(CharacterBody bodyType);
    }
}