using OLJ.Character;
using OLJ.Manager;
using OLJ.Gameplay.Interactable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.UI;
using MyBox;
using Spine.Unity;

namespace OLJ.Gameplay.Interactable
{
    public class CageInteract : InteractableBehaviour
    {
        [Separator("Animation")]
        [SerializeField] private Animator cageAnim;
        //[SerializeField] private float cageShakeInterval = 2f;
        [SerializeField] private SpineAnimationData spa;
        [SerializeField] private string cageOpenAnimParam = "CageOpen";
        private const string qteAnimation = "cain_opencage";
        private int cageOpenAnimParamHash;
        private int cageShakeAnimParamHash;

        [Separator("Object to spawn on cage open")]
        [SerializeField] private GameObject cageTop;
        [SerializeField] private GameObject cageOpenAshes;
        [SerializeField] private GameObject cageImpactAshes;

        [Separator("On cage open")]
        [SerializeField] private float cageTopForce = 100f;
        [SerializeField] private bool isContainingMonster = true; 

        [Separator("Monsters")]
        [SerializeField, ConditionalField(nameof(isContainingMonster), false)] private GameObject monster;
        [SerializeField, ConditionalField(nameof(isContainingMonster), false)] private Transform monsterSpawnPoint;

        private bool isCageOpen = false;

        private void Start()
        {
            cageOpenAnimParamHash = Animator.StringToHash(cageOpenAnimParam);
            cageShakeAnimParamHash = Animator.StringToHash("CageShake");

            //onInteractStart.AddListener(OpenCage);

            StartCoroutine(ShakeCage());
        }

        private void OnDisable()
        {
            //QTEManager.Instance.onQTEfinished.RemoveAllListeners();
            //onInteractStart.RemoveListener(OpenCage);
        }

        protected override void StartInteractAction(CharacterBody cb)
        {
            base.StartInteractAction(cb);

            QTEManager.Instance.onQTEfinished.AddListener(OnQteEnd);

            ControlManager.Instance.DisableControl();
            QTEManager.Instance.StartQTE(0 );
            QTEUIManager.Instance.SetQTEAnimation(spa.sk, qteAnimation);
            this.GetComponent<Collider>().enabled = false;
        }

        private void OnQteEnd(int _index)
        {
            Debug.Log($"QTEUI called: End Interact QTE {_index}");
            QTEUIManager.Instance.HideQTEAnimation();
            ControlManager.Instance.EnableControl();
            this.isCageOpen = true;
            this.cageAnim.SetBool(cageOpenAnimParamHash, isCageOpen);

            StartCoroutine(SetUpCageOpen());
            StartCoroutine(SetUpCageImpact());
            QTEManager.Instance.onQTEfinished.RemoveListener(OnQteEnd);
            Invoke(nameof(SetUpMonster), .18f);
        }

        private IEnumerator SetUpCageOpen()
        {
            yield return new WaitForSeconds(.15f);

            cageTop.SetActive(isCageOpen);
            cageOpenAshes.SetActive(isCageOpen);
            Rigidbody rb = cageTop.GetComponent<Rigidbody>();
            rb.AddForce(new Vector3(Random.Range(-1f, 1f),
                                    Random.Range(-1f, 1f),
                                    Random.Range(-1f, 1f)) * cageTopForce, ForceMode.Impulse);

            this.GetComponentInChildren<SpriteRenderer>().sortingOrder = -101;

            onInteractEnd?.Invoke();
        }

        private void SetUpMonster()
        {
            if (isContainingMonster && monsterSpawnPoint.childCount < 1)
            {
                GameObject monsters = Instantiate(monster, monsterSpawnPoint);
                monsters.transform.localPosition = Vector3.zero;
            }
            else
                Debug.Log("Cage empty wleeee");
        }

        private IEnumerator SetUpCageImpact()
        {
            yield return new WaitForSeconds(.5f);
            cageImpactAshes.SetActive(true);
        }

        private IEnumerator ShakeCage()
        {
            while(!isCageOpen)
            {
                yield return new WaitForSeconds(Random.Range(1f, 5f));

                if (isCageOpen) yield break;

                cageAnim.Play(cageShakeAnimParamHash);                   
            }
        }
    }
}
