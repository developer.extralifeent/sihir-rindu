using OLJ.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Manager;
using OLJ.UI;

namespace OLJ.Gameplay.Interactable {
    public class DiaryInteract :InteractableBehaviour
    {
        [SerializeField] DiarySO diary;
        CharacterBody cb;
        protected override void StartInteractAction(CharacterBody bodyType) {
            Debug.Log("Diary Picked Up");
            AddDiary();
            this.gameObject.SetActive(false);
            #region Temporarily Disabled
            //cb = bodyType;
            //base.InteractStartAction(bodyType);
            //if (bodyType.GetComponent<DiaryBehaviour>()) {
            //    UIManager.Instance.IsOpenDiary = !UIManager.Instance.IsOpenDiary;
            //    ControlManager.Instance.SwitchInputAction(ControlManager.Instance.CharacterInputAction.UI);
            //    ControlManager.Instance.CharacterInputAction.UI.Cancel.performed += CloseDiary;
            //    transform.Find("Visual").gameObject.SetActive(false);
            //    AddDiary();
            //    bodyType.DisableControl();
            //    if (OLJ.UI.DiaryManager.Instance) {
            //        OLJ.UI.DiaryManager.Instance.OpenDiary();
            //        OLJ.UI.DiaryManager.Instance.OpenPage(CollectionManager.Instance.DiaryData.Count - 1);
            //    }
                   
            //}
            #endregion
        }

        void CloseDiary(UnityEngine.InputSystem.InputAction.CallbackContext context) {
            InteractEndAction(cb);
        }

        public override void InteractEndAction(CharacterBody bodyType)
        {
            Debug.Log("Close diary?");
            ControlManager.Instance.SwitchInputAction(ControlManager.Instance.CharacterInputAction.Player);
            base.InteractEndAction(bodyType);
            bodyType.EnableControl();
            if (OLJ.UI.DiaryManager.Instance)
                OLJ.UI.DiaryManager.Instance.HideDiary();
            gameObject.SetActive(false);
        }

        void AddDiary() {
            CollectionManager.Instance.AddToCollection(diary);
            Debug.Log("Diary Added: " + diary);
        }
    }
}

