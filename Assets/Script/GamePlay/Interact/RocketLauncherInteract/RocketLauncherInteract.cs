using Fungus;
using MyBox;
using OLJ.Character;
using OLJ.MiniGame.Fight2;
using UnityEngine;

namespace OLJ.Gameplay.Interactable
{
    public class RocketLauncherInteract : InteractableBehaviour
    {
        [Separator("Rocket Launcher Parameters")]
        [SerializeField] private Fight2PlayerState playerState;
        [SerializeField] private Flowchart shootingFlowchart;
        [SerializeField] private string flowchartBlockName;

        protected override void StartInteractAction(CharacterBody bodyType)
        {  
            base.StartInteractAction(bodyType);
         
            if (playerState.PlayerMissileState != PlayerMissileState.CarryingMissile) return;

            shootingFlowchart.ExecuteBlock(flowchartBlockName);
            playerState.OnMissileReleased();
        }
    }
}
