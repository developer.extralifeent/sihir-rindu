using MyBox;
using OLJ.Character;
using OLJ.Manager;
using OLJ.MiniGame.Fight2;
using OLJ.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Gameplay.Interactable
{
    public class MissileInteract : InteractableBehaviour
    {
        private Fight2GameManager fight2Manager;
        private QTEUIManager qteUIManager;

        [Separator("QTEs")]
        [SerializeField] SpineAnimationData spa;

        public int MissileSpotIndex { get; set; }

        private void Awake()
        {
            fight2Manager = Fight2GameManager.Instance;
            qteUIManager = QTEUIManager.Instance;
        }

        protected override void StartInteractAction(CharacterBody _bodyType)
        {
            if (fight2Manager.GetPlayerMissileState() == PlayerMissileState.CarryingMissile) return;

            ControlManager.Instance.DisableControl();
            QTEManager.Instance.StartQTE(1);
            if (qteUIManager && spa.animName.Length > 0 && spa.sk && spa.animName != string.Empty)
            {
                qteUIManager.SetQTEAnimation(spa.sk, spa.animName);
            }

            QTEManager.Instance.onQTEfinished.AddListener(OnMissilePicked);
        }

        private void OnMissilePicked(int _index)
        {
            if (QTEUIManager.Instance)
            {
                QTEUIManager.Instance.HideQTEAnimation();
            }
            ControlManager.Instance.EnableControl();

            fight2Manager.GetPlayerState().OnMissileTaken();
            HideIcon(ControlManager.Instance.CurrentBody);
            QTEManager.Instance.onQTEfinished.RemoveListener(OnMissilePicked);

            fight2Manager.EmptyMissileSpot(MissileSpotIndex);
            fight2Manager.RemoveBrokenMissile(this.GetComponent<Fight2MissileBehaviour>()); 
            onInteractStart?.Invoke();
            fight2Manager.ShotCannonKancil();
            Destroy(this.gameObject);
        }
    }
}