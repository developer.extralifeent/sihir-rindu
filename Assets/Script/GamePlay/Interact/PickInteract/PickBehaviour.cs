using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using OLJ.Manager;
using OLJ.Character;
using OLJ.UI;

namespace OLJ.Gameplay.Interactable {
    public class PickBehaviour : MonoBehaviour {
        private PickInteract currentObject;
        CharacterInput charInput;
        private void Start() {

            charInput = ControlManager.Instance.CharacterInputAction;
        }
        public void PickedObject(PickInteract obj) {
            if (!currentObject) {
                charInput.Player.Interact.performed += DropObject;
                obj.gameObject.SetActive(false);
                currentObject = obj;
                Sprite sp=currentObject.GetComponentInChildren<SpriteRenderer>().sprite;
                GetComponentInChildren<CharacterUIController>().SetPickIcon(sp);
            }
        }
        public void DropObject(InputAction.CallbackContext context) {
            if (currentObject != null) {
                currentObject.transform.position = transform.position;
                currentObject.gameObject.SetActive(true);
                currentObject.InteractEndAction(GetComponent<CharacterBody>());
                currentObject = null;
                GetComponentInChildren<CharacterUIController>().UnSetPickIcon();
                charInput.Player.Interact.performed -= DropObject;
            }
        }
    }
}

