using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLJ.Character;

namespace OLJ.Gameplay.Interactable {
    public class PickInteract : InteractableBehaviour {

        protected override void StartInteractAction(CharacterBody bodyType) {
            base.StartInteractAction(bodyType);
            if (bodyType.GetComponent<PickBehaviour>()) {
                Debug.Log(bodyType.name + ": Picked");
                bodyType.GetComponent<PickBehaviour>().PickedObject(this);
            }
        }       
    }
}

