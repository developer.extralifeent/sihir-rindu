using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Ini buat door coridor di scene 12 chapter prolog 
namespace OLJ.Gameplay
{
    public class DoorManager : MonoBehaviour
    {
        [SerializeField] GameObject[] timunTrigger;
        [SerializeField] GameObject[] doorTrigger;

        public void ResetDoor()
        {
            foreach (GameObject go in timunTrigger) go.SetActive(true);
            foreach (GameObject go in doorTrigger) go.SetActive(false);
        }
    }
}


