using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Ini buat bush di Chapter prolog Scene 9
namespace OLJ.Gameplay
{
    public class BushManager : MonoBehaviour
    {
        int[,] randomizedPositions = new int[3, 3];
        [SerializeField] Transform[] positions;
        [SerializeField] MeshRenderer[] bushMeshes;
        [SerializeField] Material bushStatic;
        [SerializeField] Material bushGerak;
        [Space]
        [SerializeField] GameObject kancil;
        [Space]
        [SerializeField] GameObject[] falseBush = new GameObject[2];
        [SerializeField] GameObject[] correctBush = new GameObject[3];

        const int fixedBushIndex = 8;

        void Start()
        {
            GeneratePositions();
            falseBush[0].gameObject.SetActive(false);
            falseBush[1].gameObject.SetActive(false);
        }

        public void TriggerLonceng(int iter)
        {
            correctBush[0].transform.parent.gameObject.SetActive(true);
            //mas wita minta bunyi lonceng ketiga ga dirandom
            if(iter != 2)
            {
                correctBush[iter].transform.position = positions[randomizedPositions[iter, 0]].position;
                kancil.transform.position = new Vector3(positions[randomizedPositions[iter, 0]].position.x, kancil.transform.position.y, positions[randomizedPositions[iter, 0]].position.z);

                falseBush[0].gameObject.SetActive(true);
                falseBush[0].transform.position = positions[randomizedPositions[iter, 1]].position;

                falseBush[1].gameObject.SetActive(true);
                falseBush[1].transform.position = positions[randomizedPositions[iter, 2]].position;

                int[] index = { randomizedPositions[iter, 0], randomizedPositions[iter, 1], randomizedPositions[iter, 2] };
                GoyangBush(index);
            }
            else
            {
                correctBush[iter].transform.position = positions[fixedBushIndex].transform.position;
                kancil.transform.position = new Vector3(positions[fixedBushIndex].transform.position.x, kancil.transform.position.y, positions[fixedBushIndex].transform.position.z);

                falseBush[0].transform.position = positions[randomizedPositions[iter, 0]].position;
                falseBush[1].transform.position = positions[randomizedPositions[iter, 1]].position;

                int[] index = { randomizedPositions[iter, 0], randomizedPositions[iter, 1], fixedBushIndex};
                GoyangBush(index);
            }
        }

        void GoyangBush(int[] bushIndex)
        {
            foreach (int idx in bushIndex)
                bushMeshes[idx].material = bushGerak;
        }

        public void RevertGoyang()
        {
            foreach (MeshRenderer mr in bushMeshes)
                mr.material = bushStatic;
        } 

        void GeneratePositions()
        {
            for(int i = 0; i < 3; i++)
            {
                List<int> randomList = RandomizedList(0, positions.Length - 1, 3);
                if(i == 2 && randomList.Contains(fixedBushIndex))
                {
                    randomList.Remove(fixedBushIndex);
                    int index = Random.Range(0, 9);
                    while(randomList.Contains(index) || index == fixedBushIndex)
                        index = Random.Range(0, 9);
                }

                for (int j = 0; j < randomList.Count; j++)
                    randomizedPositions[i, j] = randomList[j];
            }
        }

        List<int> RandomizedList(int start, int finish, int take)
        {
            List<int> orderedSequence = new List<int>();
            for (int i = start; i <= finish; i++) orderedSequence.Add(i);

            List<int> finalSequence = new List<int>();
            for (int i = 0; i < take; i++)
            {
                int rng = Random.Range(0, orderedSequence.Count);
                finalSequence.Add(orderedSequence[rng]);
                orderedSequence.RemoveAt(rng);
            }

            return finalSequence;
        }
    }
}
