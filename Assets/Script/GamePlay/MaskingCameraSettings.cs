using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OLJ.Gameplay
{
    public class MaskingCameraSettings : MonoBehaviour {

        [SerializeField] private Camera mainCam;
        private Camera maskingCam;

        // Start is called before the first frame update
        void Start() {
            maskingCam = this.GetComponent<Camera>();

            StartCoroutine(CheckMainCamFoV());
        }

        private IEnumerator CheckMainCamFoV() {
            while (true) {
                if (maskingCam.fieldOfView != mainCam.fieldOfView)
                    maskingCam.fieldOfView = mainCam.fieldOfView;
                yield return null;
            }
        }
    }
}

