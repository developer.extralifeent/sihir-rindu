using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITween : MonoBehaviour
{
    public float Duration = 0.5f;
    public float Amount = 1;
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.scale(gameObject, new Vector3(gameObject.transform.lossyScale.x + Amount, gameObject.transform.lossyScale.y + Amount, gameObject.transform.lossyScale.z + Amount), Duration).setLoopPingPong();
    }

  
}
