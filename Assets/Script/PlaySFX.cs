using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFX : MonoBehaviour
{
    // Regular way to play sound

    //void Start()
    //{
    //    AudioManager.instance.Play("");
    //}

    void PlaySound(string SFX) //Used to play sound with animation event
    {
        AudioManager.instance.Play(SFX);
    }
}
