
stone-fall.png
size: 4096,2048
format: RGBA4444
filter: Linear,Linear
repeat: none
batu
  rotate: false
  xy: 2245, 165
  size: 475, 478
  orig: 512, 512
  offset: 25, 14
  index: -1
cannon-vfx_00000
  rotate: false
  xy: 2306, 1071
  size: 434, 472
  orig: 640, 836
  offset: 108, 95
  index: -1
cannon-vfx_00001
  rotate: false
  xy: 1841, 882
  size: 463, 562
  orig: 640, 836
  offset: 94, 110
  index: -1
cannon-vfx_00002
  rotate: false
  xy: 1235, 1398
  size: 485, 638
  orig: 640, 836
  offset: 85, 114
  index: -1
cannon-vfx_00003
  rotate: false
  xy: 1722, 1446
  size: 497, 590
  orig: 640, 836
  offset: 62, 184
  index: -1
cannon-vfx_00004
  rotate: false
  xy: 2221, 1545
  size: 511, 491
  orig: 640, 836
  offset: 44, 283
  index: -1
cannon-vfx_00005
  rotate: false
  xy: 560, 78
  size: 523, 339
  orig: 640, 836
  offset: 49, 297
  index: -1
cannon-vfx_00006
  rotate: false
  xy: 1085, 92
  size: 546, 328
  orig: 640, 836
  offset: 38, 306
  index: -1
dirt
  rotate: false
  xy: 1841, 636
  size: 251, 244
  orig: 251, 244
  offset: 0, 0
  index: -1
side-smoke0001
  rotate: false
  xy: 2094, 645
  size: 372, 235
  orig: 386, 236
  offset: 14, 1
  index: -1
side-smoke0002
  rotate: false
  xy: 2468, 837
  size: 374, 232
  orig: 386, 236
  offset: 10, 3
  index: -1
side-smoke0003
  rotate: false
  xy: 2734, 1806
  size: 384, 230
  orig: 386, 236
  offset: 0, 0
  index: -1
smoke_00000
  rotate: false
  xy: 1230, 453
  size: 609, 784
  orig: 779, 919
  offset: 68, 0
  index: -1
smoke_00001
  rotate: false
  xy: 622, 422
  size: 606, 799
  orig: 779, 919
  offset: 94, 0
  index: -1
smoke_00002
  rotate: false
  xy: 2, 419
  size: 618, 802
  orig: 779, 919
  offset: 90, 0
  index: -1
smoke_00003
  rotate: false
  xy: 2, 1223
  size: 622, 813
  orig: 779, 919
  offset: 89, 0
  index: -1
smoke_00004
  rotate: false
  xy: 626, 1239
  size: 607, 797
  orig: 779, 919
  offset: 88, 0
  index: -1
tanah
  rotate: false
  xy: 1085, 2
  size: 169, 88
  orig: 169, 88
  offset: 0, 0
  index: -1
tent
  rotate: false
  xy: 2, 49
  size: 556, 368
  orig: 1024, 512
  offset: 234, 72
  index: -1
tent-destroy
  rotate: false
  xy: 1633, 192
  size: 610, 259
  orig: 1024, 512
  offset: 198, 73
  index: -1
