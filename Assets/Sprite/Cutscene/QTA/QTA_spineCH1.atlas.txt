
QTA_spineCH1.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
Cain Buka Cage/Body
  rotate: false
  xy: 1923, 1632
  size: 624, 788
  orig: 1920, 1080
  offset: 635, 237
  index: -1
Cain Buka Cage/Cage
  rotate: false
  xy: 1802, 503
  size: 499, 419
  orig: 1920, 1080
  offset: 676, 54
  index: -1
Cain Buka Cage/Cape
  rotate: false
  xy: 1923, 924
  size: 422, 706
  orig: 1920, 1080
  offset: 797, 94
  index: -1
Cain Buka Cage/Hand
  rotate: false
  xy: 3757, 1341
  size: 278, 327
  orig: 1920, 1080
  offset: 794, 418
  index: -1
Cain Buka Cage/Head
  rotate: false
  xy: 1595, 1094
  size: 249, 244
  orig: 1920, 1080
  offset: 845, 730
  index: -1
Cain Buka Cage/Lid
  rotate: false
  xy: 492, 545
  size: 493, 229
  orig: 1920, 1080
  offset: 666, 303
  index: -1
Cain Lepasin Kancil/Body
  rotate: false
  xy: 3717, 934
  size: 366, 405
  orig: 1920, 1080
  offset: 579, 101
  index: -1
Cain Lepasin Kancil/Cape
  rotate: false
  xy: 3622, 284
  size: 455, 358
  orig: 1920, 1080
  offset: 461, 231
  index: -1
Cain Lepasin Kancil/Hand Back Frame 1
  rotate: false
  xy: 3451, 708
  size: 263, 316
  orig: 1920, 1080
  offset: 737, 183
  index: -1
Cain Lepasin Kancil/Hand Back Frame 2
  rotate: false
  xy: 3451, 1026
  size: 264, 321
  orig: 1920, 1080
  offset: 737, 178
  index: -1
Cain Lepasin Kancil/Hand Front Frame 1
  rotate: false
  xy: 3094, 498
  size: 261, 318
  orig: 1920, 1080
  offset: 638, 146
  index: -1
Cain Lepasin Kancil/Hand Front Frame 2
  rotate: false
  xy: 3810, 1670
  size: 271, 341
  orig: 1920, 1080
  offset: 649, 132
  index: -1
Cain Lepasin Kancil/Head
  rotate: false
  xy: 3357, 453
  size: 263, 253
  orig: 1920, 1080
  offset: 638, 501
  index: -1
Cain Lepasin Kancil/Kancil Body
  rotate: false
  xy: 3451, 1349
  size: 304, 410
  orig: 1920, 1080
  offset: 1250, 670
  index: -1
Cain Lepasin Kancil/Kancil Hand
  rotate: false
  xy: 2347, 1352
  size: 168, 278
  orig: 1920, 1080
  offset: 1166, 746
  index: -1
Cain Lepasin Kancil/Rope
  rotate: false
  xy: 3162, 818
  size: 287, 941
  orig: 1920, 1080
  offset: 863, 139
  index: -1
Cain Pasir hisap/Body
  rotate: false
  xy: 2513, 359
  size: 551, 271
  orig: 1920, 1080
  offset: 703, 279
  index: -1
Cain Pasir hisap/Cape
  rotate: false
  xy: 844, 776
  size: 502, 259
  orig: 1920, 1080
  offset: 710, 377
  index: -1
Cain Pasir hisap/Hand
  rotate: false
  xy: 844, 1037
  size: 749, 301
  orig: 1920, 1080
  offset: 582, 247
  index: -1
Cain Pasir hisap/Head
  rotate: false
  xy: 3716, 644
  size: 292, 288
  orig: 1920, 1080
  offset: 783, 465
  index: -1
Cain Pasir hisap/Sand Frame 1
  rotate: false
  xy: 2212, 1
  size: 1106, 293
  orig: 1920, 1080
  offset: 437, 145
  index: -1
Cain Pasir hisap/Sand Frame 2
  rotate: false
  xy: 1107, 206
  size: 1103, 295
  orig: 1920, 1080
  offset: 438, 143
  index: -1
Cain Pasir hisap/Sand Frame 3
  rotate: false
  xy: 1, 242
  size: 1104, 301
  orig: 1920, 1080
  offset: 438, 141
  index: -1
Cain jembatan/Cain
  rotate: false
  xy: 3810, 2013
  size: 228, 407
  orig: 1920, 1080
  offset: 1313, 235
  index: -1
Cain jembatan/Cape 1
  rotate: false
  xy: 3066, 347
  size: 246, 149
  orig: 1920, 1080
  offset: 1370, 328
  index: -1
Cain jembatan/Cape 2
  rotate: false
  xy: 3314, 296
  size: 222, 155
  orig: 1920, 1080
  offset: 1370, 321
  index: -1
Cain jembatan/Cape 3
  rotate: false
  xy: 2303, 454
  size: 208, 181
  orig: 1920, 1080
  offset: 1342, 313
  index: -1
Cain jembatan/Jembatan 1
  rotate: false
  xy: 1348, 570
  size: 414, 268
  orig: 1920, 1080
  offset: 104, 97
  index: -1
Cain jembatan/Jembatan 2
  rotate: false
  xy: 1595, 840
  size: 205, 252
  orig: 1920, 1080
  offset: 423, 94
  index: -1
Cain jembatan/Jembatan 3
  rotate: false
  xy: 1, 857
  size: 841, 481
  orig: 1920, 1080
  offset: 558, 105
  index: -1
Cain jembatan/Jembatan 3a
  rotate: false
  xy: 2347, 882
  size: 176, 240
  orig: 1920, 1080
  offset: 558, 105
  index: -1
Cain jembatan/Jembatan 3b
  rotate: false
  xy: 2525, 632
  size: 567, 469
  orig: 1920, 1080
  offset: 832, 117
  index: -1
Cain jembatan/Jembatan 4
  rotate: false
  xy: 2347, 1124
  size: 191, 226
  orig: 1920, 1080
  offset: 1158, 120
  index: -1
Cain jembatan/Jembatan 5
  rotate: false
  xy: 1, 612
  size: 489, 243
  orig: 1920, 1080
  offset: 1291, 107
  index: -1
Cain jembatan/Jembatan 6
  rotate: false
  xy: 2303, 637
  size: 168, 243
  orig: 1920, 1080
  offset: 696, 107
  index: -1
Cain jembatan/Kukumba 1
  rotate: false
  xy: 2549, 1762
  size: 629, 658
  orig: 1920, 1080
  offset: 18, 246
  index: -1
Cain jembatan/Kukumba 2
  rotate: false
  xy: 2549, 1103
  size: 611, 657
  orig: 1920, 1080
  offset: 22, 250
  index: -1
Cain jembatan/Kukumba 3
  rotate: false
  xy: 3180, 1761
  size: 628, 659
  orig: 1920, 1080
  offset: 28, 245
  index: -1
black_box
  rotate: false
  xy: 1, 1340
  size: 1920, 1080
  orig: 1920, 1080
  offset: 0, 0
  index: -1
