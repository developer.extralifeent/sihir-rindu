
Golem.png
size: 4096,4096
format: RGBA4444
filter: Linear,Linear
repeat: none
back/Lfoot-back
  rotate: false
  xy: 692, 2263
  size: 424, 521
  orig: 426, 523
  offset: 1, 1
  index: -1
back/Lhand- back
  rotate: false
  xy: 2903, 397
  size: 594, 1017
  orig: 596, 1019
  offset: 1, 1
  index: -1
back/Rfoot-back
  rotate: false
  xy: 1118, 2263
  size: 424, 521
  orig: 426, 523
  offset: 1, 1
  index: -1
back/Rhand-back
  rotate: false
  xy: 3499, 397
  size: 594, 1017
  orig: 596, 1019
  offset: 1, 1
  index: -1
back/Rhanddown-push
  rotate: false
  xy: 506, 3373
  size: 440, 453
  orig: 441, 455
  offset: 1, 0
  index: -1
back/Rhandup-push
  rotate: false
  xy: 1544, 2164
  size: 574, 528
  orig: 574, 528
  offset: 0, 0
  index: -1
back/chest- back
  rotate: false
  xy: 1595, 758
  size: 1306, 656
  orig: 1308, 658
  offset: 1, 1
  index: -1
back/head-back
  rotate: false
  xy: 2, 1085
  size: 730, 630
  orig: 732, 632
  offset: 1, 1
  index: -1
back/mouth- back
  rotate: false
  xy: 2, 2925
  size: 864, 446
  orig: 866, 448
  offset: 1, 1
  index: -1
diagback/Lfoot-diagback
  rotate: false
  xy: 3506, 2544
  size: 399, 518
  orig: 401, 520
  offset: 1, 1
  index: -1
diagback/Rfoot-diagback
  rotate: false
  xy: 3506, 2544
  size: 399, 518
  orig: 401, 520
  offset: 1, 1
  index: -1
diagback/head-diagback
  rotate: false
  xy: 734, 913
  size: 859, 694
  orig: 861, 696
  offset: 1, 1
  index: -1
diagback/mouth-diagback
  rotate: false
  xy: 1335, 3634
  size: 305, 334
  orig: 307, 336
  offset: 1, 1
  index: -1
diagback/neckleaf-diagback
  rotate: false
  xy: 1310, 2
  size: 1196, 754
  orig: 1198, 756
  offset: 1, 1
  index: -1
diagback/neckleaf2-diagback
  rotate: false
  xy: 1642, 3628
  size: 1200, 340
  orig: 1202, 342
  offset: 1, 1
  index: -1
diagfront/Lfoot-diagfront
  rotate: false
  xy: 1248, 1609
  size: 420, 553
  orig: 422, 556
  offset: 1, 2
  index: -1
diagfront/Rfoot-diagfront
  rotate: false
  xy: 2844, 2100
  size: 407, 535
  orig: 410, 537
  offset: 1, 1
  index: -1
diagfront/chest-diagfront
  rotate: false
  xy: 948, 3234
  size: 1155, 392
  orig: 1157, 394
  offset: 1, 1
  index: -1
diagfront/eye-diagfront
  rotate: false
  xy: 540, 3828
  size: 426, 140
  orig: 428, 142
  offset: 1, 1
  index: -1
diagfront/head-diagfront
  rotate: false
  xy: 463, 1717
  size: 783, 544
  orig: 786, 546
  offset: 1, 1
  index: -1
diagfront/mouth- 1 diagfront
  rotate: false
  xy: 2750, 3155
  size: 821, 425
  orig: 824, 428
  offset: 2, 1
  index: -1
front-push/Lhand-push
  rotate: false
  xy: 2105, 3161
  size: 643, 465
  orig: 645, 468
  offset: 1, 2
  index: -1
front-push/Lhanddown-push
  rotate: false
  xy: 1670, 1544
  size: 527, 574
  orig: 530, 576
  offset: 1, 1
  index: -1
front-push/Lhandup-push
  rotate: false
  xy: 2379, 2655
  size: 688, 498
  orig: 690, 501
  offset: 1, 2
  index: -1
front-push/Lmoss
  rotate: false
  xy: 968, 3651
  size: 365, 317
  orig: 367, 319
  offset: 1, 1
  index: -1
front-push/Rhand-push
  rotate: false
  xy: 1734, 2694
  size: 643, 465
  orig: 645, 468
  offset: 1, 2
  index: -1
front-push/Rhanddown-push
  rotate: false
  xy: 2199, 1544
  size: 527, 574
  orig: 530, 576
  offset: 2, 1
  index: -1
front-push/Rhandup-push
  rotate: false
  xy: 2, 2425
  size: 688, 498
  orig: 690, 501
  offset: 1, 2
  index: -1
front/Lfoot-front
  rotate: false
  xy: 3573, 3064
  size: 435, 516
  orig: 437, 518
  offset: 1, 1
  index: -1
front/Rfoot-front
  rotate: false
  xy: 3069, 2637
  size: 435, 516
  orig: 437, 518
  offset: 1, 1
  index: -1
front/chest- front
  rotate: false
  xy: 2, 255
  size: 1306, 656
  orig: 1308, 658
  offset: 1, 1
  index: -1
front/eyes- front
  rotate: false
  xy: 2, 3863
  size: 416, 105
  orig: 418, 107
  offset: 1, 1
  index: -1
front/head-front
  rotate: false
  xy: 2120, 2120
  size: 722, 533
  orig: 724, 535
  offset: 1, 1
  index: -1
front/mouth- front
  rotate: false
  xy: 868, 2786
  size: 864, 446
  orig: 866, 448
  offset: 1, 1
  index: -1
side/Lfoot-side
  rotate: false
  xy: 3253, 2007
  size: 460, 535
  orig: 462, 538
  offset: 1, 2
  index: -1
side/Rfoot-side
  rotate: false
  xy: 2, 1888
  size: 459, 535
  orig: 461, 538
  offset: 1, 2
  index: -1
side/eye-side
  rotate: false
  xy: 420, 3849
  size: 118, 119
  orig: 120, 121
  offset: 1, 1
  index: -1
side/head-side
  rotate: false
  xy: 2728, 1416
  size: 821, 589
  orig: 823, 591
  offset: 1, 1
  index: -1
side/mouth
  rotate: false
  xy: 2844, 3582
  size: 942, 386
  orig: 944, 388
  offset: 1, 1
  index: -1
side/neckleaf-side 2
  rotate: false
  xy: 2, 3424
  size: 502, 423
  orig: 504, 425
  offset: 1, 1
  index: -1

Golem2.png
size: 4096,4096
format: RGBA4444
filter: Linear,Linear
repeat: none
back/body-back
  rotate: false
  xy: 2, 2
  size: 1486, 1111
  orig: 1488, 1114
  offset: 1, 1
  index: -1
back/neckleaf- back
  rotate: false
  xy: 2, 3291
  size: 1250, 766
  orig: 1252, 768
  offset: 1, 1
  index: -1
diagback/Lhand-diagback
  rotate: false
  xy: 3024, 2177
  size: 671, 1051
  orig: 673, 1053
  offset: 1, 1
  index: -1
diagback/Rhand-diagback
  rotate: false
  xy: 2436, 2191
  size: 586, 1037
  orig: 588, 1039
  offset: 1, 1
  index: -1
diagfront/Lhand-diagfront
  rotate: false
  xy: 2782, 1044
  size: 776, 1131
  orig: 779, 1133
  offset: 1, 1
  index: -1
diagfront/Rhand- 1 diagfront
  rotate: false
  xy: 2, 1388
  size: 643, 1062
  orig: 646, 1065
  offset: 1, 2
  index: -1
diagfront/body-1 diagfront
  rotate: false
  xy: 647, 1115
  size: 1481, 1109
  orig: 1483, 1111
  offset: 1, 1
  index: -1
diagfront/neckleaf-diagfront
  rotate: false
  xy: 2642, 3230
  size: 1222, 827
  orig: 1224, 829
  offset: 1, 1
  index: -1
front/Lhand- front
  rotate: false
  xy: 1244, 2226
  size: 594, 1017
  orig: 596, 1019
  offset: 1, 1
  index: -1
front/Rhand-front
  rotate: false
  xy: 1840, 2226
  size: 594, 1017
  orig: 596, 1019
  offset: 1, 1
  index: -1
front/neckleaf- front
  rotate: false
  xy: 2, 2452
  size: 1240, 837
  orig: 1242, 840
  offset: 1, 1
  index: -1
side/Lhand-side
  rotate: false
  xy: 2130, 1071
  size: 650, 1118
  orig: 652, 1120
  offset: 1, 1
  index: -1
side/neckleaf-side
  rotate: false
  xy: 1254, 3245
  size: 1386, 812
  orig: 1388, 814
  offset: 1, 1
  index: -1

Golem3.png
size: 4096,4096
format: RGBA4444
filter: Linear,Linear
repeat: none
diagback/body-diagback
  rotate: false
  xy: 1490, 2026
  size: 1436, 1231
  orig: 1439, 1234
  offset: 2, 2
  index: -1
eat/eat2
  rotate: false
  xy: 1430, 2
  size: 2363, 2022
  orig: 2366, 2025
  offset: 1, 2
  index: -1
front/body-front
  rotate: false
  xy: 2, 2146
  size: 1486, 1111
  orig: 1488, 1114
  offset: 1, 1
  index: -1
side/body-side
  rotate: false
  xy: 2, 884
  size: 1426, 1260
  orig: 1428, 1262
  offset: 1, 1
  index: -1

Golem4.png
size: 4096,2048
format: RGBA4444
filter: Linear,Linear
repeat: none
eat/eat1
  rotate: false
  xy: 2, 2
  size: 2616, 2036
  orig: 2618, 2040
  offset: 1, 2
  index: -1

Golem5.png
size: 4096,4096
format: RGBA4444
filter: Linear,Linear
repeat: none
eat/eat3
  rotate: false
  xy: 2, 2
  size: 2144, 2416
  orig: 2147, 2420
  offset: 1, 2
  index: -1
