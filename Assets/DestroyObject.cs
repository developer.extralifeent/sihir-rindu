using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    [SerializeField] float duration;
    private void Start()
    {
        Destroy(this.gameObject, duration);
    }
}
